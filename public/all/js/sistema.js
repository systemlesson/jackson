/* 
 * Arquivo padrão de comandos que são carregados pelo sistema em todas as páginas
 */

function modalErro() {
    var data = {
        "component": "returnDefaultFailJson",
        "erro": 1,
        "modal": {
            "type": "error",
            "message": "N\u00e3o foi Poss\u00edvel realizar a opera\u00e7\u00e3o",
            "icon": "<i class=\"glyphicon glyphicon-check\"><\/i>",
            "title": "Falha!",
            "urlRedirect": "",
            "buttonColor": "#F44336"
        }
    }
    modalAlert(JSON.stringify(data), false);
}


function validaCPF(cpf) {

    if (cpf == "") {
        return true
    }
    cpf = cpf.replace(/[^\d]+/g, '');

    if (/^1{11}|2{11}|3{11}|4{11}|5{11}|6{11}|7{11}|8{11}|9{11}|0{11}$/.test(cpf)) {
        return false;
    }
    if (!/^\d{11}$/.test(cpf) && !/^\d{3}\.\d{3}\.\d{3}-\d{2}$/.test(cpf)) {
        return false;
    }

    var d1 = 0;
    for (var i = 0; i < 9; i++) {
        d1 += (10 - i) * parseInt(cpf.charAt(i), 10);
    }
    d1 = 11 - d1 % 11;
    if (d1 === 10 || d1 === 11) {
        d1 = 0;
    }
    if (d1 + '' !== cpf.charAt(9)) {
        return false;
    }

    var d2 = 0;
    for (i = 0; i < 10; i++) {
        d2 += (11 - i) * parseInt(cpf.charAt(i), 10);
    }
    d2 = 11 - d2 % 11;
    if (d2 === 10 || d2 === 11) {
        d2 = 0;
    }

    return (d2 + '' === cpf.charAt(10));
}



jQuery(document).ready(function () {


    $("input, textarea").on("drop", function (e) {
        e.stopPropagation();
        e.preventDefault();
        return false;
    });

    $('[data-toggle="popover"]').popover({
        trigger: 'hover'
    });


    confirm = function (title, text, type, callback) {
        swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
            confirmButtonColor: "#4CAF50",
            cancelButtonColor: "#9a0303",
            cancelButtonText: "Cancelar",
            confirmButtonText: "Sim, confirmo!",
            closeOnConfirm: false,
            html: true

        }, callback);
        return false;
    };


});

$(document).on('click', '.alert-confirm-atender', function (e) {

    e.preventDefault();

    var form = $('form');
    var href = $(this).prop('href');
    var objRegistro = $('#tr' + $(this).attr('target-id'));

    swal({
        title: "Você tem certeza que deseja atender a solicitação?",
        text: "Essa operação não poderá ser desfeita.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        cancelButtonText: "Não, cancelar",
        confirmButtonText: "Sim, tenho certeza!",
        closeOnConfirm: false
    }, function () {
        $.post(href, function (data) {
            var json = JSON.parse(data);

            var swalAjaxMessage = json.modal.message;
            var swalAjaxType = json.modal.type;
            var swalAjaxIcon = json.modal.icon;
            var swalAjaxTitle = json.modal.title;
            var swalAjaxUrlRedirect = json.modal.urlRedirect;

            if (json.erro == 0) {

                $('button.confirm').on('click', function () {
                    location.href = swalAjaxUrlRedirect;
                    return false;
                });

            } else {
                if (form != false) {
                    form.find('button.confirm').removeAttr('disabled');
                }
            }
            swal(swalAjaxTitle, swalAjaxMessage, swalAjaxType);

            $('button.confirm').on('click', function () {
                return false;
            });
        });

    });

    return false;
});

$(document).on('click', '.alert-confirm-cancelar', function (e) {

    e.preventDefault();

    var form = $('form');
    var href = $(this).prop('href');
    var objRegistro = $('#tr' + $(this).attr('target-id'));

    swal({
        title: "Você tem certeza que deseja cancelar a solicitação?",
        text: "Essa operação não poderá ser desfeita.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        cancelButtonText: "Não, cancelar",
        confirmButtonText: "Sim, tenho certeza!",
        closeOnConfirm: false
    }, function () {
        $.post(href, function (data) {
            var json = JSON.parse(data);

            var swalAjaxMessage = json.modal.message;
            var swalAjaxType = json.modal.type;
            var swalAjaxIcon = json.modal.icon;
            var swalAjaxTitle = json.modal.title;
            var swalAjaxUrlRedirect = json.modal.urlRedirect;

            if (json.erro == 0) {

                $('button.confirm').on('click', function () {
                    location.href = swalAjaxUrlRedirect;
                    return false;
                });

            } else {
                if (form != false) {
                    form.find('button.confirm').removeAttr('disabled');
                }
            }
            swal(swalAjaxTitle, swalAjaxMessage, swalAjaxType);

            $('button.confirm').on('click', function () {
                return false;
            });
        });

    });

    return false;
});

$(document).on('click', '.alert-confirm-devolver', function (e) {

    e.preventDefault();

    var form = $('form');
    var href = $(this).prop('href');
    var objRegistro = $('#tr' + $(this).attr('target-id'));

    swal({
        title: "Você tem certeza que deseja devolver os produtos?",
        text: "Essa operação não poderá ser desfeita.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        cancelButtonText: "Não, cancelar",
        confirmButtonText: "Sim, tenho certeza!",
        closeOnConfirm: false
    }, function () {
        $.post(href, function (data) {
            var json = JSON.parse(data);

            var swalAjaxMessage = json.modal.message;
            var swalAjaxType = json.modal.type;
            var swalAjaxIcon = json.modal.icon;
            var swalAjaxTitle = json.modal.title;
            var swalAjaxUrlRedirect = json.modal.urlRedirect;

            if (json.erro == 0) {

                $('button.confirm').on('click', function () {
                    location.href = swalAjaxUrlRedirect;
                    return false;
                });

            } else {
                if (form != false) {
                    form.find('button.confirm').removeAttr('disabled');
                }
            }
            swal(swalAjaxTitle, swalAjaxMessage, swalAjaxType);

            $('button.confirm').on('click', function () {
                return false;
            });
        });

    });

    return false;
    
    
    
});