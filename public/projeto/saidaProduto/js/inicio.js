$(document).ready(function () {

    $('#DATA_FINAL').bootstrapMaterialDatePicker({format: 'DD/MM/YYYY',
        clearButton: true,
        weekStart: 1,
        time: false
    }).on('change', function (e, date)
    {
        $('#DATA_INICIAL').bootstrapMaterialDatePicker('setMaxDate', date);
    });

    $('#DATA_INICIAL').bootstrapMaterialDatePicker({format: 'DD/MM/YYYY',
        clearButton: true,
        weekStart: 1,
        time: false}).on('change', function (e, date)
    {
        $('#DATA_FINAL').bootstrapMaterialDatePicker('setMinDate', date);
    });


 $('.js-exportable').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        "order":false,
        "columnDefs": [
//            {"width": "30px", "targets": 0},
            {"orderable": false, "targets": 0}
        ],
        buttons: [
            {extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            },
            {extend: 'csv',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            },
            {extend: 'excel',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            },
            {extend: 'pdf',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            },
            {extend: 'print',
                text: 'Imprimir Relação',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            }
        ]

    })

});




