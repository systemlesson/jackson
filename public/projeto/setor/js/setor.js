$(document).ready(function () {

    $('label').append('<sup class="col-red">*</sup>');
    $('.no-red').find('.col-red').html('');

    var form_val = $('#frmAdicionar');
    form_val.bootstrapValidator({
        message: 'O valor informado não é válido!',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            NOME: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            }
            
        }//fields
    })
    
    
            .on('success.form.bv', function (e) {
                $('.btn-salvar').attr('disabled', 'disabled');
                e.preventDefault();
                var $form = $(e.target);
                var metodo = $form.attr('href');
                $.post(metodo, $form.serialize(), function (data) {
                    try {
                        modalAlert(data, $form);
                    } catch (e) {
                        console.log("erro ajax");
                    }
                });
            });
    
    
    $('.multiSelect').multiSelect({
        selectableOptgroup: true,
        selectableHeader: "<div class='custom-header'>Recurso(s) Disponível(is):</div>",
        selectionHeader: "<div class='custom-header'>Recurso(s) Selecionado(s):</div>"

    });


});