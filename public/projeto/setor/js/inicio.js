$(document).ready(function () {



    $('.js-exportable').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        "order": [[1, "asc"]],
        "columnDefs": [
//            {"width": "30px", "targets": 0},
            {"orderable": false, "targets": 0}
        ],
        buttons: [
            {extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [1, ]
                }
            },
            {extend: 'csv',
                exportOptions: {
                    columns: [1, ]
                }
            },
            {extend: 'excel',
                exportOptions: {
                    columns: [1, ]
                }
            },
            {extend: 'pdf',
                exportOptions: {
                    columns: [1, ]
                }
            },
            {extend: 'print',
                text: 'Imprimir Relação',
                exportOptions: {
                    columns: [1, ]
                }
            }
        ]

    });


    
});
