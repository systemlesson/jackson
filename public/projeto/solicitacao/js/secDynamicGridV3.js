var list = {view: [], dados: []};
var idLoadData = null;


function LoadlistaInit(array) {
    if (listaInit.dados.length > 0) {
        list = listaInit;
        setList(list);
    }
}

/**
 * Seta os campos do form na Grid
 * @param {object} grid
 * @returns {void}
 */

function setList(list) {
    var linha = '';

    $.each(list.view, function (key, obj) {
           if(obj.EXCLUIDO == 0){
                linha += '<tr >';
                linha += '<td class="align-center" id="">    <div class="btn-group tableAction">        <button type="button" class="btn btn-default waves-effect dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">keyboard_arrow_down</i></button>         <ul class="dropdown-menu ">';
                linha += '<li><a style="cursor: pointer;" onclick="loadData(' + key + ');"><i class="material-icons bg-orange padding-5 ">create</i>&nbsp;Alterar</a></li>';
                linha += '<li><a style="cursor: pointer;" onclick="deleteData(' + key + ');"><i class="material-icons bg-red padding-5 ">delete_forever</i>&nbsp;Excluir</a></li>';
                linha += '</ul></div> </td>';
                for (var key in obj) {
                    if (obj[key] == null) {
                        obj[key] = '';
                    }
                    if(key != "EXCLUIDO" && key != "ID_ITEM_SOLICITACAO_PRODUTO"){
                        linha += '<td>' + obj[key] + '</td>';
                    }
                }
                linha += '</tr>';
            }
                
    });
    
    tableForm.destroy();
    $('#listTable').html(linha);
    $('.selectpicker').selectpicker('refresh');
    geraTable();
}

function addData() {
    var campos = $('.form-grid input,.form-grid select'); //Inputs que irão ser adicionado ao grid
    var camposDados = []; //dados em branco
    var camposView = []; //dados em branco

    campos.each(function (index, element) {

        var id = $(this).attr('id');
        if (id != undefined) {
            //Só adiciona aos campos dados os que tiverem id            
            var valDados = $(this).val();
            var valView = $(this).val();
            if (this.tagName == 'SELECT') {
                var valView = $(this).find('option:selected').text();
            }

            camposDados[id] = valDados;
            camposView[id] = valView;

            if ($(this).attr('data-valida') != undefined) {
                //revalidação do campos
                revalid(id);
            }
        }
    });

    if ($('.form-grid div.has-error').length === 0) {
        list.dados.push(camposDados);
        list.view.push(camposView);
        setList(list);
        $('.form-grid input').val('');
        $('.form-grid select').val('');
        $('#EXCLUIDO').val('0');
        $('.selectpicker').selectpicker('refresh');

    }
}

function updateData() {
    var campos = $('.form-grid input,.form-grid select'); //Inputs que irão ser adicionado ao grid
    var camposDados = []; //dados em branco
    var camposView = []; //dados em branco

    campos.each(function (index, element) {

        var id = $(this).attr('id');
        if (id != undefined) {            
            //Só adiciona aos campos dados os que tiverem id            
            var valDados = $(this).val();
            var valView = $(this).val();
            if (this.tagName == 'SELECT') {
                var valView = $(this).find('option:selected').text();
            }

            camposDados[id] = valDados;
            camposView[id] = valView;

            if ($(this).attr('data-valida') != undefined) {
                //revalidação do campos
                revalid(id);
            }
        }
        
    });

    if ($('.form-grid div.has-error').length === 0 && idLoadData !== null) {
        list.dados[idLoadData] = camposDados;
        list.view[idLoadData] = camposView;
        setList(list);
        idLoadData = null;
        $('.form-grid input').val('');
        $('.form-grid select').val('');
        $('.form-grid .data-block-insert').show();
        $('.form-grid .data-block-update').hide();
    }
    $('.selectpicker').selectpicker('refresh');
    $('.multiple').multiSelect('refresh');

    
}




function revalid(id) {
    $('form').data('bootstrapValidator').enableFieldValidators(id, true, 'notEmpty');
    $('form').data('bootstrapValidator').revalidateField(id);
}

function deleteData(id) {
   
   var item = list.dados[id];

   //Vem do banco
   if(item.ID_ITEM_SOLICITACAO_PRODUTO != undefined){
      list.view[id].EXCLUIDO =  '1'; //exclui da view
      list.dados[id].EXCLUIDO = '1'; // coloca nos dados como excluido para atualizar no controller
   }
   
   //Vem da view e n da persistência
   if(item.ID_ITEM_SOLICITACAO_PRODUTO == undefined || item.ID_ITEM_SOLICITACAO_PRODUTO == ""){
       list.view.splice(id,1); 
       list.dados.splice(id,1);
   }
   
   setList(list);
   $('form').data('bootstrapValidator').resetForm();
   return false;
}



function loadData(id) {

    var campos = $('.form-grid input,.form-grid select');
    var dadoSelecionado = list.dados[id];
    $('.selectpicker').selectpicker('refresh');


    campos.each(function (index, element) {
        $(this).val(dadoSelecionado[$(this).attr('name')]);
    });


    
    $('.form-grid .data-block-insert').hide();
    $('.form-grid .data-block-update').show();

    idLoadData = id;    
    $('form').data('bootstrapValidator').resetForm();
    $('.selectpicker').selectpicker('refresh');
    return false;
}

function reset() {

    idLoadData = null;
    $('.form-grid input').val('');
    $('.form-grid  select').val('');
    $('.selectpicker').selectpicker('refresh');
    $('form').data('bootstrapValidator').resetForm();
    $('.form-grid .data-block-insert').show();
    $('.form-grid .data-block-update').hide();

}
    

function geraTable(){
    tableForm = $('.js-exportable').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        "order": [[1, "asc"]],
        "paging": false, //desabilita paginação
        "searching": false,//desabilita pesquisa
        "ordering": true, // disabilita ordenamento        
        buttons: []

    })
}
    

$(document).ready(function () {
    $(document).on('click', '.form-grid .btn-add-item', function () {
        addData();
    });

    $(document).on('click', '.form-grid .btn-update-item', function () {
        updateData();
    });

    $(document).on('click', '.form-grid .btn-cancel-update-item', function () {
        reset();
    });

    $(document).on('click', '.btn-add-limpar', function () {
        reset();
//        $('form').data('bootstrapValidator').enableFieldValidators('NUMERO_AULA', false, 'notEmpty');    
//        $('form').data('bootstrapValidator').enableFieldValidators('CONTEUDO', false, 'notEmpty');    
        
    });


     geraTable();
     LoadlistaInit();
     
});


