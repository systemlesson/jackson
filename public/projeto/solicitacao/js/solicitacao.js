$(document).ready(function () {
    $('.no-red').find('.col-red').html('');

    $('.pg-adicionar form,.pg-alterar form').bootstrapValidator({
        message: 'O valor informado não é válido!',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ID_FUNCIONARIO_USUARIO: {
                validators: {
                    notEmpty: {
                        enabled: false,
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            QUANTIDADE: {
                validators: {
                    notEmpty: {
                        enabled: false,
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            ID_PRODUTO: {
                validators: {
                    notEmpty: {
                        enabled: false,
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            SITUACAO: {
                validators: {
                    notEmpty: {
                        enabled: false,
                        message: SYSTEM_MSG.MSG1
                    }
                }
            }

        }//fields
    }).on('success.form.bv', function (e) {


        $('.btn-salvar').attr('disabled', 'disabled');
        e.preventDefault();

        //////////////////Tratando dados

        var $form = $(e.target);
        //Tratando dados

        var dadosForm = $('#frmAdicionar').serializeArray();
        //Retirando dados do array

        for (var i = 0; i < dadosForm.length; i++) {
            if (dadosForm[i].name == "EXCLUIDO") {
                dadosForm.splice(i, 1);
            }
            if (dadosForm[i].name == "ID_PRODUTO") {
                dadosForm.splice(i, 1);
            }
            if (dadosForm[i].name == "QUANTIDADE") {
                dadosForm.splice(i, 1);
            }
            if (dadosForm[i].name == "ID_ITEM_SOLICITACAO_PRODUTO") {
                dadosForm.splice(i, 1);
            }
        }

        //Criando grid
        var listaDados = window.list.dados;

        var item = {};
        var itens = [];
        for (i = 0; i < listaDados.length; i++) {
            item = {
                ID_ITEM_SOLICITACAO_PRODUTO: listaDados[i]['ID_ITEM_SOLICITACAO_PRODUTO'],
                ID_PRODUTO: listaDados[i]['ID_PRODUTO'],
                EXCLUIDO: listaDados[i]['EXCLUIDO'],
                QUANTIDADE: listaDados[i]['QUANTIDADE']
            };
            itens.push(item);
        }
        var dadosPost = {
            'gridSolicitacao': itens
        };


        ////\\\*** MAGIC ORGANIZA ARRAY***///\\\ 
        var index = 0;
        var nameAnterior = null;
        for (i = 0; i < dadosForm.length; i++) {
            if (dadosForm[i].name.indexOf('[]') != -1) {
                if (nameAnterior === dadosForm[i].name) {
                    index++;
                } else {
                    index = 0;
                }
                var aux = dadosForm[i].name.replace('[]', '');
                dadosPost[[aux] + '[' + index + ']'] = dadosForm[i].value;
            } else {
                index = 0;
                dadosPost[dadosForm[i].name] = dadosForm[i].value;
            }
            nameAnterior = dadosForm[i].name;
        }
        ////\\\*** FIM MAGIC ***///\\\


        //{ 'gridCronograma' : itens , 'acao' : acao , n : v }
        var metodo = $('#frmAdicionar').attr('action');

        $.ajax({
            method: "POST",
            data: dadosPost,
            async: false,
            url: metodo,
            success: function (data) {
                try {
                    modalAlert(data, $form);
                } catch (e) {
                    console.log("erro ajax");
                }
            }
        });



    });


});