function desabilitaNTE(){
    $('form').data('bootstrapValidator').enableFieldValidators( 'ID_NTE'  , false, 'notEmpty'); 
    $('form').data('bootstrapValidator').enableFieldValidators( 'IDMUNICIPIO'  , false, 'notEmpty'); 
    $('form').data('bootstrapValidator').enableFieldValidators( 'ESC_COD_SEC'  , false, 'notEmpty'); 
    
    $('[for=ID_NTE]').find('col-red').text('');
    $('[for=IDMUNICIPIO]').find('col-red').text('');
    $('[for=ESC_COD_SEC]').find('col-red').text('');
}

function habilitaNTE(){
    $('form').data('bootstrapValidator').enableFieldValidators( 'ID_NTE'  , true, 'notEmpty'); 
    $('form').data('bootstrapValidator').enableFieldValidators( 'IDMUNICIPIO'  , true, 'notEmpty'); 
    $('form').data('bootstrapValidator').enableFieldValidators( 'ESC_COD_SEC'  , true, 'notEmpty'); 
    
    $('[for=ID_NTE]').find('col-red').text('*');
    $('[for=IDMUNICIPIO]').find('col-red').text('*');
    $('[for=ESC_COD_SEC]').find('col-red').text('*');
}



$(document).ready(function () {

    $('[for=ID_NTE]').append('<sup class="col-red">*</sup>');
    $('[for=IDMUNICIPIO]').append('<sup class="col-red">*</sup>');
    $('[for=ESC_COD_SEC]').append('<sup class="col-red">*</sup>');

    $('#CPF').mask('999.999.999-99');


    $('.pg-listar form')
            .bootstrapValidator({
                message: 'O valor informado não é válido!',
                //live: 'submitted',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    CPF:{
                      validators:{
                           callback: {
                                callback: function (value, validator, $field) {                    
                                    if (value === '') {
                                      if($('#ROLE_ID').val() == ''){
                                           habilitaNTE(); 
                                      }else{
                                           desabilitaNTE();                                                                                                                         
                                      }
                                     return true;
                                    }
                                    
                                    if(validaCPF(value)){
                                       desabilitaNTE();
                                       return true;                                       
                                    }else{
                                       
                                        desabilitaNTE();
                                        var result = {
                                           valid:false,
                                           message:'CPF inválido!'
                                       }                                       
                                       return result;
                                       
                                    }
                                }
                            } 
                      }  
                    },
                    ROLE_ID:{
                      validators:{
                           callback: {
                                callback: function (value, validator, $field) {                    
                                    
                                    if(value == 1 ){
                                        desabilitaNTE();
                                        return true;                                       
                                    }else{
                                        
                                      if($('#CPF').val() == ''){
                                           habilitaNTE(); 
                                      }else{
                                           desabilitaNTE();                                                                                                                         
                                      }
                                      return true;
                                    }
                                    
                                                                        
                                }
                            } 
                      }  
                    },
                    ID_NTE: {
                        validators: {
                            notEmpty: {
                                message: 'Preencha este campo!'
                            }
                        }
                    },
                    IDMUNICIPIO: {
                        validators: {
                            notEmpty: {
                                message: 'Preencha este campo!'
                            }
                        }
                    },
                    ESC_COD_SEC: {
                        validators: {
                            notEmpty: {
                                message: 'Preencha este campo!'
                            }
                        }
                    },
                }
            })
            .on('success.form.bv', function (e) {
                return true;
            });


    $('.js-exportable').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        "order": [[1, "asc"]],
        "columnDefs": [
//            {"width": "30px", "targets": 0},
            {"orderable": false, "targets": 0}
        ],
        buttons: [
            {extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            },
            {extend: 'csv',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            },
            {extend: 'excel',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            },
            {extend: 'pdf',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            },
            {extend: 'print',
                text: 'Imprimir Relação',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            }
        ]

    });


    
});
