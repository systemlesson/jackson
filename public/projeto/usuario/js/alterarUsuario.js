  function verificaVinculoCpfEstado(cpf) {      

          $.ajax({
            async: false,
            type: 'POST',
            url: '/usuariosistema/getPerfisDisponiveisNomeNte/',
            data: {cpf: cpf},
            dataType: 'json',
            beforeSend: function (xhr) {
                pageLoaderShow('Carregando dados...');           
            }, complete: function (result) {
                pageLoaderHide();                           
            },
            success: function (result) {
                   retorno = {
                        error : true,
                        message : "Não foi possível verificar o CPF!",
                        dados   : {}
                    }
                
                //se tiver algum erro executa essa linha e ignora o que estiver abaixo e exibe a mensagem que vem do servidor
                if (result.error == true || result.error == undefined) {
                    if(retorno.message != undefined ){
                        retorno.message = result.message
                    }                    
                    return retorno;
                }
                
                pageLoaderHide();                                                        
                retorno.error = false;
                retorno.message = ''
                retorno.dados = result;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log('Ocorreu um erro!');
            }
        })
        
                        
       return retorno;
    }
    
    
    function carregarCampos(dados){
        
        var elementoNome = $('#NOME');
        if(dados.nomeServidor == null){
            elementoNome.val('');
            elementoNome.removeAttr('disabled');
        }else{
            elementoNome.val(dados.nomeServidor);
            elementoNome.attr('disabled','disabled');         
        }
        
        $('#EMAIL').removeAttr('disabled');
        
        var elementoNte =  $('#ID_NTE');
        if(dados.possueNteVinculada == true){
            elementoNte.val('');
            elementoNte.attr('disabled','disabled');         
            $('.box-nte').hide();
        }else{            
            elementoNte.removeAttr('disabled');            
            var opt =  "<option value=''> Selecione... </option>";
            $.each(dados.ntes,function(key,value){
                opt+="<option value='"+key+"' >"+value+"</option>"
            });
            
            elementoNte.html(opt);         
            $('.selectpicker').selectpicker('refresh');
            $('.box-nte').show();
            
        }
        
        
        
        //PERFIS    
        var elementoPerfil  =$('#PERFIS');
        elementoPerfil.removeAttr('disabled');
        var opt = '';
        $.each( dados.perfisDisponiveis ,function(key,value){
            opt+="<option value='"+key+"' >"+value+"</option>"
        });
        elementoPerfil.html(opt);         
        $('.selectpicker').selectpicker('refresh');
        $('.multiple').multiSelect('refresh');
        
    }
    
function checkPerfilSel(perfil){
    if(perfil == 1){
        var elementoNte =  $('#ID_NTE');
        elementoNte.val('');
        elementoNte.attr('disabled','disabled');         
        $('.box-nte').hide();
    }
}   

function checkPerfilDes(perfil){
    console.log(perfil);
    if(perfil == 1){
            var elementoNte =  $('#ID_NTE');
            elementoNte.removeAttr('disabled');            
            $('.selectpicker').selectpicker('refresh');
            $('.box-nte').show();
    }
}   

$(document).ready(function () {

    $('#CPF').mask('999.999.999-99');

    $('[for=CPF]').append('<sup class="col-red">*</sup>');
    $('[for=NOME]').append('<sup class="col-red">*</sup>');
    $('[for=EMAIL]').append('<sup class="col-red">*</sup>');
    $('[for=PERFIS]').append('<sup class="col-red">*</sup>');

   
    $('.pg-adicionar form,.pg-alterar form')
            .bootstrapValidator({
                message: 'O valor informado não é válido!',
                //live: 'submitted',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    CPF: {
                        validators: {
                            callback: {
                                callback: function (value, validator, $field) {
                                    
                                    var retorno = {
                                        valid: false,    // or false
                                        message: SYSTEM_MSG.MSG1
                                    }
                                    
                                    //verifica se está em branco
                                    if(value == ''){
                                        return retorno;
                                    }
                                    //verifica cpf está em sistema.js
                                    if(validaCPF(value)){
                                         //trás as informações do banco
                                         var verifica = verificaVinculoCpfEstado(value);
                                         if(!verifica.error){                                             
                                             carregarCampos(verifica.dados[0]);
                                             $('form').data('bootstrapValidator').resetForm();
                                             retorno.valid = true;
                                             retorno.message = '';
                                             return retorno;
                                         }else{
                                             retorno.message = verifica.message;
                                             return retorno;
                                         }
                                    }else{
                                        retorno.message = SYSTEM_MSG.MSG18;
                                        return retorno;
                                    }                                    
                                }
                            }
                        }
                    },
                    NOME: {
                        validators: {
                            notEmpty: {
                                message: SYSTEM_MSG.MSG1
                            }
                        }
                    },
                    EMAIL: {
                        validators: {
                            notEmpty: {
                                message: SYSTEM_MSG.MSG1
                            },
                            emailAddress: {
                                message: SYSTEM_MSG.MSG19
                            }

                        },
                    },
                    ID_NTE: {
                        validators: {
                            notEmpty: {
                                message: SYSTEM_MSG.MSG1
                            }
                        }
                    },'PERFIS[]': {
                        validators: {
                            notEmpty: {
                                message: SYSTEM_MSG.MSG1
                            }
                        }
                    }
                }
            })
            .on('success.form.bv', function (e) {

                $('.btn-salvar').attr('disabled', 'disabled');
                e.preventDefault();
                var $form = $(e.target);
                var metodo = $form.attr('action');
                $.post(metodo, $form.serialize(), function (data) {
                    modalAlert(data, $form);
                });
            });
  

  
    
    
    
    
});
