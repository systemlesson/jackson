$(document).ready(function () {

    //Exportable table
    $('.js-exportable').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        "order": [[1, "asc"]],
        "columnDefs": [
//            {"width": "30px", "targets": 0},
            {"orderable": false, "targets": 0}
        ],
        buttons: [
            {extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            },
            {extend: 'csv',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            },
            {extend: 'excel',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            },
            {extend: 'pdf',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            },
            {extend: 'print',
                text: 'Imprimir',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6]
                }
            }
        ]

    });


    $('.pg-listar form')
            .bootstrapValidator({
                message: 'O valor informado não é válido!',
                //live: 'submitted',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    EMAIL: {
                        validators: {
                            emailAddress: {
                                message: 'E-mail Inválido!'
                            }
                        }
                    },
                    ID_PERFIL: {
                        validators: {
                            notEmpty: {
                                message: 'Preencha este campo!'
                            }
                        }
                    },
                    ID_NTE: {
                        validators: {
                            notEmpty: {
                                message: 'Preencha este campo!'
                            }
                        }
                    }
                }
            })
            .on('success.form.bv', function (e) {
                return true;
            });

    $('[for=ID_NTE], [for=ID_PERFIL]').append('<sup class="col-red">*</sup>');
    $('#CPF').mask('999.999.999-99');

});
