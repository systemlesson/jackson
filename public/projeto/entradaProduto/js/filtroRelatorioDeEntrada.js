$(document).ready(function () {

    $('#DATA_FINAL').bootstrapMaterialDatePicker({format: 'DD/MM/YYYY',
        clearButton: true,
        weekStart: 1,
        time: false
    }).on('change', function (e, date)
    {
        $('#DATA_INICIAL').bootstrapMaterialDatePicker('setMaxDate', date);
    });

    $('#DATA_INICIAL').bootstrapMaterialDatePicker({format: 'DD/MM/YYYY',
        clearButton: true,
        weekStart: 1,
        time: false}).on('change', function (e, date)
    {
        $('#DATA_FINAL').bootstrapMaterialDatePicker('setMinDate', date);
    });




});




