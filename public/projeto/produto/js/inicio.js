$(document).ready(function () {
    $('#CODIGO').mask('9999999');


    $('.js-exportable').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        "order": [[1, "asc"]],
        "columnDefs": [
//            {"width": "30px", "targets": 0},
            {"orderable": false, "targets": 0}
        ],
        buttons: [
            {extend: 'copy',
                text: 'Copiar',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6,7]
                }
            },
            {extend: 'csv',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6,7]
                }
            },
            {extend: 'excel',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6,7]
                }
            },
            {extend: 'pdf',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6,7]
                }
            },
            {extend: 'print',
                text: 'Imprimir Relação',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6,7]
                }
            }
        ]

    })

    



});
