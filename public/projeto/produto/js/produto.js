$(document).ready(function () {
    $('#CODIGO').mask('9?9999999');
    $('#QUANTIDADE').mask('99999999');
    $('label').append('<sup class="col-red">*</sup>');
    $('.no-red').find('.col-red').html('');
    

    $('.pg-adicionar form,.pg-alterar form').bootstrapValidator({
        message: 'O valor informado não é válido!',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            CODIGO: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
             NOME: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            DESCRICAO: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            ID_UNIDADE: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            ID_CATEGORIA: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            }

        }//fields
    })
            .on('success.form.bv', function (e) {
                $('.btn-salvar').attr('disabled', 'disabled');
                e.preventDefault();
                var $form = $(e.target);
                var metodo = $form.attr('href');
                $.post(metodo, $form.serialize(), function (data) {
                    try {
                        modalAlert(data, $form);
                    } catch (e) {
                        console.log("erro ajax");
                    }
                });
            });
    
//    $('[data-popover-salvar]').popover({
//        title:'Salvar',
//        trigger:'hover',
//        html:true,
//        content:'Ceteza que deseja  ? <b>"salvar"</b>',
//        placement:'top'
//    });
    
});