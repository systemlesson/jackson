$(document).ready(function () {

    $('#MATRICULA').mask('99?999');
    $('#TELEFONE').mask('(99) 9999-9999');
    $('#CELULAR').mask('(99) 99999-9999');
    $('#CPF').mask('999.999.999-99');

    $('label').append('<sup class="col-red">*</sup>');
    $('.no-red').find('.col-red').html('');


    $('.pg-adicionar form,.pg-alterar form').bootstrapValidator({
        message: 'O valor informado não é válido!',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            NOME: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            TELEFONE: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            CELULAR: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    }
                }
            },
            ID_SETOR: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG08
                    },
                }
            },
            CPF: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG1
                    },
                    cpfVal: {
                        message: SYSTEM_MSG.MSG18
                    }
                }
            },
            TIPO_USUARIO: {
                enabled: true,
                validators: {
                    notEmpty: {
                        message: SYSTEM_MSG.MSG08
                    }
                }
            },
            MATRICULA: {
                enabled: true,
                validators: {
                     notEmpty: {
                        message: SYSTEM_MSG.MSG08
                    }
                }
            },
             PASSOWORD: {
                enabled: true,
                validators: {
                     notEmpty: {
                        message: SYSTEM_MSG.MSG08
                    }
                }
            }

        }//fields
    })

            .on('success.form.bv', function (e) {
                $('.btn-salvar').attr('disabled', 'disabled');
                e.preventDefault();
                var $form = $(e.target);
                var metodo = $form.attr('href');
                $.post(metodo, $form.serialize(), function (data) {
                    pageLoaderHide();
                    modalAlert(data, $form);
                });
            });


    $('.multiSelect').multiSelect({
        selectableOptgroup: true,
        selectableHeader: "<div class='custom-header'>Recurso(s) Disponível(is):</div>",
        selectionHeader: "<div class='custom-header'>Recurso(s) Selecionado(s):</div>"

    });

});