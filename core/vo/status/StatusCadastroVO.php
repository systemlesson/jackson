<?php

namespace core\vo\status;

/**
 * Classe Modelo de Status e Cadastro
 */
class StatusCadastroVO {

    private $id;
    private $descricao;

    function getId() {
        return $this->id;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

}
