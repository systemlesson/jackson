<?php

namespace core\vo\rasea;

use core\vo\rasea\RoleVO;

class UserAssignmentVO {

    private $roleId;
    private $username;
    
    
    function __construct() {
        $this->roleId = new RoleVO();
    }

    function getRoleId() {
        return $this->roleId;
    }

    function getUsername() {
        return $this->username;
    }

    function setRoleId($roleId) {
        $this->roleId = $roleId;
    }

    function setUsername($username) {
        $this->username = $username;
    }



}
