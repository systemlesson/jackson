<?php

namespace core\vo\rasea;

use core\vo\rasea\OperationVO;
use core\vo\rasea\ResourceVO;

class PermissionVO {

    private $operationId;
    private $resourceId;
    
    
    function __construct() {
        $this->operationId = new OperationVO();
        $this->resourceId = new ResourceVO();
    }
    
    function getOperationId() {
        return $this->operationId;
    }

    function getResourceId() {
        return $this->resourceId;
    }

    function setOperationId($operationId) {
        $this->operationId = $operationId;
    }

    function setResourceId($resourceId) {
        $this->resourceId = $resourceId;
    }



   

}

