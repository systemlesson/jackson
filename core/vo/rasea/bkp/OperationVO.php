<?php

namespace core\vo\rasea;

class OperationVO {

    private $id;
    private $name;
    private $applicationId;

    function __construct() {
        $this->applicationId = new ApplicationVO();
    }
    
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getApplicationId() {
        return $this->applicationId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setApplicationId($applicationId) {
        $this->applicationId = $applicationId;
    }



}
