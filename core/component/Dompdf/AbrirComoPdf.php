<?php
namespace core\component\Dompdf;

use core\component\Dompdf\src\Dompdf;
use core\component\Dompdf\src\Options;


require_once __DIR__ . '\autoload.inc.php';

/**
 * Description of domPdf
 *
 * @author fabiosantana.santos
 */
class AbrirComoPdf {

    
    
    static public function domPdf($html) {
        
        try {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);

            $dompdf->setPaper('A4', 'landscape');

            $options = new Options();
            $options->setIsHtml5ParserEnabled('isHtml5ParserEnabled', true);
            $dompdf->set_option('isHtml5ParserEnabled', true);
            
            
            $dompdf->render();

            $dompdf->stream('nomeDoArquivo', array(
                "Attachment" => false));
        } catch (Exception $e) {
            
            var_dump($e);die;
// Do something here with $e and notify the user of the error in whatever way you see fit
        }
    }
}