<?php

namespace core\helper;

/**
 * Classe utilitária de Data
 * @package core
 * @subpackage helper
 * @author Judá Passos <juda.santos@educacao.ba.gov.br>
 */
class DataHelper {

    /**
     * Verifica se uma data é válida
     * @static
     * @access public
     * @param string $data data a ser validada
     * @param string $separador Separador utilizado
     * @return boolean True para válido e False para inválido
     */
    public static function isValid($data, $separador = '/') {
        $data = explode($separador, $data);
        return checkdate($data[1], $data[0], $data[2]);
    }

    /**
     * 
     * @param string $cod
     * @return string
     */
    public static function mesPorExtenso($mes) {


        switch ($mes) {
            case "01": $mes = 'Janeiro';
                break;
            case "02": $mes = 'Fevereiro';
                break;
            case "03": $mes = 'Março';
                break;
            case "04": $mes = 'Abril';
                break;
            case "05": $mes = 'Maio';
                break;
            case "06": $mes = 'Junho';
                break;
            case "07": $mes = 'Julho';
                break;
            case "08": $mes = 'Agosto';
                break;
            case "09": $mes = 'Setembro';
                break;
            case "10": $mes = 'Outubro';
                break;
            case "11": $mes = 'Novembro';
                break;
            case "12": $mes = 'Dezembro';
                break;
        }

        return $mes;
    }

    /**
     * Semana por Extenso
     * @param Integer $semana
     * @return type
     */
    public static function semanaPorExtenso($semana) {


        switch ($semana) {
            case 0: $semana = 'Domingo';
                break;
            case 1: $semana = 'Segunda-Feira';
                break;
            case 2: $semana = 'Terca-Feira';
                break;
            case 3: $semana = 'Quarta-Feira';
                break;
            case 4: $semana = 'Quinta-Feira';
                break;
            case 5: $semana = 'Sexta-Feira';
                break;
            case 6: $semana = 'Sábado';
                break;
        }

        return $semana;
    }

    /**
     * Método responsável por validar uma data específica
     * @access public
     * @param  $data data a ser validada
     * @param  $msg mensagem de exceção
     * @throws Exception
     * @return void
     */
    public static function validarData($data, $msg) {
        if (empty(preg_match("/([0-2][0-9]|3[0-1])\/[0-1][0-9]\/[0-9]{4}/", $data))) {
            throw new Exception($msg);
        }
    }

    /**
     * Converte String para DateTime
     * @static
     * @access public
     * @param string $data String no formato dd/mm/yyyy a ser convertida 
     *  @return \DateTime
     */
    public static function strToDateTime($data) {
        $match = [];
        preg_match("/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/", $data, $match);
        return \DateTime::createFromFormat('d/m/Y', $match[0]);
    }

    /**
     * Converte DateTime para String
     * @static
     * @access public
     * @param string $data String no formato DD/MM/YYYY
     *  @return \String 
     */
    public static function dataPorExtenso($data) {
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        return strftime('%d de %B de %Y', strtotime($data));
    }

    /**
     * Compara se data 1 é maior que a data 2
     * @static
     * @access public
     * @param string $data1 String no formato DD/MM/YYYY
     * @param string $data2 String no formato DD/MM/YYYY
     *  @return \String 
     */
    
    public static function comparaDataMaior($date1, $date2, $formateDate1 = 'd/m/Y H:i:s', $formateDate2 = 'd/m/Y H:i:s') {
        //$timeZone = new DateTimeZone('America/Recife');  

        $date1 = \DateTime::createFromFormat($formateDate1, $date1);
        $date2 = \DateTime::createFromFormat($formateDate1, $date2);
        

        if (!($date1 instanceof \DateTime)) {
            throw new \Exception('Data de entrada invalida!!');
        }

        if (!($date2 instanceof \DateTime)) {
            throw new \Exception('Data de entrada invalida!!');
        }


        return $date1 > $date2;
    }

}
