<?php

namespace core\helper;

/**
 * Description of Manipulador
 *
 * @author JUDAPASSOS
 */
class DaoHelper {

    /**
     * Conexao
     * @var PDO $conexao
     * @access private
     */
    private $conexao;

    /**
     * Preparação
     * @var PDOStatement $stmt
     * @access private
     */
    private $stmt;

    /**
     * Comando Sql a ser executado
     * @var String $sql
     * @access private
     */
    private $sql;

    /**
     * Retorno do método
     * @var mixed $retorno
     * @access private
     */
    private $retorno;

    /**
     * Conjunto de valores da consulta
     * @var mixed $fetch
     * @access private
     */
    private $fetch;

    /**
     * Flah Auto Commit
     * @var mixed $autoCommit
     * @access private
     */
    private $autoCommit;

    function __construct() {
        $this->conexao = NULL;
        $this->stmt = NULL;
        $this->sql = NULL;
        $this->retorno['retornoOperacao'] = FALSE;
        $this->retorno['retornoMensagem'] = NULL;
        $this->retorno['retornoPaginacao'] = NULL;
        $this->autoCommit = true;
    }

    public function getFetch() {
        return $this->fetch;
    }

    public function setFetch($fetch) {
        $this->fetch = $fetch;
    }

    /**
     * Método seta a flag para auto commit
     * @final
     * @access protected
     * @param void
     * @return bool
     */
    final protected function getAutoCommit() {
        return $this->autoCommit;
    }

    /**
     * Método seta a flag para auto commit
     * @final
     * @access protected
     * @param bool
     * @return void
     */
    final protected function setAutoCommit($autoCommit) {
        $this->autoCommit = $autoCommit;
    }

    /**
     * @param void
     * @return PDO 
     */
    public function getConexao() {
        return $this->conexao;
    }

    /**
     * @param PDO $conexao 
     * @return void
     */
    public function setConexao($conexao) {
        $this->conexao = $conexao;
    }

    /**
     * @param void
     * @return PDOStatement 
     */
    public function getStmt() {
        return $this->stmt;
    }

    /**
     * @param PDOStatement $stmt 
     * @return void
     */
    public function setStmt($stmt) {
        $this->stmt = $stmt;
    }

    /**
     * @param void
     * @return String 
     */
    public function getSql() {
        return $this->sql;
    }

    /**
     * @param String $sql 
     * @return void
     */
    public function setSql($sql) {
        $this->sql = trim($sql);
    }

    public function getRetorno() {
        return $this->retorno;
    }

    public function setRetornoMensagem($retornoMensagem) {
        $this->retorno['retornoMensagem'] = $retornoMensagem;
    }

    public function setRetornoPaginacao($retornoPaginacao) {
        $this->retorno['retornoPaginacao'] = $retornoPaginacao;
    }

    public function setRetornoOperacao($retornoOperacao) {
        $this->retorno['retornoOperacao'] = $retornoOperacao;
    }

    public function getRetornoMensagem() {
        return $this->retorno['retornoMensagem'];
    }

    public function getRetornoOperacao() {
        return $this->retorno['retornoOperacao'];
    }

    public function getRetornoPaginacao() {
        return $this->retorno['retornoPaginacao'];
    }

    ## METODOS PARA OCI

    public function parse($sql) {

        $this->setStmt(oci_parse($this->getConexao(), $sql));
    }

    public function bindValue($param, $value, $demiliter = true) {

        if ($demiliter) {
            $value = " '" . $value . "' ";
        }

        $this->setSql(str_replace(" " . $param, " " . $value, $this->getSql()));
        $this->setSql(str_replace("(" . $param, "(" . $value, $this->getSql()));
    }

    public function execute($autocommit = true, $sql = null) {

        $this->parse($this->getSql());

        if ($autocommit) {
            if (false === @oci_execute($this->getStmt(), OCI_COMMIT_ON_SUCCESS)) {
                throw new \Exception('Erro de SQL -> ' .$this->getSql());
            }
        } else {
            if (false === @oci_execute($this->getStmt(), OCI_NO_AUTO_COMMIT)) {
                throw new \Exception('Erro de SQL -> ' . $this->getSql());
            }
        }
    }

    public function executeSql($sql = null, $autocommit = true) {

        $this->parse($sql);

        if ($autocommit) {
            if (false === @oci_execute($this->getStmt(), OCI_COMMIT_ON_SUCCESS)) {
                throw new \Exception($sql);
            }
        } else {
            if (false === @oci_execute($this->getStmt(), OCI_NO_AUTO_COMMIT)) {
                throw new \Exception($sql);
            }
        }
    }

    public function fetchAll() {
        oci_fetch_all($this->getStmt(), $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    public function fetch() {
        return oci_fetch_array($this->getStmt());
    }

    public function fetchOneOrNullResult() {
        $res = oci_fetch_array($this->getStmt());
        return empty($res) ? null : $res[0];
    }

    public function fetchAssoc() {
        return oci_fetch_assoc($this->getStmt());
    }

    public function commit() {
        return oci_commit($this->getConexao());
    }

    public function rollback() {
        return oci_rollback($this->getConexao());
    }

    ## METODOS PARA PAGINACAO 

    public function executeWithPagination($objVO) {
      
        $this->executeSql("select count(*) from ( " . $this->getSql() . " )");
        $total = $this->fetch()[0];
        $this->setRetornoPaginacao(array('TOTAL'=>$total,'DRAW'=>$objVO->getPaginacaoDraw()));
        
        $this->executeSql("SELECT *
                            FROM (SELECT ROWNUM rnum, a.*
                            FROM (
                                    ".$this->getSql()." 
                            ) a
                            WHERE ROWNUM <= ".$objVO->getPaginacaoRegistroInicio()." + ".$objVO->getPaginacaoRegistroTotalPorPagina().")
                        WHERE rnum >= ".$objVO->getPaginacaoRegistroInicio());
        
        
    }

}

?>
