<!-- SUCCESS SECTION -->
    <section class="clearfix padding success-section">
        <div class="container">
            <div class="sectionTitle text-center">
                <h3>Success Stories</h3>
            </div>

            <div class="owl-carousel success-inner">
                <div class="slide">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 text-center">
                            <div class="success_video">
                                <img src="img/home/course/video-img.jpg" data-video="https://www.youtube.com/embed/oOMcZoeEK0A?autoplay=1">
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 text-center">
                            <div class="slide-info">
                                <div class="carousal_content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate </p>
                                </div>
                                <div class="carousal_bottom">
                                    <div class="thumb">
                                        <img src="img/home/course/thumb1.png" alt="Image" draggable="false">
                                    </div>
                                    <div class="thumb_title">
                                        <span class="author_name">John Doe</span>
                                        <span class="author_designation">PHD Student<a href="#"> Apple Inc</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slide">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 text-center">
                            <div class="success_video">
                                <img src="img/home/course/video-img.jpg" data-video="https://www.youtube.com/embed/oOMcZoeEK0A?autoplay=1">
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 text-center">
                            <div class="slide-info">
                                <div class="carousal_content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate </p>
                                </div>
                                <div class="carousal_bottom">
                                    <div class="thumb">
                                        <img src="img/home/course/thumb1.png" alt="Image" draggable="false">
                                    </div>
                                    <div class="thumb_title">
                                        <span class="author_name">John Doe</span>
                                        <span class="author_designation">PHD Student<a href="#"> Apple Inc</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slide">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 text-center">
                            <div class="success_video">
                                <img src="img/home/course/video-img.jpg" data-video="https://www.youtube.com/embed/oOMcZoeEK0A?autoplay=1">
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 text-center">
                            <div class="slide-info">
                                <div class="carousal_content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate </p>
                                </div>
                                <div class="carousal_bottom">
                                    <div class="thumb">
                                        <img src="img/home/course/thumb1.png" alt="Image" draggable="false">
                                    </div>
                                    <div class="thumb_title">
                                        <span class="author_name">John Doe</span>
                                        <span class="author_designation">PHD Student<a href="#"> Apple Inc</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>