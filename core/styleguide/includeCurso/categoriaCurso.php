<!-- COURSE CATEGORY -->
    <section class="courseCategory padding paralax" style="background-image: url(img/home/paralax/paralax03a.jpg);">
        <div class="container">
            <div class="sectionTitle text-center">
                <h3>Course Categories</h3>
            </div>

            <div class="row">
                <div class="col-sm-4 col-xs-6">
                    <div class="text-box">
                        <div class="text-box-icon">
                            <i class="fa fa-paint-brush" aria-hidden="true"></i>
                        </div>
                        <div class="text-box-top">
                            <h4><a href="">Design</a></h4>
                            <a class="courseNo" href="">10 Course</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-xs-6">
                    <div class="text-box">
                        <div class="text-box-icon">
                            <i class="fa fa-music" aria-hidden="true"></i>
                        </div>
                        <div class="text-box-top">
                            <h4><a href="">Music</a></h4>
                            <a class="courseNo" href="">8 Course</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-xs-6">
                    <div class="text-box">
                        <div class="text-box-icon">
                            <i class="fa fa-gamepad" aria-hidden="true"></i>
                        </div>
                        <div class="text-box-top">
                            <h4><a href="">Gaming</a></h4>
                            <a class="courseNo" href="">6 Course</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-xs-6">
                    <div class="text-box">
                        <div class="text-box-icon">
                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                        </div>
                        <div class="text-box-top">
                            <h4><a href="">Technology</a></h4>
                            <a class="courseNo" href="">9 Course</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-xs-6">
                    <div class="text-box">
                        <div class="text-box-icon">
                            <i class="fa fa-code" aria-hidden="true"></i>
                        </div>
                        <div class="text-box-top">
                            <h4><a href="">Development</a></h4>
                            <a class="courseNo" href="">12 Course</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-xs-6">
                    <div class="text-box">
                        <div class="text-box-icon">
                            <i class="fa fa-camera-retro" aria-hidden="true"></i>
                        </div>
                        <div class="text-box-top">
                            <h4><a href="">Photography</a></h4>
                            <a class="courseNo" href="">4 Course</a>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btnArea text-center">
                <a href="" class="btn btn-default commonBtn">View more</a>
            </div>
        </div>
    </section>