<!-- BANNER -->
            <div class="bannercontainer bannerV2">
                <div class="fullscreenbanner-container">
                    <div class="fullscreenbanner">
                        <ul>
                            <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                                <img src="img/home/slider/course_banner_1.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                <div class="slider-caption container">
                                    <div class="tp-caption rs-caption-1 sft text-center"
                                         data-x="center"
                                         data-y="210"
                                         data-speed="800"
                                         data-start="500"
                                         data-easing="Back.easeInOut"
                                         data-endspeed="300">
                                        Sale Your Courses Online
                                    </div>

                                    <div class="tp-caption rs-caption-2 sft text-center"
                                         data-x="center"
                                         data-y="320"
                                         data-speed="1000"
                                         data-start="1500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn"
                                         data-captionhidden="off">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consectetur ante volutpat sem aliquam lobortis. Mauris porta fermentum volutpat.
                                    </div>
                                    <div class="tp-caption rs-caption-3 sft text-center"
                                         data-x="center"
                                         data-y="400"
                                         data-speed="800"
                                         data-start="2000"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn"
                                         data-captionhidden="off">
                                        <a href="#" class="btn primary-btn">Start Now</a>
                                    </div>
                                </div>
                            </li>
                            <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
                                <img src="img/home/slider/course_banner_2.jpg" alt="slidebg2" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                <div class="slider-caption container">
                                    <div class="tp-caption rs-caption-1 sft"
                                         data-hoffset="0"
                                         data-y="210"
                                         data-speed="800"
                                         data-start="500"
                                         data-easing="Back.easeInOut"
                                         data-endspeed="300">
                                        Sale Your Courses Online
                                    </div>

                                    <div class="tp-caption rs-caption-2 sft"
                                         data-hoffset="0"
                                         data-y="320"
                                         data-speed="1000"
                                         data-start="1500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn"
                                         data-captionhidden="off">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consectetur ante volutpat sem aliquam lobortis. Mauris porta fermentum volutpat.
                                    </div>
                                    <div class="tp-caption rs-caption-3 sft"
                                         data-hoffset="0"
                                         data-y="400"
                                         data-speed="800"
                                         data-start="2000"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn"
                                         data-captionhidden="off">
                                        <a href="#" class="btn primary-btn">Start Now</a>
                                    </div>
                                </div>
                            </li>
                            <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700"  data-title="Slide 3">
                                <img src="img/home/slider/course_banner_3.jpg" alt="slidebg3" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                <div class="slider-caption container">
                                    <div class="tp-caption rs-caption-1 sft text-right"
                                         data-hoffset="-50"
                                         data-x="right"
                                         data-y="210"
                                         data-speed="800"
                                         data-start="500"
                                         data-easing="Back.easeInOut"
                                         data-endspeed="300">
                                        Sale Your Courses Online
                                    </div>

                                    <div class="tp-caption rs-caption-2 sft text-right"
                                         data-hoffset="-50"
                                         data-x="right"
                                         data-y="320"
                                         data-speed="1000"
                                         data-start="1500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn"
                                         data-captionhidden="off">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consectetur ante volutpat sem aliquam lobortis. Mauris porta fermentum volutpat.
                                    </div>
                                    <div class="tp-caption rs-caption-3 sft text-right"
                                         data-hoffset="-50"
                                         data-x="right"
                                         data-y="400"
                                         data-speed="800"
                                         data-start="2000"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn"
                                         data-captionhidden="off">
                                        <a href="#" class="btn primary-btn">Start Now</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>