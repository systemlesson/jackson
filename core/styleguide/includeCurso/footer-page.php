<footer class="footer-v2">
        <div class="menuFooter clearfix">
            <div class="container">
                <div class="row clearfix">

                    <div class="col-sm-3 col-xs-6">
                        <div class="footer-about">
                            <a class="footer-logo" href="index.html">
                                <img src="img/logo.png" alt="logo" class="img-responsive" />
                            </a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>
                            <a href="">Read More ></a>
                        </div>
                    </div><!-- col-sm-3 col-xs-6 -->

                    <div class="col-sm-3 col-xs-6">
                        <h5>Study</h5>
                        <ul class="menuLink">
                            <li><a href="about.html">About Royal College</a></li>
                            <li><a href="campus.html">About Campus</a></li>
                            <li><a href="stuff.html">Staff Members</a></li>
                            <li><a href="about.html">Why Choose Us?</a></li>
                        </ul>
                    </div><!-- col-sm-3 col-xs-6 -->

                    <div class="col-sm-3 col-xs-6">
                        <h5>Explore</h5>
                        <ul class="menuLink">
                            <li><a href="course-fullwidth.html">All Courses</a></li>
                            <li><a href="buying-steps.html">Admission</a></li>
                            <li><a href="photo-gallery3col.html">Photo Gallery</a></li>
                            <li><a href="international_students.html">International Students</a></li>
                        </ul>
                    </div><!-- col-sm-3 col-xs-6 -->

                    <div class="col-sm-3 col-xs-6">
                        <h5>Contact Us</h5>
                        <p>Lorem ipsum dolor sit amet, consecte adipisicing elit sed do eiusmod. </p>
                        <div class="footer-contact">
                            <ul>
                                <li> <i class="fa fa-home" aria-hidden="true"></i>1201 park street, Fifth Avenue, Dhanmondy, Dhaka.</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i>[88] 657 524 332</li>
                                <li><a href="mailto:info@Royal University.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>info@Royal University.com</a></li>
                            </ul>
                        </div>
                    </div><!-- col-sm-3 col-xs-6 -->

                </div><!-- row clearfix -->
            </div><!-- container -->
        </div><!-- menuFooter -->

        <div class="footer clearfix">
            <div class="container">
                <div class="footer-bottom">
                    <div class="row clearfix">
                        <div class="col-sm-6 col-xs-12 copyRight">
                            <p>© 2016 Copyright Royal College Bootstrap Template by <a href="http://www.iamabdus.com">Abdus</a></p>
                        </div><!-- col-sm-6 col-xs-12 -->
                        <div class="col-sm-6 col-xs-12 privacy_policy">
                            <a href="contact-us.html">Site Map </a>
                            <a href="privacy-policy.html">Privacy Policy</a>
                            <a href="privacy-policy.html">Trademarks and Copyright</a>
                        </div><!-- col-sm-6 col-xs-12 -->
                    </div><!-- row clearfix -->
                </div>
            </div><!-- container -->
        </div><!-- footer -->
    </footer>