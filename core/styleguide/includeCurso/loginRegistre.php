<!-- REGISTER MODAL -->
        <div class="modal fade customModal" id="createAccount" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="panel panel-default formPanel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign up to checkout</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="panel-body">
                            <form action="#" method="POST" role="form">
                                <div class="form-group formField">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <input type="text" class="form-control" placeholder="Full Name">
                                </div>
                                <div class="form-group formField">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group formField">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-block commonBtn">Log in</button>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Check here for exiting deals and personalized Course recommendations
                                        </label>
                                    </div>
                                </div>
                            </form>
                            <div class="alt-text">or</div>
                            <div class="modal-border"></div>
                            <div class="alt-btn">
                                <a class="facebook-btn btn btn-block" href="#">Log in with Facebook</a>
                                <a class="twitter-btn btn btn-block" href="#">Log in with Facebook</a>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <p>Allready have an account? <a href="#">Log in</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- LOGIN MODAL -->
        <div class="modal fade customModal" id="loginModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="panel panel-default formPanel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Log In to your Account</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="panel-body">
                            <form action="#" method="POST" role="form">
                                <div class="form-group formField">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group formField">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-block commonBtn">Log in</button>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Remember me
                                        </label>
                                    </div>
                                    <a class="forgot-link" href="#">Fogot Password?</a>
                                </div>
                            </form>
                            <div class="alt-text">or</div>
                            <div class="modal-border"></div>
                            <div class="alt-btn">
                                <a class="facebook-btn btn btn-block" href="#">Log in with Facebook</a>
                                <a class="twitter-btn btn btn-block" href="#">Log in with Facebook</a>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <p>Don’t have an Account? <a href="#">Sign up</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        