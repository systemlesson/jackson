 <!--POPULAR COURSE -->
            <div class="popularCourse padding clearfix">
                <div class="sectionTitle text-center">
                    <h3>Popular Courses</h3>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="imageBox">
                            <div class="productImage clearfix">
                                <a href="single-course-right-sidebar.html"><img src="img/home/course/course-img1.jpg" alt="Image"></a>
                                <span class="sticker"><i class="fa fa-clock-o" aria-hidden="true"></i>6 Hours</span>
                            </div>
                            <div class="productCaption clearfix">
                                <h3><a href="single-course-right-sidebar.html">Lorem ipsum dolor sit amet consectetur adipiscing.</a></h3>
                                <div class="product-meta">
                                    <span class="author">by: John Doe</span>
                                </div>
                                <div class="rating">
                                    <span class="rating-star">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </span>
                                    <span class="rating-review">(20 Reviews)</span>
                                </div>
                                <div class="caption-bottom">
                                    <div class="price">
                                        <span class="offer-price">$79</span>
                                        <span class="regular-price"><del>$100</del></span>
                                    </div>
                                    <div class="user">
                                        <i class="fa fa-user-o" aria-hidden="true"></i>
                                        <span class="course-user">5.9 k <br> Enroled</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="imageBox">
                            <div class="productImage clearfix">
                                <a href="single-course-right-sidebar.html"><img src="img/home/course/course-img2.jpg" alt="Image"></a>
                                <span class="sticker"><i class="fa fa-clock-o" aria-hidden="true"></i>6 Hours</span>
                            </div>
                            <div class="productCaption clearfix">
                                <h3><a href="single-course-right-sidebar.html">Lorem ipsum dolor sit amet consectetur adipiscing.</a></h3>
                                <div class="product-meta">
                                    <span class="author">by: John Doe</span>
                                </div>
                                <div class="rating">
                                    <span class="rating-star">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </span>
                                    <span class="rating-review">(20 Reviews)</span>
                                </div>
                                <div class="caption-bottom">
                                    <div class="price">
                                        <span class="offer-price">$79</span>
                                        <span class="regular-price"><del>$100</del></span>
                                    </div>
                                    <div class="user">
                                        <i class="fa fa-user-o" aria-hidden="true"></i>
                                        <span class="course-user">5.9 k <br> Enroled</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="imageBox">
                            <div class="productImage clearfix">
                                <a href="single-course-right-sidebar.html"><img src="img/home/course/course-img3.jpg" alt="Image"></a>
                                <span class="sticker"><i class="fa fa-clock-o" aria-hidden="true"></i>6 Hours</span>
                            </div>
                            <div class="productCaption clearfix">
                                <h3><a href="single-course-right-sidebar.html">Lorem ipsum dolor sit amet consectetur adipiscing.</a></h3>
                                <div class="product-meta">
                                    <span class="author">by: John Doe</span>
                                </div>
                                <div class="rating">
                                    <span class="rating-star">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </span>
                                    <span class="rating-review">(20 Reviews)</span>
                                </div>
                                <div class="caption-bottom">
                                    <div class="price">
                                        <span class="offer-price">$79</span>
                                        <span class="regular-price"><del>$100</del></span>
                                    </div>
                                    <div class="user">
                                        <i class="fa fa-user-o" aria-hidden="true"></i>
                                        <span class="course-user">5.9 k <br> Enroled</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="imageBox">
                            <div class="productImage clearfix">
                                <a href="single-course-right-sidebar.html"><img src="img/home/course/course-img4.jpg" alt="Image"></a>
                                <span class="sticker"><i class="fa fa-clock-o" aria-hidden="true"></i>6 Hours</span>
                            </div>
                            <div class="productCaption clearfix">
                                <h3><a href="single-course-right-sidebar.html">Lorem ipsum dolor sit amet consectetur adipiscing.</a></h3>
                                <div class="product-meta">
                                    <span class="author">by: John Doe</span>
                                </div>
                                <div class="rating">
                                    <span class="rating-star">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </span>
                                    <span class="rating-review">(20 Reviews)</span>
                                </div>
                                <div class="caption-bottom">
                                    <div class="price">
                                        <span class="offer-price">$79</span>
                                        <span class="regular-price"><del>$100</del></span>
                                    </div>
                                    <div class="user">
                                        <i class="fa fa-user-o" aria-hidden="true"></i>
                                        <span class="course-user">5.9 k <br> Enroled</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btnArea text-center">
                    <a href="" class="btn btn-default commonBtn">View more</a>
                </div>
            </div>

        </div>
    </div>