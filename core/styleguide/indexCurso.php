

<div class="main_wrapper">

    <?php include "includeCurso/header.php"; ?>
    <?php include "includeCurso/header-page.php"; ?>
    <?php include "includeCurso/banners.php"; ?>
    <?php include "includeCurso/filtro.php"; ?>
    <?php include "includeCurso/popularCourses.php"; ?>
    <?php include "includeCurso/categoriaCurso.php"; ?>






    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container">
            <div class="info-section">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="box text-center">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            <div class="separator"></div>
                            <span>Over 5,000 Students Enrolled</span>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="box text-center">
                            <i class="fa fa-shield" aria-hidden="true"></i>
                            <div class="separator"></div>
                            <span>Get Certified Once Completed</span>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="box text-center">
                            <i class="fa fa-book" aria-hidden="true"></i>
                            <div class="separator"></div>
                            <span>500 + Online Courses and Increasing</span>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="box text-center">
                            <i class="fa fa-group" aria-hidden="true"></i>
                            <div class="separator"></div>
                            <span>More than 100 Super Qualified Instructor</span>
                        </div>
                    </div>
                </div>
            </div>

            <?php include "includeCurso/successStories.php"; ?>




            <!-- LIGHT SECTION -->
            <section class="clearfix padding feature-section">
                <div class="container">

                    <div class="sectionTitle text-center">
                        <h3>Why Royal Course?</h3>
                    </div>

                    <div class="row">
                        <div class="col-sm-3 col-xs-6 text-center">
                            <div class="feature-box">
                                <span><i class="fa fa-firefox" aria-hidden="true"></i></span>
                                <h3>Learn Anything Online</h3>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-6 text-center">
                            <div class="feature-box">
                                <span><i class="fa fa-desktop" aria-hidden="true"></i></span>
                                <h3>Learn On Any Device</h3>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-6 text-center">
                            <div class="feature-box">
                                <span><i class="fa fa-id-badge" aria-hidden="true"></i></span>
                                <h3>Cartification Courses</h3>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-6 text-center">
                            <div class="feature-box">
                                <span><i class="fa fa-headphones" aria-hidden="true"></i></span>
                                <h3>24/7 Support</h3>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                            </div>
                        </div>
                    </div>

                    <div class="btnArea text-center">
                        <a href="" class="btn btn-default commonBtn">All Categories</a>
                        <a href="" class="btn btn-default commonBtn secondaryBtn">View more</a>
                    </div>

                </div>
            </section>


            <?php include "includeCurso/footer-page.php"; ?>
            <?php include "includeCurso/loginRegistre.php"; ?>

        </div>
    </div>


    <?php include "includeCurso/footer.php"; ?>

</body>
</html>

