<?php

namespace config;

use core\config\ControllerCoreConfig;
use module\sistema\util\ControlePermissao;
use core\helper\SessionHelper;

#***************************************************************
#-> CONTROLADORES
#***************************************************************
# controladores do módulo sistema #
use module\sistema\controller\login\LoginController;
use module\sistema\controller\cadastro\CadastroController;
use module\sistema\controller\error\ErrorController;
use module\sistema\controller\perfil\PerfilController;
use module\sistema\controller\logout\LogoutController;
use module\sistema\controller\RecuperarSenha\RecuperarSenhaController;
use core\helper\PermissaoAcessoHelper;

# controladores do módulo almoxarifado #
use module\almoxarifado\controller\InicioController;
use module\almoxarifado\controller\UnidadeController;
use module\almoxarifado\controller\ProdutoController;
use module\almoxarifado\controller\SetorController;
use module\almoxarifado\controller\CategoriaController;
use module\almoxarifado\controller\FuncionarioController;
use module\almoxarifado\controller\RelatorioEntradaController;
use module\almoxarifado\controller\EntradaProdutoController;
use module\almoxarifado\controller\SolicitacaoController;
use module\almoxarifado\controller\DevolucaoController;
use module\almoxarifado\controller\SaidaProdutoController;

class ControllerConfig extends ControllerCoreConfig {

    /**
     * getControllerInstance
     * Método que devolve a instancia de um controlador
     * @param type $nomeControlador
     * @return mixed
     */
    static function getControllerInstance($nomeControlador) {

        switch ($nomeControlador) {
            case '': return new LoginController();
                break;
            case 'login':
                return new LoginController();
                break;
            case 'cadastro':
                return new CadastroController();
                break;
            case 'perfil':
                return new PerfilController();
                break;
            case 'logout':
                return new LogoutController();
                break;
            case 'error':
                return new ErrorController();
                break;
            case 'RecuperarSenha':
                return new RecuperarSenhaController();
                break;

            /* MODULO SIAPP */

            case 'inicio':
                return new InicioController();
                break;
            case 'combo':
                return new ComboController();
                break;
            case 'unidade':
                return new UnidadeController();
                break;
            case 'produto':
                return new ProdutoController();
                break;
            case 'setor':
                return new SetorController();
                break;
            case 'categoria':
                return new CategoriaController();
                break;
            case 'funcionario':
                return new FuncionarioController();
                break;
            case 'entradaproduto':
                return new EntradaProdutoController();
                break;
            case 'solicitacao':
                return new SolicitacaoController();
                break;
            case 'devolucao':
                return new DevolucaoController();
                break;
            case 'saidaproduto':
                return new SaidaProdutoController();
                break;
              
            
        }
    }

    /**
     * validateAction
     * Método que verifica de uma determinada action existe. 
     * Caso não exista aciona o controlador 404
     * @param String $controller
     * @param String $action
     */
    static public function validateAction($controller, $action) {
        return true;
        if (!method_exists($controller, $action)) {
            $errorController = new ErrorController();
            $errorController->action404($action);
            die;
        }

        $controlePermissao = new PermissaoAcessoHelper();
        $controller = self::tratarNomeControle($controller);
        $arrayPermissoes = SessionHelper::getSessionValue('segPermissoes');

        // Controle Permissão Simplificado.
        // FAZER::adicionar IN e criar um item no congif com array contendo controles publicos
        if ($controller != 'login' && $controller != 'logout' && $controller != 'recuperarsenha' && $controller != 'cadastro') {
            if (!$controlePermissao->validarPermissao($arrayPermissoes, $controller, $action)) {
                $errorController = new ErrorController();
                $errorController->acessoNegado($controller);
                die;
            }
        }
    }

}
