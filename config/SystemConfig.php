<?php

namespace config;

use core\config\SystemCoreConfig;

/**
 * Classe de Configuração do sistema
 */
class SystemConfig extends SystemCoreConfig {

    # algumas dessas mensagens são do sised #
    const SYSTEM_MSG = array(
        'MSG0' => 'Data inválida!',
        'MSG1' => 'Preencha este campo!',
        'MSG2' => 'Adicionado com sucesso!',
        'MSG3' => 'Alterado com sucesso!',
        'MSG4' => 'Excluído com sucesso!',
        'MSG04' => 'Já existe outra unidade com este nome.',
        'MSG05' => 'Esse já é o nome atual da Unidade.',
        'MSG06' => 'Já existe outra categoria com este nome.',
        'MSG07' => 'Já existe outro setor com este nome.',    
        'MSG08' => 'Selecione uma opção!',    
        
        'MSG5' => 'Plano de Curso já Cadastro para a Turma',
        'MSG6' => 'Já existe território de identidade com a descrição informada.',
        'MSG7' => 'Exclusão Não Permitida! Curso vinculado a universidade(s).',
        'MSG8' => 'Já existe curso com a descrição informada.',
        'MSG9' => 'Já existe universidade com a descrição informada.',
        'MSG10' => 'Exclusão Não Permitida! Universidade vinculada a curso(s).',
        'MSG11' => 'Já existe edital com a descrição informada.',
        'MSG12' => 'Universidade já incluída na listagem.',
        'MSG13' => 'Exclusão Não Permitida! Edital vinculado a turma(s).',
        'MSG14' => 'Previsão de Conclusão inferior a Data de Ingresso.',
        'MSG15' => 'Necessário incluir ao menos uma IES no edital.',
        'MSG16' => 'Já existe polo com a descrição informada.',
        'MSG17' => 'Exclusão Não Permitida! Polo vinculado a turma(s).',
        'MSG18' => 'Este CPF é inválido.',
        'MSG19' => 'E-mail Inválido!',
        'MSG20' => 'Já existe outra entidade cadastrada com o CPF informado.',
        'MSG21' => 'Valor indevido. Informar valor superior a 0',
        'MSG22' => 'E-mail Inválido!',
        'MSG23' => 'Já existe outra entidade cadastrada com o CPF informado.',
        'MSG24' => 'Conclua a inserção de todos os dados da unidade anterior antes de dar prosseguimento ao Plano de Curso.',
        'MSG25' => 'A unidade escolar em que está associado encontra-se inativa.',
        'MSG26' => 'O campo cronograma da [nome da unidade] é obrigatório!',
        'MSG27' => 'Complete todas as informações necessárias antes de concluir o envio do seu Plano de Curso.',
        'MSG28' => 'Login realizado com sucesso!',
        'MSG29' => 'Selecione apenas 1 perfil',
        'MSG30' => 'E-MAIL ou SENHA inválida!',
        'MSG31' => 'É obrigatório a inclusão de um arquivo de tipo PDF.',
        'MSG32' => 'Alteração não permitida! É necessário haver ao menos um registro ativo!',
        'MSG33' => 'Tamanho limite atingido! 5MB é o limite para cada tipo de arquivo.',
        'MSG34' => 'Servidor com status não permitido de acesso ao sistema!.',
        'MSG35' => 'Usuário inativo no sistema.',
        'MSG36' => 'Não é possível realizar esse cadastro. Servidor não pertence a sua lotação.',
        'MSG37' => 'Erro ao carregar o arquivo.',
        'MSG38' => 'Não é permitido a inserção de mais de um arquivo PDF..',
        'MSG39' => 'Não é permitido inserir dois projetos político pedagógico para a mesma unidade escolar em um mesmo exercício.',
        'MSG40' => 'Usuário com status SEC Online não permitido no sistema',
        'MSG41' => 'Exclusão Não Permitida! Eixo Integrador vinculado a plano de curso.',
        'MSG42' => 'Exclusão Não Permitida! Competência vinculada a um eixo integrador',
        'MSG43' => 'Já existe competência com a descrição informada.',
        'MSG44' => 'Exclusão Não Permitida! Habilidade vinculada a uma competência.',
        'MSG45' => 'Já existe habilidade com a descrição informada..',
        'MSG46' => 'Já existe eixo integrador com a descrição informada.',
        'MSG47' => 'Já existe relacionamento de eixo integrador com a descrição informada..',
        'MSG48' => 'Exclusão Não Permitida! Relacionamento vinculado a um plano de curso.',
        'MSG49' => 'Já existe tema gerador com a descrição informada',
        'MSG50' => 'Exclusão Não Permitida! Tema Gerador vinculado a plano de curso..',
        'MSG51' => 'Já existe aprendizagem desejada com a descrição informada.',
        'MSG52' => 'Exclusão Não Permitida! Aprendizagem desejada vinculada a plano de curso.',
        'MSG53' => 'Já existe aspecto cognitivo com a descrição informada.',
        'MSG54' => 'Exclusão Não Permitida! Aspecto cognitivo vinculado a plano de curso',
        'MSG55' => 'Já existe aspecto sócio-formativo com a descrição informada..',
        'MSG56' => 'Exclusão Não Permitida! Aspecto sócio-formativo vinculado a plano de curso.',
        'MSG57' => 'Já existe saberes necessários com a descrição informada',
        'MSG58' => 'Exclusão Não Permitida! Saberes necessários vinculado a plano de curso..',
        'MSG59' => 'Ação Não Permitida! Já existe um PPP para o ano vigente.',
        'Adicionar1' => 'Os dados da sua etapa foram salvos com sucesso! Você será redirecionado para a próxima etapa.',
        'Adicionar2' => 'Os dados da sua etapa foram salvos com sucesso! Você será redirecionado para a etapa anterior.',
        'Adicionar3' => 'Os dados da sua etapa foram salvos com sucesso!',
        'Adicionar4' => 'Plano de curso salvo com sucesso!',
    );
    const SYSTEM_ROOT = "";
    const SYSTEM_NAME = "ESTUDOFRAMEWORK ";
    const SYSTEM_NAME_FORMATED = '<i class="col-red">SISTEMA </i><b>ALMOXARIFADO</b>';
    const SYSTEM_DESCRICAO = 'Sistema exemplo para aprendizado';

    # AUTENTICAÇÃO
    const AUTH_RASEA_APPLICATION_NAME = "ALMOXARIFADO"; # NOME DA APLICAÇÃO (IGUAL AO RASEA)
    # AMBIENTE DE TRABALHO
    const ENVIRONMENT = "DESENVOLVIMENTO"; # [DESENVOLVIMENTO - HOMOLOGACAO - PRODUCAO]
    # CUSTOM
    const RECAPTCHA_SECRET_KEY = "6LeWeWYUAAAAAAr50jlPjilDzE4g9tRA4AJPNovD";
    const RECAPTCHA_PUBLIC_KEY = "6LeWeWYUAAAAAKILAlezwCmuilBXI3cjS9XPEaXX";
    const EMAIL_REMETENTE = "nao-responda@educacao.ba.gov.br";
    const NOME_REMETENTE = "Controle de Segurança";
    const CHAVE_APLICACAO = "#DEV@SEC-SIAPP";

}
