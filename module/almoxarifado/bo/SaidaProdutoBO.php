<?php

namespace module\almoxarifado\bo;

use config\SystemConfig;
use module\almoxarifado\vo\SaidaProdutoVO;
use module\almoxarifado\dao\SaidaProdutoDAO;
use core\helper\BoHelper;
use core\exception\AppException;

/**
 * Classe de negócio referente a (SaidaProduto)
 */
class SaidaProdutoBO {

    /**
     * Método que realiza a listagem
     * @param SaidaProdutoVO $objSaidaProdutoVO
     * @return ArrayObject
     */
    public function listar(SaidaProdutoVO $objSaidaProdutoVO) {

        $objBoHelper = new BoHelper();
        
         if (empty($objSaidaProdutoVO->getSaida())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        /**
         *  realizando procedimentos
         */
        try {

            $objSaidaProdutoDAO = new SaidaProdutoDAO();
            $objBoHelper->setRetorno($objSaidaProdutoDAO->listar($objSaidaProdutoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a listagem
     * @param SaidaProdutoVO $objSaidaProdutoVO
     * @return ArrayObject
     */
    public function selecionar($objSaidaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objSaidaProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {

                $objSaidaProdutoDAO = new SaidaProdutoDAO();
                $objBoHelper->setRetorno($objSaidaProdutoDAO->selecionar($objSaidaProdutoVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }


        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza inclusão
     * @param SaidaProdutoVO $objSaidaProdutoVO
     * @return String
     */
    public function inserir(SaidaProdutoVO $objSaidaProdutoVO) {


        $objBoHelper = new BoHelper();
        /**
         *  Verificando Regra de negócio
         */
       

        if (strlen($objSaidaProdutoVO->getIdSolicitacaoProduto()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("Id de Solicitação não pode ser vazio! ");
            $objBoHelper->setChkErro(TRUE);
        }


        if (!$objBoHelper->getChkErro()) {

            try {

                $objSaidaProdutoDAO = new SaidaProdutoDAO();
                $objBoHelper->setRetorno($objSaidaProdutoDAO->inserir($objSaidaProdutoVO));

                $objBoHelper->addRetornoMensagem("Solicitação atendida com sucesso!");
            } catch (\Exception $ex) {

                echo $ex->getMessage();
                die;

                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoMensagemImplode();
    }

    public function alterar($objSaidaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objSaidaProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {

                $objSaidaProdutoDAO = new SaidaProdutoDAO();
                $objBoHelper->setRetorno($objSaidaProdutoDAO->alterar($objSaidaProdutoVO));
            } catch (\Exception $ex) {
                // var_dump($objBoHelper->getRetornoMensagemImplode(); die;
                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }


        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

}
