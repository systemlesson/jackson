<?php

namespace module\almoxarifado\bo;

use config\SystemConfig;
use core\helper\BoHelper;
use module\almoxarifado\vo\UnidadeVO;
use module\almoxarifado\dao\UnidadeDAO;

# Classe de negócio referente a >Unidade< #

class UnidadeBO {

    /**
     * Método que realiza a listagem
     * @param UnidadeVO $objUnidadeVO
     * @return ArrayObject
     */
    public function listar(UnidadeVO $objUnidadeVO) {
        # Instanciando classe de apoio da camada #
        $objBoHelper = new BoHelper();
        
        # Realizando Procedimentos
        try {
            $objUnidadeDAO = new UnidadeDAO();
            $objBoHelper->setRetorno($objUnidadeDAO->listar($objUnidadeVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi possível realizar a operação.");
        }

        # Retornando resultado da operação
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a listagem
     * @param UnidadeVO $objUnidadeVO
     * @return ArrayObject
     */
    public function selecionar($objUnidadeVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio
        if (empty($objUnidadeVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo ID não pode ficar vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objUnidadeDAO = new UnidadeDAO();
                $objBoHelper->setRetorno($objUnidadeDAO->selecionar($objUnidadeVO));
            } catch (Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a inclusão da unidade
     * @param UnidadeVO $objUnidadeVO
     * @return String
     */
    public function inserir($objUnidadeVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio
        if (strlen($objUnidadeVO->getNome()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha esse campo.()");
            $objBoHelper->setChkErro(TRUE);
        }

//        if ($this->existe($objUnidadeVO)) {
//            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG04']);
//            $objBoHelper->setChkErro(TRUE);
//        }

        if ($this->verificaUnidade($objUnidadeVO, TRUE)) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG04']);
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objUnidadeDAO = new UnidadeDAO();
                $objBoHelper->setRetorno($objUnidadeDAO->inserir($objUnidadeVO));
                $objBoHelper->addRetornoMensagem("Adicionado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                exit;
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        #Retornando resultado da operação
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza a alteração da unidade
     * @param UnidadeVO $objUnidadeVO
     * @return String
     */
    public function alterar(UnidadeVO $objUnidadeVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (strlen($objUnidadeVO->getNome() == "")) {
            $objBoHelper->setRetornoOperacao("Preencha esse campo.");
            $objBoHelper->setChkErro(TRUE);
        }

//        if ($this->existe($objUnidadeVO)) {
//            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG04']);
//            $objBoHelper->setChkErro(TRUE);
//        }

        if ($this->verificaUnidade($objUnidadeVO, TRUE)) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG04']);
            $objBoHelper->setChkErro(TRUE);
        }
//        var_dump($objBoHelper); exit;
        if (!$objBoHelper->getChkErro()) {
            try {
                $objUnidadeDAO = new UnidadeDAO();
                $objBoHelper->setRetornoOperacao($objUnidadeDAO->alterar($objUnidadeVO));
                $objBoHelper->addRetornoMensagem("Alterado com sucesso!");
            } catch (\Exception $ex) {
//                var_dump($objBoHelper->getRetornoOperacao()); die;
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza a alteração da unidade
     * @param UnidadeVO $objUnidadeVO
     * @return String
     */
    public function excluir(UnidadeVO $objUnidadeVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (empty($objUnidadeVO->getId())) { 
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG1']);
            $objBoHelper->setChkErro(1);
        }
        
        if (!$objBoHelper->getChkErro()) {
            try {
                $objUnidadeDAO = new UnidadeDAO();
                $objBoHelper->setRetornoOperacao($objUnidadeDAO->excluir($objUnidadeVO));
                $objBoHelper->addRetornoMensagem("Excluído com sucesso.");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Verifica a existência de um registro
     * @param UnidadeVO $objUnidadeVO, $exceto = FALSE
     * @return UnidadeVO
     */
    public function verificaUnidade(UnidadeVO $objUnidadeVO, $exceto = FALSE) {
        $objUnidadeDAO = new UnidadeDAO();
        $objBoHelper = new BoHelper();

        if (!$objBoHelper->getChkErro()) {
            try {
                $objBoHelper->setRetornoOperacao($objUnidadeDAO->verificaUnidade($objUnidadeVO, $exceto));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoOperacao();
    }

    /**
     * Puxa a tabela de Unidade para a combo box da tela de Produto
     * @param UnidadeVO $objUnidadeVO
     * @return String
     */
    public function listarUnidade(UnidadeVO $objUnidadeVO) {
        $objBoHelper = new BoHelper();

        try {
            $objUnidadeDAO = new UnidadeDAO();
            $objBoHelper->setRetorno($objUnidadeDAO->listarUnidade($objUnidadeVO));
            return $objBoHelper->getRetornoOperacao();
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }
    }

    /**
     * Verifica a existência de um registro (não utilizado)
     * @param UnidadeVO $objUnidadeVO
     * @return UnidadeVO
     */
    public function existe($objUnidadeVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #     
        if (empty($objUnidadeVO->getNome())) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG1'] . " (Nome)");
            $objBoHelper->setChkErro(1);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objUnidadeDAO = new UnidadeDAO();
                $objBoHelper->setRetorno($objUnidadeDAO->existe($objUnidadeVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoOperacao();
    }

}
