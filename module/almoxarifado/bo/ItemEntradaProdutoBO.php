<?php

namespace module\almoxarifado\bo;

use module\almoxarifado\vo\ItemEntradaProdutoVO;
use module\almoxarifado\dao\ItemEntradaProdutoDAO;
use core\helper\BoHelper;

/**
 * Classe de negócio referente a (ItemEntradaProduto)
 */
class ItemEntradaProdutoBO {

    /**
     * Método que realiza inclusão
     * @param ItemEntradaProdutoVO $objItemEntradaProdutoVO
     * @return String
     */
    public function inserir(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {


        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (strlen($objItemEntradaProdutoVO->getIdProduto()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("Id funcionario não pode ser vazio! ");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objItemEntradaProdutoVO->getQuantidade()) == 0) {
            $objBoHelper->addRetornoMensagem("Quantidade não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objItemEntradaProdutoVO->getExcluido()) == 0) {
            $objBoHelper->addRetornoMensagem("Excluido não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objItemEntradaProdutoVO->getIdEntradaProdutos()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("IdEntradaProduto não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }
        $objBoHelper->validacaoPadraoCadastro($objItemEntradaProdutoVO);


        if (!$objBoHelper->getChkErro()) {
            try {

                $objItemEntradaProdutoDAO = new ItemEntradaProdutoDAO();

                $objBoHelper->setRetorno($objItemEntradaProdutoDAO->inserir($objItemEntradaProdutoVO));

                $objBoHelper->addRetornoMensagem("Adicionado com sucesso!");
            } catch (\Exception $ex) {

                echo $ex->getMessage();
                die;
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {

            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoMensagemImplode();
    }

    public function listarItemEntradaProduto(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objItemEntradaProdutoVO->getIdEntradaProdutos()->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id de entrada produto não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {

                $objItemEntradaProdutoDAO = new ItemEntradaProdutoDAO();
                $objBoHelper->setRetorno($objItemEntradaProdutoDAO->listarItemEntradaProduto($objItemEntradaProdutoVO));
            } catch (\Exception $ex) {
                // var_dump($objBoHelper->getRetornoMensagemImplode(); die;

                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    public function listar(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objItemEntradaProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Id do item entrada não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (!$objBoHelper->getChkErro()) {
            try {

                $objItemEntradaProdutoDAO = new ItemEntradaProdutoDAO();
                $objBoHelper->setRetorno($objItemEntradaProdutoDAO->listar($objItemEntradaProdutoVO));
            } catch (\Exception $ex) {
                // var_dump($objBoHelper->getRetornoMensagemImplode(); die;

                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    public function alterar(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {
        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (strlen($objItemEntradaProdutoVO->getIdProduto()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("Id funcionario não pode ser vazio! ");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objItemEntradaProdutoVO->getQuantidade()) == 0) {
            $objBoHelper->addRetornoMensagem("Quantidade não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objItemEntradaProdutoVO->getExcluido()) == 0) {
            $objBoHelper->addRetornoMensagem("Excluido não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objItemEntradaProdutoVO->getIdEntradaProdutos()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("IdEntradaProduto não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }

        $objBoHelper->validacaoPadraoCadastro($objItemEntradaProdutoVO);


        if (!$objBoHelper->getChkErro()) {

            try {

                $objItemEntradaProdutoDAO = new ItemEntradaProdutoDAO();

                $objBoHelper->setRetorno($objItemEntradaProdutoDAO->alterar($objItemEntradaProdutoVO));
                $objBoHelper->addRetornoMensagem("Alterado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }
        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoMensagemImplode();
    }

    public function listarByProduto(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objItemEntradaProdutoVO->getIdProduto()->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id produto não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {

                $objItemEntradaProdutoDAO = new ItemEntradaProdutoDAO();
                $objBoHelper->setRetorno($objItemEntradaProdutoDAO->listarItemEntradaProduto($objItemEntradaProdutoVO));
            } catch (\Exception $ex) {
                // var_dump($objBoHelper->getRetornoMensagemImplode(); die;

                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

}
