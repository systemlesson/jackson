<?php

namespace module\almoxarifado\bo;

use core\exception\AppException;
use config\SystemConfig;
use module\almoxarifado\vo\ProdutoVO;
use module\almoxarifado\dao\ProdutoDAO;
use core\helper\BoHelper;

/**
 * Classe de negócio referente a (Produto)
 */
class ProdutoBO {

    /**
     * Método que realiza a listagem
     * @param ProdutoVO $objProdutoVO
     * @return ArrayObject
     */
    public function listar(ProdutoVO $objProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  realizando procedimentos
         */
        try {

            $objProdutoDAO = new ProdutoDAO();
            $objBoHelper->setRetorno($objProdutoDAO->listar($objProdutoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a listagem
     * @param ProdutoVO $objProdutoVO
     * @return ArrayObject
     */
    public function selecionar($objProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {

                $objProdutoDAO = new ProdutoDAO();
                $objBoHelper->setRetorno($objProdutoDAO->selecionar($objProdutoVO));
            } catch (\Exception $ex) {
//                 var_dump($objBoHelper->getRetornoMensagemImplode()); die;
                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }


        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza inclusão
     * @param ProdutoVO $objProdutoVO
     * @return String
     */
    public function inserir(ProdutoVO $objProdutoVO) {


        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (strlen($objProdutoVO->getCodigo()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Codigo)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getNome()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Nome)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getQuantidade()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Quantidade)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getDescricao()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Descrição)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getIdCategoria()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("Id categoria não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getIdUnidade()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("Id unidade não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }
        if ($this->existe($objProdutoVO, TRUE)) {
            $objBoHelper->addRetornoMensagem("Ja existe um produto com o mesmo Código");
            $objBoHelper->setChkErro(TRUE);
        }


        $objBoHelper->validacaoPadraoCadastro($objProdutoVO);


        if (!$objBoHelper->getChkErro()) {

            try {

                $objProdutoDAO = new ProdutoDAO();

                $objBoHelper->setRetorno($objProdutoDAO->inserir($objProdutoVO));

                $objBoHelper->addRetornoMensagem("Adicionado com sucesso!");
            } catch (\Exception $ex) {

                echo $ex->getMessage();
                die;
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    public function alterar(ProdutoVO $objProdutoVO) {
        $objBoHelper = new BoHelper();
        
        # Verificando a Regra de Negócio #
        if (strlen($objProdutoVO->getId() == 0)) {
            $objBoHelper->addRetornoMensagem("Id não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getCodigo()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Codigo)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getNome()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Nome)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getQuantidade()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Quantidade)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getDescricao()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Descrição)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getIdCategoria()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Id Categoria)!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objProdutoVO->getIdUnidade()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo (Id Unidade)!");
            $objBoHelper->setChkErro(TRUE);
        }

        if ($this->existe($objProdutoVO, TRUE)) {
            $objBoHelper->addRetornoMensagem("Já existe um produto com o mesmo Código");
            $objBoHelper->setChkErro(TRUE);
        }
        $objBoHelper->validacaoPadraoAlteracao($objProdutoVO);

        //die("aa");
        if (!$objBoHelper->getChkErro()) {
            try {
                $objProdutoDAO = new ProdutoDAO();
                $objBoHelper->setRetorno($objProdutoDAO->alterar($objProdutoVO));
                $objBoHelper->addRetornoMensagem("Alterado com sucesso!");
            } catch (\Exception $ex) {
                throw new AppException("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetorno();
    }

    /**
     *  Retornando Resultado da operação
     */

    /**
     * Método que realiza exclusão
     * @param ProdutoVO $objProdutoVO
     * @return String
     */
    public function excluir(ProdutoVO $objProdutoVO) {

        $objBoHelper = new BoHelper();
        /**
         *  Verificando Regra de negócio
         */
        if (empty($objProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG01']);
            $objBoHelper->setChkErro(TRUE);
        }




        if (!$objBoHelper->getChkErro()) {
            try {

                $objProdutoDAO = new ProdutoDAO();
                $objBoHelper->setRetornoOperacao($objProdutoDAO->excluir($objProdutoVO));
                $objBoHelper->addRetornoMensagem("Excluido com sucesso!");
            } catch (\Exception $ex) {

                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza exclusão
     * @param ProdutoVO $objProdutoVO
     * @return String
     */
    public function listarCombo(ProdutoVO $objProdutoVO) {
        $objBoHelper = new BoHelper();

        try {
            $objProdutoDAO = new ProdutoDAO();
            $objBoHelper->setRetorno($objProdutoDAO->listarCombo($objProdutoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }


        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Verifica a existencia de um registro
     * 
     * @param ProdutoVO $objProdutoVO
     * @return ProdutoVO
     */
    public function existe($objProdutoVO, $exceto = FALSE) {
        $objBoHelper = new BoHelper();

        if (!$objBoHelper->getChkErro()) {
            try {
                $objProdutoDAO = new ProdutoDAO();
                $objBoHelper->setRetorno($objProdutoDAO->existe($objProdutoVO, $exceto));
            } catch (\Exception $ex) {

                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }
        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoOperacao();
    }

    public function relatorioDeProduto(ProdutoVO $objProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  realizando procedimentos
         */
        if (!$objBoHelper->getChkErro()) {
            try {

                $objProdutoDAO = new ProdutoDAO();
                $objBoHelper->setRetorno($objProdutoDAO->relatorioDeProduto($objProdutoVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação");
            }
        } else {
            echo $ex->getMessage();
            throw new \Exception("Não foi possível realizar a operação.");
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    public function adicionarProdutoEstoque(ProdutoVO $objProdutoVO) {
//        die("aa");
        $objBoHelper = new BoHelper();

        if (empty($objProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem('Id produto não pode ser nulo ');
            $objBoHelper->setChkErro(TRUE);
        }
        if (empty($objProdutoVO->getQuantidade())) {
            $objBoHelper->addRetornoMensagem('Quantidade não pode ser nulo');
            $objBoHelper->setChkErro(TRUE);
        }
//        var_dump($objProdutoVO->getQuantidade()); die(); 
        if (!$objBoHelper->getChkErro()) {
            try {

                $objProdutoDAO = new ProdutoDAO();
                $objBoHelper->setRetorno($objProdutoDAO->adicionarProdutoEstoque($objProdutoVO));
                $objBoHelper->addRetornoMensagem("Adicionado com sucesso!");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoOperacao();
    }

    public function retirarProdutoEstoque(ProdutoVO $objProdutoVO) {
        $objBoHelper = new BoHelper();

        if (empty($objProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem('Id produto não pode ser nulo');
            $objBoHelper->setChkErro(TRUE);
        }
        
        if (empty($objProdutoVO->getQuantidade())) {
            $objBoHelper->addRetornoMensagem('Id quantidade não pode ser nulo');
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objProdutoDAO = new ProdutoDAO();
                $objBoHelper->setRetorno($objProdutoDAO->retirarProdutoEstoque($objProdutoVO));
                $objBoHelper->addRetornoMensagem("Retirado com sucesso!");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando Resultado da operação #
        return $objBoHelper->getRetornoOperacao();
    }
    
    public function listarQuantidade(ProdutoVO $objProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Id do produto não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {

                $objProdutoDAO = new ProdutoDAO();
                $objBoHelper->setRetorno($objProdutoDAO->listarQuantidade($objProdutoVO));
            } catch (\Exception $ex) {
                // var_dump($objBoHelper->getRetornoMensagemImplode(); die;

                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoOperacao();
    }

   

}
