<?php

namespace module\almoxarifado\bo;

use config\SystemConfig;
use module\almoxarifado\vo\ItemSolicitacaoProdutoVO;
use module\almoxarifado\dao\ItemSolicitacaoProdutoDAO;
use core\helper\BoHelper;

# Classe de negócio referente a (Item da Solicitação do Produto) #

class ItemSolicitacaoProdutoBO {

    /**
     * Método que realiza inclusão
     * @param ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO
     * @return String
     */
    public function inserir(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objBoHelper = new BoHelper();


        # Verificando Regra de Negócio #

        if (strlen($objItemSolicitacaoProdutoVO->getIdProduto()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("ID de Produto não pode ser vazio! ");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objItemSolicitacaoProdutoVO->getIdSolicitacao()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("ID de Solicitação não pode ser vazio! ");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objItemSolicitacaoProdutoVO->getQuantidade()) == 0) {
            $objBoHelper->addRetornoMensagem("Quantidade não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }

//        var_dump($objItemSolicitacaoProdutoVO->getQuantidade());exit;


        $objBoHelper->validacaoPadraoCadastro($objItemSolicitacaoProdutoVO);

        if (!$objBoHelper->getChkErro()) {
            try {
                $objItemSolicitacaoProdutoDAO = new ItemSolicitacaoProdutoDAO();
                $objBoHelper->setRetorno($objItemSolicitacaoProdutoDAO->inserir($objItemSolicitacaoProdutoVO));
                $objBoHelper->addRetornoMensagem("Adicionado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                exit;
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    public function alterar(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty($objItemSolicitacaoProdutoVO->getIdProduto()->getId())) {
            $objBoHelper->addRetornoMensagem("Produto não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objItemSolicitacaoProdutoVO->getQuantidade()) == 0) {
            $objBoHelper->addRetornoMensagem("Quantidade não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objItemSolicitacaoProdutoVO->getExcluido()) == 0) {
            $objBoHelper->addRetornoMensagem("Excluído não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objItemSolicitacaoProdutoVO->getIdSolicitacao()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("ID da solicitação não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }



        $objBoHelper->validacaoPadraoCadastro($objItemSolicitacaoProdutoVO);

        if (!$objBoHelper->getChkErro()) {
            try {
                $objItemSolicitacaoProdutoDAO = new ItemSolicitacaoProdutoDAO();
                $objBoHelper->setRetorno($objItemSolicitacaoProdutoDAO->alterar($objItemSolicitacaoProdutoVO));
                $objBoHelper->addRetornoMensagem("Alterado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                exit;
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando Resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    public function listar(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objBoHelper = new BoHelper();

//        var_dump($objItemSolicitacaoProdutoVO->getId()); exit;

        if (!$objBoHelper->getChkErro()) {
            try {
                $objItemSolicitacaoProdutoDAO = new ItemSolicitacaoProdutoDAO();
                $objBoHelper->setRetorno($objItemSolicitacaoProdutoDAO->listar($objItemSolicitacaoProdutoVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetorno();
    }

    public function listarPorSolicitacao(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objBoHelper = new BoHelper();

//        var_dump($objItemSolicitacaoProdutoVO); exit;
        # Verificando Regra de Negócio #
        if (empty($objItemSolicitacaoProdutoVO->getIdSolicitacao()->getId())) {
            $objBoHelper->addRetornoMensagem("Id Solicitação não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objItemSolicitacaoProdutoDAO = new ItemSolicitacaoProdutoDAO();
                $objBoHelper->setRetorno($objItemSolicitacaoProdutoDAO->listarPorSolicitacao($objItemSolicitacaoProdutoVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetorno();
    }

    public function listarPorSaida(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty($objItemSolicitacaoProdutoVO->getIdSolicitacao()->getId())) {
            $objBoHelper->addRetornoMensagem("Campo Id Solicitação não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }


        if (!$objBoHelper->getChkErro()) {
            try {
                $objItemSolicitacaoProdutoDAO = new ItemSolicitacaoProdutoDAO();
                $objBoHelper->setRetornoOperacao($objItemSolicitacaoProdutoDAO->listarPorSaida($objItemSolicitacaoProdutoVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoOperacao();
    }

}
