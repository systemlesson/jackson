<?php

namespace module\almoxarifado\bo;

use config\SystemConfig;
use core\helper\BoHelper;
use module\almoxarifado\vo\SolicitacaoVO;
use module\almoxarifado\dao\SolicitacaoDAO;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;

# Classe de negócio referente à Solicitação #

class SolicitacaoBO {

    /**
     * Método que realiza a listagem
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return ArrayObject
     */
    public function listar(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();

        # Realizando procedimentos #
        try {
            $objSolicitacaoDAO = new SolicitacaoDAO();
            $objBoHelper->setRetorno($objSolicitacaoDAO->listar($objSolicitacaoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi possível realizar a operação.");
        }
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que seleciona um item da tabela
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return ArrayObject
     */
    public function selecionar(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty(($objSolicitacaoVO->getId()))) {
            $objBoHelper->addRetornoMensagem("Campo ID não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objSolicitacaoDAO = new SolicitacaoDAO();
                $objBoHelper->setRetorno($objSolicitacaoDAO->selecionar($objSolicitacaoVO));
            } catch (\Exception $ex) {
                throw new \Exception($objBoHelper->getRetornoMensagemImplode());
            }
        }

        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a inclusão de uma solicitação
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return String
     */
    public function inserir(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (strlen($objSolicitacaoVO->getIdFuncionarioUsuario()->getId()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo: Solicitante.");
            $objBoHelper->setChkErro(TRUE);
        }

        if ($objSolicitacaoVO->getItemSolicitacaoProdutoArrayIterator()->count() == 0) {
            $objBoHelper->addRetornoMensagem("A Solicitação deve ter pelo menos um produto.");
            $objBoHelper->setChkErro(TRUE);
        }

        $objBoHelper->validacaoPadraoCadastro($objSolicitacaoVO);

        if (!$objBoHelper->getChkErro()) {
            try {
                $objSolicitacaoDAO = new SolicitacaoDAO();
                $objBoHelper->setRetorno($objSolicitacaoDAO->inserir($objSolicitacaoVO));
                $objBoHelper->addRetornoMensagem("Pedido solicitado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                exit;
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retonando resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza a alteração da solicitação
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return String
     */
    public function alterar(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty($objSolicitacaoVO->getIdFuncionarioUsuario()->getId())) {
            $objBoHelper->addRetornoMensagem("Preencha o campo – Solicitante.");
            $objBoHelper->setChkErro(TRUE);
        }


        ##########################################################

        $objItemSolicitacaoAuxBO = new ItemSolicitacaoProdutoBO();

        foreach ($objSolicitacaoVO->getItemSolicitacaoProdutoArrayIterator() as $objItemSolicitacaoVO) {
            if (!empty($objItemSolicitacaoVO->getId())) {
                $objItemSolicitacaoAuxVO = $objItemSolicitacaoAuxBO->listar($objItemSolicitacaoVO)['retornoOperacao'];
                $quantidadeAnterior = (int) $objItemSolicitacaoAuxVO->getQuantidade();
            } else {
                $quantidadeAnterior = 0;
            }

            $objProdutoBO = new ProdutoBO();
            $objProdutoVO = new ProdutoVO();

            $objProdutoVO->setId($objItemSolicitacaoVO->getIdProduto()->getId());
            $objProdutoVO = $objProdutoBO->selecionar($objProdutoVO)['retornoOperacao'];
            $nomeProduto = $objProdutoVO->getNome();


            $novaQuantidade = (int) $objItemSolicitacaoVO->getQuantidade();
            $qtdSolicitada = (int) $quantidadeAnterior - $novaQuantidade;

            $objProdutoVO->setId($objItemSolicitacaoVO->getIdProduto()->getId());
            $qtdDisponivel = (int) $objProdutoBO->listarQuantidade($objProdutoVO);

//            $objProdutoVO->setId($objItemSolicitacaoVO->getIdProduto()->getId());
            if ($qtdSolicitada < 0) {
                if (($qtdSolicitada * -1) > $qtdDisponivel) {

                    $objBoHelper->addRetornoMensagem("\nProduto " . $nomeProduto . " não possui estoque suficiente. \nSOLICITADO: " . ($qtdSolicitada * -1) . " DISPONÍVEL: " . $qtdDisponivel);
                    $objBoHelper->setChkErro(TRUE);
                }
            }
        }

        ############################################################


        $objBoHelper->validacaoPadraoAlteracao($objSolicitacaoVO);


        if (!$objBoHelper->getChkErro()) {
            try {

                $objSolicitacaoDAO = new SolicitacaoDAO();
                $objBoHelper->setRetorno($objSolicitacaoDAO->alterar($objSolicitacaoVO));
                $objBoHelper->addRetornoMensagem("Alterado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                exit;
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retonando resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza a exclusão da solicitação
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return String
     */
    public function excluir(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty($objSolicitacaoVO->getId())) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG01']);
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objSolicitacaoDAO = new SolicitacaoDAO();
                $objBoHelper->setRetornoMensagem($objSolicitacaoDAO->excluir($objSolicitacaoVO));
                $objBoHelper->addRetornoMensagem("Excluído com sucesso!");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Verifica a existência de um registro
     * @param SolicitacaoVO $objSolicitacaoVO, $exceto = FALSE
     * @return UnidadeVO
     */
    public function existe($objSolicitacaoVO, $exceto = FALSE) {
        $objBoHelper = new BoHelper();

        if (!$objBoHelper->getChkErro()) {
            try {
                $objSolicitacaoDAO = new SolicitacaoDAO();
                $objBoHelper->setRetorno($objSolicitacaoDAO->existe($objSolicitacaoVO, $exceto));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        return $objBoHelper->getRetornoOperacao();
    }

//    # ver se precisa disso #
    public function listarCombo(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();

        try {
            $objSolicitacaoDAO = new SolicitacaoDAO();
            $objBoHelper->setRetorno($objSolicitacaoDAO->listarCombo($objSolicitacaoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }

        return $objBoHelper->getRetorno();
    }

// RAFAEL
    public function relatorioSolicitacaoNaoAtendidas(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();
        # Realizando procedimentos #
        try {
            $objSolicitacaoDAO = new SolicitacaoDAO();
            $objBoHelper->setRetorno($objSolicitacaoDAO->relatorioSolicitacaoNaoAtendidas($objSolicitacaoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi possível realizar a operação.");
        }
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza o antendimento da solicitação
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return String
     */
    public function atender(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty($objSolicitacaoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Id solicitação não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (!$objBoHelper->getChkErro()) {
            try {
                $objSolicitacaoDAO = new SolicitacaoDAO();
                $objSolicitacaoDAO->atender($objSolicitacaoVO);
                $objBoHelper->addRetornoMensagem("Solicitação atendida.");
            } catch (\Exception $ex) {

                throw new \Exception($ex->getMessage());
                //throw new \Exception("Não foi possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza o cancelamento da solicitação
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return String
     */
    public function cancelar(SolicitacaoVO $objSolicitacaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty($objSolicitacaoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Id solicitação não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        
        
        
        
         ##########################################################

        $objItemSolicitacaoAuxBO = new ItemSolicitacaoProdutoBO();

        foreach ($objSolicitacaoVO->getItemSolicitacaoProdutoArrayIterator() as $objItemSolicitacaoVO) {
            if (!empty($objItemSolicitacaoVO->getId())) {
                $objItemSolicitacaoAuxVO = $objItemSolicitacaoAuxBO->listar($objItemSolicitacaoVO)['retornoOperacao'];
                $quantidadeAnterior = (int) $objItemSolicitacaoAuxVO->getQuantidade();
            } else {
                $quantidadeAnterior = 0;
            }

            $objProdutoBO = new ProdutoBO();
            $objProdutoVO = new ProdutoVO();

            $objProdutoVO->setId($objItemSolicitacaoVO->getIdProduto()->getId());
            $objProdutoVO = $objProdutoBO->selecionar($objProdutoVO)['retornoOperacao'];
            $nomeProduto = $objProdutoVO->getNome();


            $novaQuantidade = (int) $objItemSolicitacaoVO->getQuantidade();
            $qtdSolicitada = (int) $quantidadeAnterior - $novaQuantidade;

            $objProdutoVO->setId($objItemSolicitacaoVO->getIdProduto()->getId());
            $qtdDisponivel = (int) $objProdutoBO->listarQuantidade($objProdutoVO);

//            $objProdutoVO->setId($objItemSolicitacaoVO->getIdProduto()->getId());
            if ($qtdSolicitada < 0) {
                if (($qtdSolicitada * -1) > $qtdDisponivel) {

                    $objBoHelper->addRetornoMensagem("\nProduto " . $nomeProduto . " não possui estoque suficiente. \nSOLICITADO: " . ($qtdSolicitada * -1) . " DISPONÍVEL: " . $qtdDisponivel);
                    $objBoHelper->setChkErro(TRUE);
                }
            }
        }

        ############################################################
        if (!$objBoHelper->getChkErro()) {
            try {
                $objSolicitacaoDAO = new SolicitacaoDAO();
                $objSolicitacaoDAO->cancelar($objSolicitacaoVO);
                $objBoHelper->addRetornoMensagem("Solicitação cancelada.");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        return $objBoHelper->getRetornoMensagemImplode();
    }

}
