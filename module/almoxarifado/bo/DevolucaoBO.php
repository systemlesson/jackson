<?php

namespace module\almoxarifado\bo;

use config\SystemConfig;
use core\helper\BoHelper;
use module\almoxarifado\vo\DevolucaoVO;
use module\almoxarifado\dao\DevolucaoDAO;

# Classe de negócio referente à Solicitação #

class DevolucaoBO {

    /**
     * Método que realiza a listagem
     * @param DevolucaoVO $objDevolucaoVO
     * @return ArrayObject
     */
    public function listar(DevolucaoVO $objDevolucaoVO) {
        $objBoHelper = new BoHelper();

        # Realizando procedimentos #
        try {
            $objDevolucaoDAO = new DevolucaoDAO();
            $objBoHelper->setRetorno($objDevolucaoDAO->listar($objDevolucaoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi possível realizar a operação.");
        }
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que visualiza um item da tabela
     * @param DevolucaoVO $objDevolucaoVO
     * @return ArrayObject
     */
    public function selecionar(DevolucaoVO $objDevolucaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty(($objDevolucaoVO->getId()))) {
            $objBoHelper->addRetornoMensagem("Campo ID não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objDevolucaoDAO = new DevolucaoDAO();
                $objBoHelper->setRetorno($objDevolucaoDAO->selecionar($objDevolucaoVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        return $objBoHelper->getRetorno();
    }

    /**
     * Método que visualiza um item da tabela
     * @param DevolucaoVO $objDevolucaoVO
     * @return ArrayObject
     */
    public function visualizar(DevolucaoVO $objDevolucaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty(($objDevolucaoVO->getId()))) {
            $objBoHelper->addRetornoMensagem("Campo ID não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objDevolucaoDAO = new DevolucaoDAO();
                $objBoHelper->setRetorno($objDevolucaoDAO->visualizar($objDevolucaoVO));
            } catch (\Exception $ex) {
                throw new \Exception($objBoHelper->getRetornoMensagemImplode());
            }
        }

        return $objBoHelper->getRetorno();
    }

    public function devolver(DevolucaoVO $objDevolucaoVO) {
        $objBoHelper = new BoHelper();

        # Verificando Regra de Negócio #
        if (empty(($objDevolucaoVO->getIdSaidaProduto()->getId()))){
            $objBoHelper->addRetornoMensagem("Campo ID não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {

                $objDevolucaoDAO = new DevolucaoDAO();
                $objBoHelper->setRetornoOperacao($objDevolucaoDAO->devolver($objDevolucaoVO));
                $objBoHelper->setRetornoMensagem('Produtos devolvidos com sucesso!');
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage());
                //throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        return $objBoHelper->getRetorno();
    }

}
