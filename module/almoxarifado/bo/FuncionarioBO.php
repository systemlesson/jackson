<?php

namespace module\almoxarifado\bo;

use config\SystemConfig;
use core\helper\BoHelper;
use module\almoxarifado\vo\FuncionarioVO;
use module\almoxarifado\dao\FuncionarioDAO;

# Classe de negócio referente a >Funcionário< #

class FuncionarioBO {

    /**
     * Método que realiza a listagem
     * @param FuncionarioVO $objFuncionarioVO
     * @return ArrayObject
     */
    public function listar(FuncionarioVO $objFuncionarioVO) {
        # instanciando classe de apoio da camada #
        $objBoHelper = new BoHelper();

        # Realizando Procedimentos #
        try {
            $objFuncionarioDAO = new FuncionarioDAO();
            $objBoHelper->setRetorno($objFuncionarioDAO->listar($objFuncionarioVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi possível realizar a operação.");
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a listagem
     * @param FuncionarioVO $objFuncionarioVO
     * @return ArrayObject
     */
    public function selecionar(FuncionarioVO $objFuncionarioVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (empty($objFuncionarioVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo ID não pode ficar vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objFuncionarioDAO = new FuncionarioDAO();
                $objBoHelper->setRetorno($objFuncionarioDAO->selecionar($objFuncionarioVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a inclusão
     * @param FuncionarioVO $objFuncionarioVO
     * @return String
     */
    public function inserir(FuncionarioVO $objFuncionarioVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (strlen($objFuncionarioVO->getMatricula()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo – Matrícula!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objFuncionarioVO->getSenha()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo – Senha!");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objFuncionarioVO->getNome()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo – Nome!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objFuncionarioVO->getCpf()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo – CPF!");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objFuncionarioVO->getTelefone()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo – Telefone!");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objFuncionarioVO->getCelular()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo – Celular!");
            $objBoHelper->setChkErro(TRUE);
        }
        if ($this->existeMatricula($objFuncionarioVO, TRUE)) {
            $objBoHelper->addRetornoMensagem("Matricula ja existente!");
            $objBoHelper->setChkErro(TRUE);
        }


        if (!$objBoHelper->getChkErro()) {
            try {
                $objFuncionarioDAO = new FuncionarioDAO();
                $objBoHelper->setRetorno($objFuncionarioDAO->inserir($objFuncionarioVO));
                $objBoHelper->addRetornoMensagem("Adicionado com sucesso!");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a alteração dos dados do funcionário
     * @param FuncionarioVO $objFuncionarioVO
     * @return String
     */
    public function alterar(FuncionarioVO $objFuncionarioVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (strlen($objFuncionarioVO->getNome()) == "") {
            $objBoHelper->addRetornoMensagem("Preencha este campo – Nome!");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objFuncionarioVO->getTelefone()) == "") {
            $objBoHelper->addRetornoMensagem("Preencha este campo – Telefone!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objFuncionarioVO->getSenha()) == "") {
            $objBoHelper->addRetornoMensagem("Preencha este campo – Senha!");
            $objBoHelper->setChkErro(TRUE);
        }

        if (strlen($objFuncionarioVO->getCelular()) == "") {
            $objBoHelper->addRetornoMensagem("Preencha este campo – Celular!");
            $objBoHelper->setChkErro(TRUE);
        }
        if ($this->existeMatricula($objFuncionarioVO, TRUE)) {
            $objBoHelper->addRetornoMensagem("Matricula ja existente!");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objFuncionarioDAO = new FuncionarioDAO();
                $objBoHelper->setRetorno($objFuncionarioDAO->alterar($objFuncionarioVO));
                $objBoHelper->addRetornoMensagem("Alterado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                exit;
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a alteração dos dados do funcionário
     * @param FuncionarioVO $objFuncionarioVO
     * @return String
     */
    public function excluir(FuncionarioVO $objFuncionarioVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (empty($objFuncionarioVO->getId())) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG1']);
            $objBoHelper->setChkErro(1);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objFuncionarioDAO = new FuncionarioDAO();
                $objBoHelper->setRetornoOperacao($objFuncionarioDAO->excluir($objFuncionarioVO));
                $objBoHelper->addRetornoMensagem("Excluído com sucesso.");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Verifica a existência de um registro
     * @param FuncionarioVO $objFuncionarioVO, $exceto = FALSE
     * @return FuncionarioVO
     */
    public function existeMatricula(FuncionarioVO $objFuncionarioVO, $exceto = FALSE) {
        $objBoHelper = new BoHelper();

        if (empty($objFuncionarioVO->getMatricula())) {
            $objBoHelper->addRetornoMensagem("Matricula não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objFuncionarioDAO = new FuncionarioDAO();
                $objBoHelper->setRetornoOperacao($objFuncionarioDAO->existeMatricula($objFuncionarioVO, $exceto));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoOperacao();
    }

    /**
     * Puxa a tabela de Setor para a combo box da tela de Funcionario
     * @param FuncionarioVO $objFuncionarioVO
     * @return String
     */
    public function listarCombo(FuncionarioVO $objFuncionarioVO) {
        $objBoHelper = new BoHelper();

        try {
            $objFuncionarioDAO = new FuncionarioDAO();
            $objBoHelper->setRetorno($objFuncionarioDAO->listarCombo($objFuncionarioVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }
        # Retornando Resultado da operação #
        return $objBoHelper->getRetorno();
    }

    /**
     * Realiza a checagem da regra de negócio referente a autenticação do usuario no Almoxarifado
     * @access protected
     * @param FuncionarioVO $objFuncionarioVO 
     * @return Array Contem informações do retorno (Boolean) e mensagem (String)
     */
    public function autenticar(FuncionarioVO $objFuncionarioVO) {

        $objBoHelper = new BoHelper();

        ## RN ##
        if (strlen($objFuncionarioVO->getCpf()) == 0) {
            $objBoHelper->addRetornoMensagem("O CPF não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objFuncionarioVO->getSenha()) == 0) {
            $objBoHelper->addRetornoMensagem("Senha não pode ser vazio!");
            $objBoHelper->setChkErro(TRUE);
        }

        ## Executando ##

        if (!$objBoHelper->getChkErro()) {

            $objFuncionarioDAO = new FuncionarioDAO();

            try {

                $objBoHelper->setRetorno($objFuncionarioDAO->autenticar($objFuncionarioVO));
                $objBoHelper->setRetornoMensagem("Autenticado com sucesso!");
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage());
            }
        } else {
//            LogManipulador::registrar(__CLASS__, __FUNCTION__, $objBoHelper->getMsgRetorno());          
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

## Retornando ##

        return $objBoHelper->getRetorno();
    }

    public function selecionarByCpf(FuncionarioVO $objFuncionarioVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (empty($objFuncionarioVO->getCpf())) {
            $objBoHelper->addRetornoMensagem("Campo CPF não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }
        if (empty($objFuncionarioVO->getSenha())) {
            $objBoHelper->addRetornoMensagem("Campo Senha não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objFuncionarioDAO = new FuncionarioDAO();
                $objBoHelper->setRetorno($objFuncionarioDAO->selecionarByCpf($objFuncionarioVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetorno();
    }

}
