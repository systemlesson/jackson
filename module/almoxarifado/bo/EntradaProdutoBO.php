<?php

namespace module\almoxarifado\bo;

use config\SystemConfig;
use module\almoxarifado\vo\EntradaProdutoVO;
use module\almoxarifado\dao\EntradaProdutoDAO;
use core\helper\BoHelper;
use core\exception\AppException;
use module\almoxarifado\vo\ProdutoVO;
use module\almoxarifado\bo\ProdutoBO;

/**
 * Classe de negócio referente a (EntradaProduto)
 */
class EntradaProdutoBO {

    /**
     * Método que realiza a listagem
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return ArrayObject
     */
    public function listar(EntradaProdutoVO $objEntradaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  realizando procedimentos
         */
        try {

            $objEntradaProdutoDAO = new EntradaProdutoDAO();
            $objBoHelper->setRetorno($objEntradaProdutoDAO->listar($objEntradaProdutoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a listagem
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return ArrayObject
     */
    public function selecionar($objEntradaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objEntradaProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {

                $objEntradaProdutoDAO = new EntradaProdutoDAO();
                $objBoHelper->setRetorno($objEntradaProdutoDAO->selecionar($objEntradaProdutoVO));
            } catch (\Exception $ex) {
                // var_dump($objBoHelper->getRetornoMensagemImplode(); die;
                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }


        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza inclusão
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return String
     */
    public function inserir(EntradaProdutoVO $objEntradaProdutoVO) {


        $objBoHelper = new BoHelper();
        /**
         *  Verificando Regra de negócio
         */
        if (strlen($objEntradaProdutoVO->getIdFuncionarioUsuario()) == 0) {
            $objBoHelper->addRetornoMensagem("Id funcionario não pode ser vazio! ");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objEntradaProdutoVO->getNomeEntregador()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha o campo Nome do entregador! ");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objEntradaProdutoVO->getUsuarioInclusao()) == 0) {
            $objBoHelper->addRetornoMensagem("Usuário inclusão não pode ser vazio! ");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objEntradaProdutoVO->getDataInclusao()) == 0) {
            $objBoHelper->addRetornoMensagem("Data inclusão não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
//        var_dump($objEntradaProdutoVO);die;


        if (!$objBoHelper->getChkErro()) {

            try {

                $objEntradaProdutoDAO = new EntradaProdutoDAO();
                $objBoHelper->setRetorno($objEntradaProdutoDAO->inserir($objEntradaProdutoVO));
                $objBoHelper->addRetornoMensagem("Adicionado com sucesso!");
            } catch (\Exception $ex) {

                echo $ex->getMessage();
                die;
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoMensagemImplode();
    }

    public function alterar(EntradaProdutoVO $objEntradaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objEntradaProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (empty($objEntradaProdutoVO->getNomeEntregador())) {
            $objBoHelper->addRetornoMensagem("Nome do entregador não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        ##########################################################

        $objItemEntradaProdutoAuxBO = new ItemEntradaProdutoBO();

        foreach ($objEntradaProdutoVO->getItemEntradaProdutoArrayIterator() as $objItemEntradaProdutoVO) {
//            var_dump($objItemEntradaProdutoVO); die;
            if (!empty($objItemEntradaProdutoVO->getId())) {
                $objItemEntradaProdutoAuxVO = $objItemEntradaProdutoAuxBO->listar($objItemEntradaProdutoVO)['retornoOperacao'];
                $quantidadeAnterior = (int) $objItemEntradaProdutoAuxVO->getQuantidade();
            } else {
                $quantidadeAnterior = 0;
            }

            $objProdutoBO = new ProdutoBO();
            $objProdutoVO = new ProdutoVO();

            $objProdutoVO->setId($objItemEntradaProdutoVO->getIdProduto()->getId());
            $objProdutoVO = $objProdutoBO->selecionar($objProdutoVO)['retornoOperacao'];
            $nomeProduto = $objProdutoVO->getNome();

            $novaQuantidade = (int) $objItemEntradaProdutoVO->getQuantidade();
            $qtdSolicitada = (int) $quantidadeAnterior - $novaQuantidade;
            $objProdutoVO->setId($objItemEntradaProdutoVO->getIdProduto()->getId());
            $qtdDisponivel = (int) $objProdutoBO->listarQuantidade($objProdutoVO);
            $objProdutoVO->setId($objItemEntradaProdutoVO->getIdProduto()->getId());
        }

        ############################################################

        if (!$objBoHelper->getChkErro()) {
            try {
                $objEntradaProdutoDAO = new EntradaProdutoDAO();
                $objBoHelper->setRetornoOperacao($objEntradaProdutoDAO->alterar($objEntradaProdutoVO));
                $objBoHelper->setRetornoMensagem("Alterado com sucesso!");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {

            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza exclusão
     * @paramEntradaProdutoVO $objEntradaProdutoVO
     * @return String
     */
    public function excluir(EntradaProdutoVO $objEntradaProdutoVO) {

        $objBoHelper = new BoHelper();
        /**
         *  Verificando Regra de negócio
         */
        if (empty($objEntradaProdutoVO->getId())) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG01']);
            $objBoHelper->setChkErro(TRUE);
        }




        if (!$objBoHelper->getChkErro()) {
            try {

                $objEntradaProdutoDAO = new EntradaProdutoDAO();
                $objBoHelper->setRetornoOperacao($objEntradaProdutoDAO->excluir($objEntradaProdutoVO));
                $objBoHelper->addRetornoMensagem("Excluido com sucesso!");
            } catch (\Exception $ex) {

                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza exclusão
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return String
     */
    public function listarCombo(EntradaProdutoVO $objEntradaProdutoVO) {
        $objBoHelper = new BoHelper();

        try {
            $objEntradaProdutoDAO = new EntradaProdutoDAO();
            $objBoHelper->setRetorno($objEntradaProdutoDAO->listarCombo($objEntradaProdutoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }


        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Verifica a existencia de um registro
     * 
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return EntradaProdutoVO
     */
    public function existe($objEntradaProdutoVO, $exceto = FALSE) {
        $objBoHelper = new BoHelper();

        if (!$objBoHelper->getChkErro()) {
            try {
                $objEntradaProdutoDAO = new EntradaProdutoDAO();
                $objBoHelper->setRetorno($objEntradaProdutoDAO->existe($objEntradaProdutoVO, $exceto));
            } catch (\Exception $ex) {

                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }
        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoOperacao();
    }

    public function relatorioDeEntrada(EntradaProdutoVO $objEntradaProdutoVO) {

        $objBoHelper = new BoHelper();

        /**
         *  realizando procedimentos
         */
        try {

            $objEntradaProdutoDAO = new EntradaProdutoDAO();
            $objBoHelper->setRetorno($objEntradaProdutoDAO->relatorioDeEntrada($objEntradaProdutoVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    public function inserirAlterarItem(EntradaProdutoVO $objEntradaProdutoVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objBoHelper = new BoHelper();



        /**
         *  Realizando procedimentos
         */
        try {

            $objEntradaProdutoDAO = new EntradaProdutoDAO();

            $objBoHelper->setRetorno($objEntradaProdutoDAO->Alterar($objEntradaProdutoVO));
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
            //throw new \Exception("Não foi Possível realizar a operação");
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

}
