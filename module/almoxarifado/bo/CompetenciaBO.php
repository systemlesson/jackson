<?php

namespace module\almoxarifado\bo;

use core\helper\BoHelper;
use module\almoxarifado\vo\CompetenciaVO;
use module\almoxarifado\dao\CompetenciaDAO;
use config\SystemConfig;

/**
 * Classe de negócio referente a competencia
 * @access public
 * @package siapp
 * @subpackage bo
 * 
 */
class CompetenciaBO {

    /**
     * Método que realiza a checagem das regras referente a listagem de competencia
     * @param NteVO $objCompetenciaVO
     * @return ArrayIterator
     * @throws Exception Caso não esteja em conformidade com a RN ou em caso de erro de banco de dados
     */
    public function listar(CompetenciaVO $objCompetenciaVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objBoHelper = new BoHelper();

        /**
         *  Realizando procedimentos
         */
        try {

            $objCompetenciaDAO = new CompetenciaDAO();
            $objBoHelper->setRetorno($objCompetenciaDAO->listar($objCompetenciaVO));
        } catch (\Exception $ex) {
            throw new \Exception("Não foi Possível realizar a operação");
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a inclusão de um  competência
     * @param CompetenciaVO $objCompetenciaVO
     * @return bool
     * @throws Exception Caso não esteja em conformidade com a RN ou em caso de erro de banco de dados
     */
    public function inserir(CompetenciaVO $objCompetenciaVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objCompetenciaVO->getDescricao())) {
            $objBoHelper->addRetornoMensagem("Competência não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        $objBoHelper->validacaoPadraoCadastro($objCompetenciaVO);

        if (!$objBoHelper->getChkErro()) {

            /**
             *  Realizando procedimentos
             */
            try {

                $objCompetenciaDAO = new CompetenciaDAO();
                $objBoHelper->setRetorno($objCompetenciaDAO->inserir($objCompetenciaVO));
                $objBoHelper->setRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG2']);
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a alteracao de um Competências
     * @param CompetenciaVO $objCompetenciaVO
     * @return bool
     * @throws Exception Caso não esteja em conformidade com a RN ou em caso de erro de banco de dados
     */
    public function alterar(CompetenciaVO $objCompetenciaVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objCompetenciaVO->getId())) {
            $objBoHelper->addRetornoMensagem("Id Competência não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (empty($objCompetenciaVO->getDescricao())) {
            $objBoHelper->addRetornoMensagem("Competência não pode ser vazio.");
            $objBoHelper->setChkErro(TRUE);
        }

        $objBoHelper->validacaoPadraoAlteracao($objCompetenciaVO);

        if (!$objBoHelper->getChkErro()) {

            /**
             *  Realizando procedimentos
             */
            try {
                $objCompetenciaDAO = new CompetenciaDAO();
                $objBoHelper->setRetorno($objCompetenciaDAO->alterar($objCompetenciaVO));
                $objBoHelper->setRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG3']);
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a checagem das regras referente a listagem de Competencias
     * @param CompetenciaVO $objCompetenciaVO
     * @return ArrayIterator
     * @throws Exception Caso não esteja em conformidade com a RN ou em caso de erro de banco de dados
     */
    public function listarPorPlanoCursoEixoIntegrador(CompetenciaVO $objCompetenciaVO) {
//faltando fazer!!!!!!!!
        /**
         *  Criando instância da classe de apoio da camada
         */
        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objCompetenciaVO->getIdEixoIntegradorTransient())) {
            $objBoHelper->addRetornoMensagem("id do Eixo Integrador não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (empty($objCompetenciaVO->getIdPlanoCursoTransient())) {
            $objBoHelper->addRetornoMensagem("Id do Plano de curso não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {

            /**
             *  Realizando procedimentos
             */
            try {

                $objCompetenciaDAO = new CompetenciaDAO();
                $objBoHelper->setRetorno($objCompetenciaDAO->listarPorPlanoCursoEixoIntegrador($objCompetenciaVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza a checagem das regras referente a seleção de um determinada Competencia
     * @param CompetenciaVO $objCompetenciaVO
     * @return CompetenciaVO
     * @throws Exception Caso não esteja em conformidade com a RN ou em caso de erro de banco de dados
     */
    public function selecionar(CompetenciaVO $objCompetenciaVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objCompetenciaVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {

            /**
             *  Realizando procedimentos
             */
            try {

                $objCompetenciaDAO = new CompetenciaDAO();
                $objBoHelper->setRetorno($objCompetenciaDAO->selecionar($objCompetenciaVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetorno();
    }

    /**
     * Método que realiza exclusão
     * @param CompetenciaVO $objCompetenciaVO
     * @return String
     */
    public function excluir(CompetenciaVO $objCompetenciaVO) {

        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objCompetenciaVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        } else if ($this->verificaDependencia($objCompetenciaVO)) {
            $objBoHelper->addRetornoMensagem( SystemConfig::SYSTEM_MSG['MSG42']);
            $objBoHelper->setChkErro(TRUE);
        }

        $objBoHelper->validacaoPadraoAlteracao($objCompetenciaVO);

        /**
         *  Checando resultados dos portalpowerbis de validação e realizando procedimentos de persistencia
         */
        if (!$objBoHelper->getChkErro()) {
            try {

                $objCompetenciaDAO = new CompetenciaDAO();

                $objBoHelper->setRetornoOperacao($objCompetenciaDAO->excluir($objCompetenciaVO));
                $objBoHelper->addRetornoMensagem("Excluído com sucesso!");
            } catch (\Exception $ex) {

                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza verificação da dependencia com eixo competencia habilidade
     * @param CompetenciaVO $objCompetenciaVO
     * @return String
     */
    public function verificaDependencia(CompetenciaVO $objCompetenciaVO) {
        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objCompetenciaVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        /**
         *  Checando resultados dos portalpowerbis de validação e realizando procedimentos de persistencia
         */
        if (!$objBoHelper->getChkErro()) {
            try {

                $objCompetenciaDAO = new CompetenciaDAO();

                $objBoHelper->setRetornoOperacao($objCompetenciaDAO->verificaDependencia($objCompetenciaVO));
            } catch (\Exception $ex) {

                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoOperacao();
    }

}
