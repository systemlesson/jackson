<?php

namespace module\almoxarifado\bo;

use core\helper\BoHelper;
use module\almoxarifado\vo\CategoriaVO;
use module\almoxarifado\dao\CategoriaDAO;
use config\SystemConfig;

/**
 * Description of categoriaBO
 *
 * @author carlos.bispo
 */
class CategoriaBO {

    public function listar(CategoriaVO $objCategoriaVO) {
        # Instanciando classe de apoio da camada #
        $objBoHelper = new BoHelper();
        
        # Realizando Procedimentos
        try {
            $objCategoriaDAO = new CategoriaDAO();
            $objBoHelper->setRetorno($objCategoriaDAO->listar($objCategoriaVO));
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi possível realizar a operação.");
        }

        # Retornando resultado da operação
        return $objBoHelper->getRetorno();
    }

    public function inserir($objCategoriaVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio
        if (strlen($objCategoriaVO->getNome()) == 0) {
            $objBoHelper->addRetornoMensagem("Preencha esse campo.()");
            $objBoHelper->setChkErro(TRUE);
        }

//        if ($this->existe($objCategoriaVO)) {
//            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG04']);
//            $objBoHelper->setChkErro(TRUE);
//        }

        if ($this->verificaCategoria($objCategoriaVO, TRUE)) {
            $objBoHelper->addRetornoMensagem("Já existe uma Categoria com este nome.");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objCategoriaDAO = new CategoriaDAO();
                $objBoHelper->setRetorno($objCategoriaDAO->inserir($objCategoriaVO));
                $objBoHelper->addRetornoMensagem("Adicionado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                exit;
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        #Retornando resultado da operação
        return $objBoHelper->getRetornoMensagemImplode();
    }

    // Método que realiza a alteracao de um Categoria
    // @param CategoriaVO $objCategoriaVO
    //@return bool
    //@throws Exception Caso não esteja em conformidade com a RN ou em caso de erro de banco de dados

   public function alterar(CategoriaVO $objCategoriaVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (strlen($objCategoriaVO->getNome() == "")) {
            $objBoHelper->addRetornoMensagem("Preencha esse campo.");
            $objBoHelper->setChkErro(TRUE);
        }

//        if ($this->existe($objUnidadeVO)) {
//            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG04']);
//            $objBoHelper->setChkErro(TRUE);
//        }

        if ($this->verificaCategoria($objCategoriaVO, TRUE)) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG04']);
            $objBoHelper->setChkErro(TRUE);
        }
//        var_dump($objBoHelper); exit;
        if (!$objBoHelper->getChkErro()) {
            try {
                $objCategoriaDAO = new CategoriaDAO();
                $objBoHelper->setRetornoOperacao($objCategoriaDAO->alterar($objCategoriaVO));
                $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG3']);
            } catch (\Exception $ex) {
//                var_dump($objBoHelper->getRetornoOperacao()); die;
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
             throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza a listagem
     * @param CategoriaVO $objCategoriaVO
     * @return ArrayObject
     */
    public function selecionar(CategoriaVO $objCategoriaVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objCategoriaVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        if (!$objBoHelper->getChkErro()) {

            /**
             *  Realizando procedimentos
             */
            try {

                $objCategoriaDAO = new CategoriaDAO();
                $objBoHelper->setRetorno($objCategoriaDAO->selecionar($objCategoriaVO));
                return $objBoHelper->getRetornoOperacao();
            } catch (Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza exclusão
     * @param CategoriaVO $objCategoriaVO
     * @return String
     */
     public function excluir(CategoriaVO $objCategoriaVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #
        if (empty($objCategoriaVO->getId())) { 
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG1']);
            $objBoHelper->setChkErro(1); 
        } 
          
        if (!$objBoHelper->getChkErro()) {
            try {
                $objCategoriaDAO = new CategoriaDAO();
                $objBoHelper->setRetornoOperacao($objCategoriaDAO->excluir($objCategoriaVO));
                $objBoHelper->addRetornoMensagem("Excluído com sucesso.");
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoMensagemImplode();
    }

    /**
     * Método que realiza verificação da dependencia com eixo competencia habilidade
     * @param CompetenciaVO $objCompetenciaVO
     * @return String
     */
    public function verificaDependencia(CategoriaVO $objCategoriaVO) {
        $objBoHelper = new BoHelper();

        /**
         *  Verificando Regra de negócio
         */
        if (empty($objCategoriaVO->getId())) {
            $objBoHelper->addRetornoMensagem("Campo id não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        /**
         *  Checando resultados dos portalpowerbis de validação e realizando procedimentos de persistencia
         */
        if (!$objBoHelper->getChkErro()) {
            try {

                $objCategoriaDAO = new CategoriaDAO();

                $objBoHelper->setRetornoOperacao($objCategoriaDAO->verificaDependencia($objCategoriaVO));
            } catch (\Exception $ex) {

                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        /**
         *  Retornando Resultado da operação
         */
        return $objBoHelper->getRetornoOperacao();
    }

    public function verificaCategoria(CategoriaVO $objCategoriaVO, $exceto = FALSE) {
        $objCategoriaDAO = new CategoriaDAO();
        $objBoHelper = new BoHelper();

        if (!$objBoHelper->getChkErro()) {
            try {
                $objBoHelper->setRetornoOperacao($objCategoriaDAO->verificaCategoria($objCategoriaVO, $exceto));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoOperacao();
    }

    public function existe($objCategoriaVO) {
        $objBoHelper = new BoHelper();

        # Verificando a Regra de Negócio #     
        if (empty($objCategoriaVO->getNome())) {
            $objBoHelper->addRetornoMensagem(SystemConfig::SYSTEM_MSG['MSG4'] . " (Nome)");
            $objBoHelper->setChkErro(1);
        }

        if (!$objBoHelper->getChkErro()) {
            try {
                $objCategoriaDAO = new CategoriaDAO();
                $objBoHelper->setRetorno($objCategoriaDAO->existe($objCategoriaVO));
            } catch (\Exception $ex) {
                throw new \Exception("Não foi possível realizar a operação.");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        # Retornando resultado da operação #
        return $objBoHelper->getRetornoOperacao();
    }

    public function listarCategoria(CategoriaVO $objCategoriaVO) {
        $objBoHelper = new BoHelper();

        try {
            $objCategoriaDAO = new CategoriaDAO();
            $objBoHelper->setRetorno($objCategoriaDAO->listarCategoria($objCategoriaVO));
            return $objBoHelper->getRetornoOperacao();
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception("Não foi Possível realizar a operação");
        }
    }

}
