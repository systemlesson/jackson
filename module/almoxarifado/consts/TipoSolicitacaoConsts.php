<?php
namespace module\almoxarifado\consts;

class TipoSolicitacaoConsts {

    const ATENDIDO = 'A';
    const NAOATENDIDO = 'N';
    const CANCELADO = 'C';
    

    private static $TIPO_SOLICITACAO= array(
        self::NAOATENDIDO => 'Não atendido',
        self::ATENDIDO => 'Atendido',
        self::CANCELADO => 'Cancelado',
    );
    
    public static function getValues() {
        return self::$TIPO_SOLICITACAO;
    }
    
    public static function getValue($codigo) {     
        return self::$TIPO_SOLICITACAO[$codigo];
    }

}
