<?php
namespace module\almoxarifado\consts;

class TipoUsuarioConsts {

    const ADMINISTRADOR = 'A';
    const USUARIO = 'U';
    

    private static $TIPO_USUARIO= array(
        self::ADMINISTRADOR => 'Administrador',
        self::USUARIO => 'Usuário',
    );
    
    public static function getValues() {
        return self::$TIPO_USUARIO;
    }
    
    public static function getValue($codigo) {     
        return self::$TIPO_USUARIO[$codigo];
    }

}
