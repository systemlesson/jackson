<?php
namespace module\almoxarifado\consts;

class ConstsDeSaida {

    const DEVOLVIDO = '1';
    const ATENDIDO = '0';
    
    

    private static $DEVOLVER= array(
        self::DEVOLVIDO => 'Devolvido',
        self::ATENDIDO => 'Atendido',
        
    );
    
    public static function getValues() {
        return self::$DEVOLVER;
    }
    
    public static function getValue($codigo) {     
        return self::$DEVOLVER[$codigo];
    }

}
