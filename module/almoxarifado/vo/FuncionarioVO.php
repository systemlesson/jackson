<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;

class FuncionarioVO extends AbstractVO {

    private $id;
    private $idSetor;
    private $matricula;
    private $nome;
    private $cpf;
    private $telefone;
    private $celular;
    private $tipoUsuario;
    private $senha;
    //TRANSIENTE
    private $execto;

    public function __construct() {
        parent::__construct();
        $this->idSetor = new SetorVO();
    }

    function getId() {
        return $this->id;
    }

    function getIdSetor() {
        return $this->idSetor;
    }

    function getMatricula() {
        return $this->matricula;
    }

    function getNome() {
        return $this->nome;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getCelular() {
        return $this->celular;
    }

    function getTipoUsuario() {
        return $this->tipoUsuario;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIdSetor($idSetor) {
        $this->idSetor = $idSetor;
    }

    function setMatricula($matricula) {
        $this->matricula = $matricula;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setTipoUsuario($tipoUsuario) {
        $this->tipoUsuario = $tipoUsuario;
    }

    public function getExecto() {
        return $this->execto;
    }

    public function setExecto($execto) {
        $this->execto = $execto;
    }
    public function getSenha() {
        return $this->senha;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }

    
    public function bind($array, $prefixo = "") {
        !empty($array["{$prefixo}ID_FUNCIONARIO_USUARIO"]) ? $this->setId(trim($array["{$prefixo}ID_FUNCIONARIO_USUARIO"])) : null;
        !empty($array["{$prefixo}ID_SETOR"]) ? $this->getIdSetor()->setId(trim($array["{$prefixo}ID_SETOR"])) : null; # FK #
        !empty($array["{$prefixo}MATRICULA"]) ? $this->setMatricula(trim($array["{$prefixo}MATRICULA"])) : null;
        !empty($array["{$prefixo}NOME"]) ? $this->setNome(trim($array["{$prefixo}NOME"])) : null;
        !empty($array["{$prefixo}CPF"]) ? $this->setCpf(trim($array["{$prefixo}CPF"])) : null;
        !empty($array["{$prefixo}TELEFONE"]) ? $this->setTelefone(trim($array["{$prefixo}TELEFONE"])) : null;
        !empty($array["{$prefixo}CELULAR"]) ? $this->setCelular(trim($array["{$prefixo}CELULAR"])) : null;
        !empty($array["{$prefixo}TIPO_USUARIO"]) ? $this->setTipoUsuario(trim($array["{$prefixo}TIPO_USUARIO"])) : null;
        !empty($array["{$prefixo}PASSOWORD"]) ? $this->setSenha(trim($array["{$prefixo}PASSOWORD"])) : null;

        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;
    }

}
