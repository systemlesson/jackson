<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;



class CategoriaVO extends AbstractVO {

    private $id;
    private $nome;
            
    
    public function __construct() {
        parent::__construct();
    }
    
   
    function getNome() {
        return $this->nome;
    }

   
    function setNome($nome) {
        $this->nome = $nome;
    }
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

                
    public function bind($array, $prefixo = "") {

 
         !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;
         
         !empty($array["{$prefixo}ID_CATEGORIA"]) ? $this->setId(trim($array["{$prefixo}ID_CATEGORIA"])) : null;
    
         !empty($array["{$prefixo}NOME"]) ? $this->setNome(trim($array["{$prefixo}NOME"])) : null;
        
         !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
         
         !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
      }
        
        }
        
        


