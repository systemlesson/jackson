<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;

class ItemEntradaProdutoVO extends AbstractVO {

    private $id;
    private $idProduto;
    private $idEntradaProdutos;
    private $quantidade;
    //Trasient
    private $EntradaProdutoArrayIterator;

    public function __construct() {
        $this->idProduto = new ProdutoVO;
        $this->idEntradaProdutos = new EntradaProdutoVO;
        $this->EntradaProdutoArrayIterator = new \ArrayIterator();

        parent::__construct();
    }

    public function getId() {
        return $this->id;
    }

    public function getIdProduto() {
        return $this->idProduto;
    }

    public function getIdEntradaProdutos() {
        return $this->idEntradaProdutos;
    }

    public function getQuantidade() {
        return $this->quantidade;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdProduto($idProduto) {
        $this->idProduto = $idProduto;
    }

    public function setIdEntradaProdutos($idEntradaProdutos) {
        $this->idEntradaProdutos = $idEntradaProdutos;
    }

    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

    public function bind($array, $prefixo = "") {
        !empty($array["{$prefixo}ID_ITEM_ENTRADA_PRODUTO"]) ? $this->setId(trim($array["{$prefixo}ID_ITEM_ENTRADA_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_PRODUTO"]) ? $this->getIdProduto()->setId(trim($array["{$prefixo}ID_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_ENTRADA_PRODUTOS"]) ? $this->getIdEntradaProdutos()->setId(trim($array["{$prefixo}ID_ENTRADA_PRODUTOS"])) : null;
        !empty($array["{$prefixo}QUANTIDADE"]) ? $this->setQuantidade(trim($array["{$prefixo}QUANTIDADE"])) : null;
        !empty($array["{$prefixo}NOME"]) ? $this->getIdProduto()->setNome(trim($array["{$prefixo}NOME"])) : null;


        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
        isset($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : 0;
    }

}
