<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;
use module\almoxarifado\vo\DevolucaoVO;

class ItemSolicitacaoProdutoVO extends AbstractVO {

    private $id;
    private $idSolicitacao;
    private $idProduto;
    private $produto;
    private $quantidade;
    
    /* transient */
    private $idDevolucao;

    public function __construct() {
        $this->idProduto = new ProdutoVO();
        $this->idSolicitacao = new SolicitacaoVO();
        $this->idDevolucao = new DevolucaoVO;
        parent::__construct();
    }

    function getId() {
        return $this->id;
    }

    function getIdSolicitacao() {
        return $this->idSolicitacao;
    }

    function getIdProduto() {
        return $this->idProduto;
    }

    function getProduto() {
        return $this->produto;
    }

    function getQuantidade() {
        return $this->quantidade;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIdSolicitacao($idSolicitacao) {
        $this->idSolicitacao = $idSolicitacao;
    }

    function setIdProduto($idProduto) {
        $this->idProduto = $idProduto;
    }

    function setProduto($produto) {
        $this->produto = $produto;
    }

    function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }
    public function getIdDevolucao() {
        return $this->idDevolucao;
    }

    public function setIdDevolucao($idDevolucao) {
        $this->idDevolucao = $idDevolucao;
    }

        
    public function bind($array, $prefixo = "") {
        !empty($array["{$prefixo}ID_ITEM_SOLICITACAO_PRODUTO"]) ? $this->setId(trim($array["{$prefixo}ID_ITEM_SOLICITACAO_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_SOLICITACAO_PRODUTO"]) ? $this->getIdSolicitacao()->setId(trim($array["{$prefixo}ID_SOLICITACAO_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_PRODUTO"]) ? $this->getIdProduto()->setId(trim($array["{$prefixo}ID_PRODUTO"])) : null;
        !empty($array["{$prefixo}PRODUTO"]) ? $this->setProduto(trim($array["{$prefixo}PRODUTO"])) : null;
        !empty($array["{$prefixo}QUANTIDADE"]) ? $this->setQuantidade(trim($array["{$prefixo}QUANTIDADE"])) : null;

        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
        isset($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;
    }

}
