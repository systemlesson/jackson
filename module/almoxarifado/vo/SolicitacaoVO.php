<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;
use module\almoxarifado\vo\ProdutoVO;

class SolicitacaoVO extends AbstractVO {
    
    private $id;
    private $idProduto;
    private $idFuncionarioUsuario;
    private $observacao;
    private $situacao;
        
    // Transient //
    private $itemSolicitacaoProdutoArrayIterator;

    
    public function __construct() {
        parent::__construct();
        $this->idProduto = new ProdutoVO();
        $this->idFuncionarioUsuario = new FuncionarioVO();
        $this->itemSolicitacaoProdutoArrayIterator = new \ArrayIterator;
    }

    function getId() {
        return $this->id;
    }

    function getIdProduto() {
        return $this->idProduto;
    }

    function getIdFuncionarioUsuario() {
        return $this->idFuncionarioUsuario;
    }

    function getObservacao() {
        return $this->observacao;
    }

    function getSituacao() {
        return $this->situacao;
    }

    function getItemSolicitacaoProdutoArrayIterator() {
        return $this->itemSolicitacaoProdutoArrayIterator;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIdProduto($idProduto) {
        $this->idProduto = $idProduto;
    }

    function setIdFuncionarioUsuario($idFuncionarioUsuario) {
        $this->idFuncionarioUsuario = $idFuncionarioUsuario;
    }

    function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    function setSituacao($situacao) {
        $this->situacao = $situacao;
    }

    function setItemSolicitacaoProdutoArrayIterator($itemSolicitacaoProdutoArrayIterator) {
        $this->itemSolicitacaoProdutoArrayIterator = $itemSolicitacaoProdutoArrayIterator;
    }

        
    public function bind($array, $prefixo = "") {
        !empty($array["{$prefixo}ID_SOLICITACAO_PRODUTO"]) ? $this->setId(trim($array["{$prefixo}ID_SOLICITACAO_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_PRODUTO"]) ? $this->getIdProduto()->setId(trim($array["{$prefixo}ID_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_FUNCIONARIO_USUARIO"]) ? $this->getIdFuncionarioUsuario()->setId(trim($array["{$prefixo}ID_FUNCIONARIO_USUARIO"])) : null;
        !empty($array["{$prefixo}OBSERVACAO"]) ? $this->setObservacao(trim($array["{$prefixo}OBSERVACAO"])) : null;
        !empty($array["{$prefixo}SITUACAO"]) ? $this->setSituacao(trim($array["{$prefixo}SITUACAO"])) : null;
        
        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
        !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;
    }
    
}
