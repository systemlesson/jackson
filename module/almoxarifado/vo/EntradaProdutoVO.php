<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;

class EntradaProdutoVO extends AbstractVO {

    private $id;
    private $idFuncionarioUsuario;
    private $nomeEntregador;
    private $observacao;
    private $excluido;

    /* Transient */
    private $itemEntradaProdutoArrayIterator;
    /* transient */
    private $dataInicial;
    /* transient */
    private $dataFinal;

    public function __construct() {
        $this->itemEntradaProdutoArrayIterator = new \ArrayIterator();
        parent::__construct();
    }

    public function getId() {
        return $this->id;
    }

    public function getIdFuncionarioUsuario() {
        return $this->idFuncionarioUsuario;
    }

    public function getNomeEntregador() {
        return $this->nomeEntregador;
    }

    public function getObservacao() {
        return $this->observacao;
    }

    public function getExcluido() {
        return $this->excluido;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setIdFuncionarioUsuario($idFuncionarioUsuario) {
        $this->idFuncionarioUsuario = $idFuncionarioUsuario;
    }

    public function setNomeEntregador($nomeEntregador) {
        $this->nomeEntregador = $nomeEntregador;
    }

    public function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    public function setExcluido($excluido) {
        $this->excluido = $excluido;
    }

    public function getItemEntradaProdutoArrayIterator() {
        return $this->itemEntradaProdutoArrayIterator;
    }

    public function setItemEntradaProdutoArrayIterator($itemEntradaProdutoArrayIterator) {
        $this->itemEntradaProdutoArrayIterator = $itemEntradaProdutoArrayIterator;
    }

    public function getDataInicial() {
        return $this->dataInicial;
    }

    public function getDataFinal() {
        return $this->dataFinal;
    }

    public function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    public function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    public function bind($array, $prefixo = "") {
        !empty($array["{$prefixo}ID_ENTRADA_PRODUTOS"]) ? $this->setId(trim($array["{$prefixo}ID_ENTRADA_PRODUTOS"])) : null;
        !empty($array["{$prefixo}ID_FUNCIONARIO_USUARIO"]) ? $this->setIdFuncionarioUsuario(trim($array["{$prefixo}ID_FUNCIONARIO_USUARIO"])) : null;
        !empty($array["{$prefixo}NOME_ENTREGADOR"]) ? $this->setNomeEntregador(trim($array["{$prefixo}NOME_ENTREGADOR"])) : null;
        !empty($array["{$prefixo}OBSERVACAO"]) ? $this->setObservacao(trim($array["{$prefixo}OBSERVACAO"])) : null;
        !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;


        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
    }

}
