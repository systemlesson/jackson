<?php

namespace module\almoxarifado\vo;
use module\almoxarifado\vo\UnidadeVO;
use module\almoxarifado\vo\CategoriaVO;
use core\vo\AbstractVO;

class ProdutoVO extends AbstractVO {

    private $id;
    private $idUnidade;
    private $idCategoria;
    private $nome;
    private $codigo;
    private $quantidade;
    private $descricao;
    private $observacao;
    private $excluido;

    /* Transient */

    public function __construct() {
        parent::__construct();
        $this->idUnidade = new UnidadeVO();
        $this->idCategoria = new CategoriaVO();
    }

    function getId() {
        return $this->id;
    }

    function getIdUnidade() {
        return $this->idUnidade;
    }

    function getIdCategoria() {
        return $this->idCategoria;
    }

    function getNome() {
        return $this->nome;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getQuantidade() {
        return $this->quantidade;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getObservacao() {
        return $this->observacao;
    }

    function getExcluido() {
        return $this->excluido;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIdUnidade($idUnidade) {
        $this->idUnidade = $idUnidade;
    }

    function setIdCategoria($idCategoria) {
        $this->idCategoria = $idCategoria;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    function setExcluido($excluido) {
        $this->excluido = $excluido;
    }

     function setAlteracao($alteracao) {
        $this->alteracao = $alteracao;
    }
    
    
    public function bind($array, $prefixo = "") {
        !empty($array["{$prefixo}ID_PRODUTO"]) ? $this->setId(trim($array["{$prefixo}ID_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_UNIDADE"]) ? $this->getIdUnidade()->setId(trim($array["{$prefixo}ID_UNIDADE"])) : null;
        !empty($array["{$prefixo}ID_CATEGORIA"]) ? $this->getIdCategoria()->setId(trim($array["{$prefixo}ID_CATEGORIA"])) : null;
        !empty($array["{$prefixo}NOME"]) ? $this->setNome(trim($array["{$prefixo}NOME"])) : null;
        !empty($array["{$prefixo}CODIGO"]) ? $this->setCodigo(trim($array["{$prefixo}CODIGO"])) : null;
        isset($array["{$prefixo}QUANTIDADE"]) ? $this->setQuantidade(trim($array["{$prefixo}QUANTIDADE"])) : null;
        !empty($array["{$prefixo}DESCRICAO"]) ? $this->setDescricao(trim($array["{$prefixo}DESCRICAO"])) : null;
        !empty($array["{$prefixo}OBSERVACAO"]) ? $this->setObservacao(trim($array["{$prefixo}OBSERVACAO"])) : null;
        !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;


        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
    }

}
