<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;
use module\almoxarifado\vo\SolicitacaoVO;

class SaidaProdutoVO extends AbstractVO {

    private $id;
    private $idSolicitacaoProduto;
    private $observacao;
    private $excluido;

    /* Transient */
    private $itemEntradaProdutoArrayIterator;
    private $dataInicial;
    private $dataFinal;
    private $saida;

    public function __construct() {
        $this->itemEntradaProdutoArrayIterator = new \ArrayIterator();
        $this->idSolicitacaoProduto= new SolicitacaoVO();
        parent::__construct();
    }

    function getId() {
        return $this->id;
    }

    function getIdSolicitacaoProduto() {
        return $this->idSolicitacaoProduto;
    }

    function getObservacao() {
        return $this->observacao;
    }

    function getExcluido() {
        return $this->excluido;
    }

    function getItemEntradaProdutoArrayIterator() {
        return $this->itemEntradaProdutoArrayIterator;
    }

    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIdSolicitacaoProduto($idSolicitacaoProduto) {
        $this->idSolicitacaoProduto = $idSolicitacaoProduto;
    }

    function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    function setExcluido($excluido) {
        $this->excluido = $excluido;
    }

    function setItemEntradaProdutoArrayIterator($itemEntradaProdutoArrayIterator) {
        $this->itemEntradaProdutoArrayIterator = $itemEntradaProdutoArrayIterator;
    }

    function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    public function getSaida() {
        return $this->saida;
    }

    public function setSaida($saida) {
        $this->saida = $saida;
    }

    
    public function bind($array, $prefixo = "") {
        !empty($array["{$prefixo}ID_SAIDA_PRODUTO"]) ? $this->setId(trim($array["{$prefixo}ID_SAIDA_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_SOLICITACAO_PRODUTO"]) ? $this->getIdSolicitacaoProduto()->setId(trim($array["{$prefixo}ID_SOLICITACAO_PRODUTO"])) : null;
        !empty($array["{$prefixo}OBSERVACAO"]) ? $this->setObservacao(trim($array["{$prefixo}OBSERVACAO"])) : null;
        !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;
        
        isset($array["{$prefixo}SAIDA"]) ? $this->setSaida(trim($array["{$prefixo}SAIDA"])) : null;

        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
    }

}
