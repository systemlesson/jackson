<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;
use module\almoxarifado\vo\SaidaProdutoVO;

class DevolucaoVO extends AbstractVO {
    private $id;
    private $idSaidaProduto;
    private $idFuncionarioUsuario;
    private $usuarioCadastrado;
    private $observacao;
    
    # Transient #
    private $ItemSolicitacaoProdutoArrayIterator;
    private $dataInicial;
    private $dataFinal;
    
    # Transient #
    public function __construct() {
//        parent::construct();
        $this->idSaidaProduto = new SaidaProdutoVO();
        $this->idFuncionarioUsuario = new FuncionarioVO();
        $this->ItemSolicitacaoProdutoArrayIterator = new \ArrayIterator;
    }

    function getId() {
        return $this->id;
    }

    function getIdSaidaProduto() {
        return $this->idSaidaProduto;
    }

    function getUsuarioCadastrado() {
        return $this->usuarioCadastrado;
    }

    function getObservacao() {
        return $this->observacao;
    }

    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIdSaidaProduto($idSaidaProduto) {
        $this->idSaidaProduto = $idSaidaProduto;
    }

    function setUsuarioCadastrado($usuarioCadastrado) {
        $this->usuarioCadastrado = $usuarioCadastrado;
    }

    function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    public function getItemSolicitacaoProdutoArrayIterator() {
        return $this->ItemSolicitacaoProdutoArrayIterator;
    }

    public function setItemSolicitacaoProdutoArrayIterator($ItemSolicitacaoProdutoArrayIterator) {
        $this->ItemSolicitacaoProdutoArrayIterator = $ItemSolicitacaoProdutoArrayIterator;
    }
    
    function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }
    
    function getIdFuncionarioUsuario() {
        return $this->idFuncionarioUsuario;
    }

    function setIdFuncionarioUsuario($idFuncionarioUsuario) {
        $this->idFuncionarioUsuario = $idFuncionarioUsuario;
    }
     
    public function bind($array, $prefixo = "") {
        !empty($array["{$prefixo}ID_DEVOLUCAO_PRODUTO"]) ? $this->setId(trim($array["{$prefixo}ID_DEVOLUCAO_PRODUTO"])) : null;
        !empty($array["{$prefixo}ID_SAIDA_PRODUTO"]) ? $this->getIdSaidaProduto()->setId(trim($array["{$prefixo}ID_SAIDA_PRODUTO"])) : null;
        !empty($array["{$prefixo}USUARIO_CADSTRADO"]) ? $this->setUsuarioCadastrado(trim($array["{$prefixo}USUARIO_CADSTRADO"])) : null;
        !empty($array["{$prefixo}OBSERVACAO"]) ? $this->setObservacao(trim($array["{$prefixo}OBSERVACAO"])) : null;
        
        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
        !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;
    }

}
