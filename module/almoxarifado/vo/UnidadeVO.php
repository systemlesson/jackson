<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;

class UnidadeVO extends AbstractVO {

    private $id;
    private $nome;
    
    public function __construct() {
        parent::__construct();
    }
    
    /* Transient */
    private $exceto;

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }
    
    function getExceto() {
        return $this->exceto;
    }

    function setExceto($exceto) {
        $this->exceto = $exceto;
    }

        
    public function bind($array, $prefixo = "") {        
        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
        !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;
        
        !empty($array["{$prefixo}ID_UNIDADE"]) ? $this->setId(trim($array["{$prefixo}ID_UNIDADE"])) : null;
        !empty($array["{$prefixo}NOME"]) ? $this->setNome(trim($array["{$prefixo}NOME"])) : null;
    }

}
