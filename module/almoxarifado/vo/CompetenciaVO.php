<?php

namespace module\almoxarifado\vo;

use core\vo\AbstractVO;

class CompetenciaVO extends AbstractVO {

    private $id;
    private $descricao;

    /* Transient */
    private $idEixoIntegradorTransient;
    private $idPlanoCursoTransient;

    public function __construct() {
        parent::__construct();
    }
    public function getId() {
        return $this->id;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getIdEixoIntegradorTransient() {
        return $this->idEixoIntegradorTransient;
    }

    public function getIdPlanoCursoTransient() {
        return $this->idPlanoCursoTransient;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setIdEixoIntegradorTransient($idEixoIntegradorTransient) {
        $this->idEixoIntegradorTransient = $idEixoIntegradorTransient;
    }

    public function setIdPlanoCursoTransient($idPlanoCursoTransient) {
        $this->idPlanoCursoTransient = $idPlanoCursoTransient;
    }

    
    public function bind($array, $prefixo = "") {

        !empty($array["{$prefixo}ID_COMPETENCIA"]) ? $this->setId(trim($array["{$prefixo}ID_COMPETENCIA"])) : null;
        !empty($array["{$prefixo}DESCRICAO"]) ? $this->setDescricao(trim($array["{$prefixo}DESCRICAO"])) : null;
        !empty($array["{$prefixo}ID_PLANO_CURSO"]) ? $this->setIdPlanoCursoTransient(trim($array["{$prefixo}ID_PLANO_CURSO"])) : null;
        !empty($array["{$prefixo}ID_EIXO_INTEGRADOR"]) ? $this->setIdEixoIntegradorTransient(trim($array["{$prefixo}ID_EIXO_INTEGRADOR"])) : null;

        !empty($array["{$prefixo}DATA_INCLUSAO"]) ? $this->setDataInclusao(trim($array["{$prefixo}DATA_INCLUSAO"])) : null;
        !empty($array["{$prefixo}DATA_ALTERACAO"]) ? $this->setDataAlteracao(trim($array["{$prefixo}DATA_ALTERACAO"])) : null;
        !empty($array["{$prefixo}USUARIO_INCLUSAO"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}USUARIO_INCLUSAO"])) : null;
        !empty($array["{$prefixo}USUARIO_ALTERACAO"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}USUARIO_ALTERACAO"])) : null;
        !empty($array["{$prefixo}EXCLUIDO"]) ? $this->setExcluido(trim($array["{$prefixo}EXCLUIDO"])) : null;
    }

}
