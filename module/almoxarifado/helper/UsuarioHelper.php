<?php

namespace module\almoxarifado\helper;

use core\helper\SessionHelper;
use module\almoxarifado\consts\PerfilConsts;
use module\almoxarifado\vo\PermissaoPerfilVO;
use module\almoxarifado\bo\PermissaoPerfilBO;
use module\almoxarifado\vo\ServidorVO;
use module\almoxarifado\vo\UsuarioVO;

class UsuarioHelper {

    /**
     * retorna nivel de qual o nivel de bloqueio dos combos
     * @access public
     * @param \ArrayIterator $arrayItetatorRaseaVO  
     * @return Array Permissões de acesso organizadas
     */
    static public function obterGrupoHierarquia($perfis = null) {

        if ($perfis == null) {
            $perfis = SessionHelper::getSessionValue('segPerfis');
        }
        $hierarquia = '';
        foreach ($perfis as $key => $perfil) {
            $intvalue = PerfilConsts::obterIntValues($perfil);
            if ($intvalue == PerfilConsts::ADMINISTRATIVO) {
                return 'administrativo'; // nte/municipio/escola livre
            }
            if ($intvalue == PerfilConsts::COORDENADOR_CODEB ||
                    $intvalue == PerfilConsts::EQUIPE_CODEB || $intvalue == PerfilConsts::DIRETOR_NTE) {// se direto nte, equipe codeb, coordenador codeb carrega a nte apenas com as opções relativa ao seu cadastro
                $hierarquia = 'nte';
            }
        }
        return empty($hierarquia) ? 'escola' : $hierarquia; //professor diretor e vice carrega lista de escolas
    }

    static public function obterMaiorPerfil() {

        $perfis = SessionHelper::getSessionValue('segPerfis');
        $hierarquia = '';
        foreach ($perfis as $key => $perfil) {
            $intvalue = PerfilConsts::obterIntValues($perfil);
            if ($intvalue == 1) {
                return 'administrativo';
            }
        }
        foreach ($perfis as $key => $perfil) {
            $intvalue = PerfilConsts::obterIntValues($perfil);
            if ($intvalue == 1) {
                return 'administrativo';
            }
        }
        return empty($hierarquia) ? 'escola' : $hierarquia; //professor diretor e vice carrega lista de escolas
    }

    static public function possuiVinculoEscolar(UsuarioVO $objUsuario) {

        if ($objUsuario->getEscolaPrint() != null && !empty($objUsuario->getEscolaPrint())) {
            return true;
        }
    }

    static public function obterListaDistinct($string) {

        $array = explode(',', $string);
        $arrayDiscinct = array();
        $stringDistinct = '';

        if ($string === null) {
            return '';
        }
        if (count($array) == 1) {
            return $string;
        }
        foreach ($array as $item) {
            $arrayDiscinct[$item] = $item;
        }
        foreach ($arrayDiscinct as $item) {
            if (empty($stringDistinct)) {
                $stringDistinct = $item;
            } else {
                $stringDistinct = $stringDistinct . ',' . $item;
            }
        }
        return $stringDistinct;
    }

    static public function getPerfisPorPerfilUsuario() {

        $hierarquia = $this->obterGrupoHierarquia();
        if ($hierarquia === 'administrativo') {
            return PerfilConsts::getValues();
        }
        if ($hierarquia === 'administrativo') {
            return PerfilConsts::getValues();
        }
        return empty($hierarquia) ? 'escola' : $hierarquia; //professor diretor e vice carrega lista de escolas
    }

    static public function filtroPorPerfil() {
        $perfil = SessionHelper::getSessionValue('segPerfis');

        if (self::obterGrupoHierarquia() == 'administrativo') {
            return'';
        }
        $ObjPermissaoPerfilVO = new PermissaoPerfilVO();
        $ObjPermissaoPerfilVO->setArrayPerfis($perfil);
        $objPermissaoPerfilBO = new PermissaoPerfilBO();
        $arrayPermissaoPerfil = $objPermissaoPerfilBO->getPerfisDisponiveis($ObjPermissaoPerfilVO);
        $arrayIdPerfil = array();
        foreach ($arrayPermissaoPerfil['retornoOperacao'] as $item) {
            array_push($arrayIdPerfil, $item->getId());
        }
        return " and NTE.ID_PERFIL in (" . implode(',', $arrayIdPerfil) . ") ";
    }

    static function possuiEscolaAtiva(ServidorVO $objServidorVO) {
        foreach ($objServidorVO->getEscolasArrayIterator() as $escola) {
            if ($escola->getIdStatus() == 1) {
                return true;
            }
        }
        return false;
    }

    static function possuiVinculoNTE(ServidorVO $objServidorVO) {
        foreach ($objServidorVO->getEscolasArrayIterator() as $escola) {
            if ($escola->getIdStatus() == 1) {
                return true;
            }
        }
        return false;
    }

    /*
     * @perfis lista de pefis opcional
     * @perfil a ser verificado
     */

    static public function possuiPerfil($perfis = null, $perfil) {

        if ($perfis == null) {
            $perfis = SessionHelper::getSessionValue('segPerfis');
        }
        foreach ($perfis as $key => $item) {
            $intvalue = PerfilConsts::obterIntValues($key);
            if ($perfil == $intvalue) {
                return true;
            }
        }
        return false;
    }

    /**
     * perfil NTE
     * @param type $perfis
     * @return boolean
     */
    static public function contemPerfilEscolar($perfis = null) {
        $arrayPerfisEscolares = array(PerfilConsts::ARTICULADOR_AREA, PerfilConsts::COORDENADOR_PEDAGOGICO,
            PerfilConsts::DIRETOR_ESCOLAR, PerfilConsts::PROFESSOR, PerfilConsts::VICE_DIRETOR_ESCOLAR, PerfilConsts::DIRETOR_NTE);

        foreach ($perfis as $perfil) {
            if (in_array($perfil->getIdPerfil(), $arrayPerfisEscolares)) {// se direto nte, equipe codeb, coordenador codeb carrega a nte apenas com as opções relativa ao seu cadastro
                return true;
            }
        }
        return false; //professor diretor e vice carrega lista de escolas
    }

    /**
     * perfil NTE
     * @param type $perfis
     * @return boolean
     */
    static public function possuiVinculoEstadual(UsuarioVO $objUsuario) {

        if ($objUsuario->getCadastro() != null && !empty($objUsuario->getCadastro())) {
            return true;
        }
        return false;
    }

    static function usuarioApenasAdministrador(UsuarioVO $objUsuario) {
        foreach ($objUsuario->getIdPerfils() as $perfil) {
            if ($perfil->getIdPerfil() === PerfilConsts::ADMINISTRATIVO) {
                return true;
            }
            return false;
        }
        return false;
    }

    static public function exibeNTE(UsuarioVO $objUsuario) {
        if (self::contemPerfilEscolarOuAdministrativoByArray(explode(',', $objUsuario->getIdPerfils()))) {
            return 'style="display:none"';
        }
        return '';
    }

    static public function contemPerfilEscolarOuAdministrativoByArray($perfis = null) {
        $arrayPerfisEscolares = array(PerfilConsts::ADMINISTRATIVO, PerfilConsts::ARTICULADOR_AREA, PerfilConsts::COORDENADOR_PEDAGOGICO,
            PerfilConsts::DIRETOR_ESCOLAR, PerfilConsts::PROFESSOR, PerfilConsts::VICE_DIRETOR_ESCOLAR);

        if ($perfis == null) {
            $perfis = SessionHelper::getSessionValue('segPerfis');
        }
        foreach ($perfis as $valor) {
            if (in_array($valor, $arrayPerfisEscolares)) {
                return true;
            }
        }
        return false;
    }

    /**
     * perfil NTE
     * @param type $perfis
     * @return boolean
     */
    static public function contemPerfilEscolarLogin($perfis = null) {
        $arrayPerfisEscolares = array(PerfilConsts::ARTICULADOR_AREA, PerfilConsts::COORDENADOR_PEDAGOGICO,
            PerfilConsts::DIRETOR_ESCOLAR, PerfilConsts::PROFESSOR, PerfilConsts::VICE_DIRETOR_ESCOLAR);

        if ($perfis == null) {
            $perfis = SessionHelper::getSessionValue('segPerfis');
        }
        foreach ($perfis as $key => $item) {
            $intvalue = PerfilConsts::obterIntValues($key);
            if (in_array($intvalue, $arrayPerfisEscolares)) {
                return true;
            }
        }
        return false;
    }

}
