<?php

namespace module\almoxarifado\helper;

class LoginHelper {

    /**
     * Realiza a reorganização dos dados coletados
     * @access public
     * @param \ArrayIterator $arrayItetatorRaseaVO  
     * @return Array Permissões de acesso organizadas
     */
    static public function reorganizar($arrayItetatorRaseaVO) {

        $arrayPermissoes = array();
        foreach ($arrayItetatorRaseaVO as $objRaseaVO) {
            $arrayPermissoes[$objRaseaVO->getRecursoNome()][$objRaseaVO->getAcaoNome()] = 1;
        }

        return $arrayPermissoes;
    }

    /**
     * Realiza a checagem da regra de negódio referente a autenticação do usuario no RASEA
     * @access public
     * @param \ArrayIterator $arrayItetatorRaseaVO 
     * @return Array Perfis de acesso do usuário
     */
    static function getArrayPerfis($arrayItetatorRaseaVO) {
        $arrayPerfis = array();
        foreach ($arrayItetatorRaseaVO as $objRaseaVO) {
            $arrayPerfis[$objRaseaVO->getPerfilNome()] = $objRaseaVO->getPerfilDescricao();
        }
        return $arrayPerfis;
    }

    static public function getArrayEscolasProfessor($objUsuarioVO) {
        $escolas = array();
        foreach ($objUsuarioVO->getEscolaCodSecArrayInterator() as $escola) {
            if ($escola->getId() != null) {
                $escolas[$escola->getId()] = $escola->getDescricao();
            }
        }
        return $escolas;
    }

    static public function getArrayNtesProfessor($objNteIterator) {
        $ntes = array();
        foreach ($objNteIterator as $nte) {
            $ntes[$nte->getId()] = $nte->getNome();
        }
        return $ntes;
    }

}
