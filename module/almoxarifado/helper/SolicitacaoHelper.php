<?php

namespace module\almoxarifado\helper;

use core\helper\SessionHelper;
use config\SystemConfig;
use module\almoxarifado\vo\SolicitacaoVO;

class SolicitacaoHelper {

    static public function generateInit($objArrayIt) {
         $valor = array(
            'view' => array(),
            'dados' => array()
        );

        foreach ($objArrayIt as $value) {
            /* @var $value SolicitacaoVO */
            
            $valor['dados'][] = array(
                'ID_ITEM_SOLICITACAO_PRODUTO' => $value->getId(),
                'ID_PRODUTO' => $value->getIdProduto()->getId(),
                'QUANTIDADE' => $value->getQuantidade(),
                'EXCLUIDO' => $value->getExcluido());
                

            $valor['view'][] = array(
                'ID_ITEM_SOLICITACAO_PRODUTO' => $value->getId(),
                'ID_PRODUTO' => $value->getIdProduto()->getNome(),
                'QUANTIDADE' => $value->getQuantidade(),
                'EXCLUIDO' => $value->getExcluido()
            );
        }
        
//        var_dump($valor); exit();
        return json_encode($valor);
    }

}
