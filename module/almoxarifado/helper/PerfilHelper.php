<?php

namespace module\almoxarifado\helper;

use core\helper\SessionHelper;
use module\almoxarifado\consts\PerfilConsts;
use module\almoxarifado\vo\PermissaoPerfilVO;
use module\almoxarifado\bo\PermissaoPerfilBO;
use module\almoxarifado\vo\ServidorVO;

class PerfilHelper {

    static function getArrayPerfilsCombo($objArrayPerfis, $removeEscolares = false) {
        $arrayPerfisEscolares = array(PerfilConsts::ARTICULADOR_AREA, PerfilConsts::COORDENADOR_PEDAGOGICO,
            PerfilConsts::DIRETOR_ESCOLAR, PerfilConsts::PROFESSOR, PerfilConsts::VICE_DIRETOR_ESCOLAR,  PerfilConsts::DIRETOR_NTE);
        $arrayPerfil = null;
        foreach ($objArrayPerfis as $perfil) {
            
            if ($removeEscolares && !in_array($perfil->getId(), $arrayPerfisEscolares)) {
                $arrayPerfil[$perfil->getId()] = $perfil->getDescricaoPerfil();
            } elseif (!$removeEscolares) {
                $arrayPerfil[$perfil->getId()] = $perfil->getDescricaoPerfil();
            }
        }
        return $arrayPerfil;
    }

}
