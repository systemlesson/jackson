<?php

namespace module\almoxarifado\dao;

use core\exception\AppException;
use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\vo\EntradaProdutoVO;
use module\almoxarifado\bo\ItemEntradaProdutoBO;
use module\almoxarifado\vo\ItemEntradaProdutoVO;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;

/**
 * Classe de persistencia EntradaProduto
 */
class EntradaProdutoDAO extends AbstractDAO {

    /**
     * @param void
     * @return ArrayIterator
     * @access public
     */
    public function listar(EntradaProdutoVO $objEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexão
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";



            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        EP.ID_ENTRADA_PRODUTOS,   
                                        EP.NOME_ENTREGADOR,
                                        EP.OBSERVACAO,
                                        EP.DATA_INCLUSAO,
                                        FU.NOME
                                    FROM NOVOFRAMEWORK.ENTRADA_PRODUTO EP
                                    INNER JOIN NOVOFRAMEWORK.FUNCIONARIO_USUARIO FU ON EP.ID_FUNCIONARIO_USUARIO = FU.ID_FUNCIONARIO_USUARIO
                                    WHERE 
                                        EP.EXCLUIDO = :EXCLUIDO
                                        AND (EP.NOME_ENTREGADOR = :NOME_ENTREGADOR OR :NOME_ENTREGADOR IS NULL)
                                        ");



            // Atribuindo valores

            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            $objDaoHelper->bindValue(":NOME_ENTREGADOR", $objEntradaProdutoVO->getNomeEntregador());

//              echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute();

            // Instanciando classes de apoio
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $entradaProduto) {
                $objEntradaProdutoVO = new EntradaProdutoVO();
                /* @var $objEntradaProdutoVO EntradaProdutoVO */
                $objEntradaProdutoVO->bind($entradaProduto);
                $arrayIterator->append($objEntradaProdutoVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new AppException($ex->getMessage());
        }
        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return boolean
     * @access public
     */
    public function inserir(EntradaProdutoVO $objEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();


        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            //Setando Id do grupo
            $objEntradaProdutoVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_ENTRADA_PRODUTO", $objDaoHelper->getConexao()));

            // Comando SQL
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.ENTRADA_PRODUTO EP
                                  (
                                   EP.ID_ENTRADA_PRODUTOS,
                                   EP.ID_FUNCIONARIO_USUARIO,
                                   EP.NOME_ENTREGADOR,
                                   EP.OBSERVACAO,
                                   EP.EXCLUIDO,
                                   EP.USUARIO_INCLUSAO,
                                   EP.DATA_INCLUSAO)   
                                   
                                 VALUES 
                                 ( 
                                   :ID_ENTRADA_PRODUTOS,
                                   :ID_FUNCIONARIO_USUARIO,
                                   :NOME_ENTREGADOR,
                                   :OBSERVACAO,
                                   :EXCLUIDO,
                                   :USUARIO_INCLUSAO,
                                    sysdate
                                             )
                                     ");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_ENTRADA_PRODUTOS", $objEntradaProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objEntradaProdutoVO->getIdFuncionarioUsuario());
            $objDaoHelper->bindValue(":NOME_ENTREGADOR", $objEntradaProdutoVO->getNomeEntregador());
            $objDaoHelper->bindValue(":OBSERVACAO", $objEntradaProdutoVO->getObservacao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1

            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objEntradaProdutoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objEntradaProdutoVO->getUsuarioInclusao());

            // Executando comando
            //  echo($objDaoHelper->getSql());die;
            $objDaoHelper->execute(false);


            $objItemEntradaProdutoBO = new ItemEntradaProdutoBO;
            foreach ($objEntradaProdutoVO->getItemEntradaProdutoArrayIterator() as $objItemEntradaProdutoVO) {
                $objProdutoVO = new ProdutoVO;
                $objProdutoBO = new ProdutoBO;

                /* @var $objItemEntradaProdutoVO ItemEntradaProdutoVO */
                $objItemEntradaProdutoVO->getIdEntradaProdutos()->setId($objEntradaProdutoVO->getId());
                $objItemEntradaProdutoBO->inserir($objItemEntradaProdutoVO);
//               
            }

            $objDaoHelper->commit();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objDaoHelper->setRetornoOperacao(TRUE);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return mixed
     * @access public
     */
    public function alterar(EntradaProdutoVO $objEntradaProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL (checar) #
            $objDaoHelper->setSql(" UPDATE 
                                            NOVOFRAMEWORK.ENTRADA_PRODUTO
                                    SET 
                                            NOME_ENTREGADOR         = :NOME_ENTREGADOR,
                                            OBSERVACAO              = :OBSERVACAO,
                                            DATA_ALTERACAO          = SYSDATE,                                          
                                            USUARIO_ALTERACAO       = :USUARIO_ALTERACAO,
                                            EXCLUIDO                = :EXCLUIDO                                                                               
                                    WHERE  
                                            ID_ENTRADA_PRODUTOS     = :ID_ENTRADA_PRODUTOS
                                    ");

            # Atribuindo valores 
            $objDaoHelper->bindValue(":ID_ENTRADA_PRODUTOS", $objEntradaProdutoVO->getId());
            $objDaoHelper->bindValue(":NOME_ENTREGADOR", $objEntradaProdutoVO->getNomeEntregador());
            $objDaoHelper->bindValue(":OBSERVACAO", $objEntradaProdutoVO->getObservacao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objEntradaProdutoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
//            echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute(false);

            $objItemEntradaProdutoBO = new ItemEntradaProdutoBO();

            foreach ($objEntradaProdutoVO->getItemEntradaProdutoArrayIterator() as $objItemEntradaProdutoVO) {
                $objItemEntradaProdutoVO->getIdEntradaProdutos()->setId($objEntradaProdutoVO->getId());
//                $objItemEntradaProdutoVO->setId($objEntradaProdutoVO->getId());
                
                if (empty($objItemEntradaProdutoVO->getId())) {
                    $objItemEntradaProdutoBO->inserir($objItemEntradaProdutoVO);
                } else {
//                    die('STOP');
                    $objItemEntradaProdutoBO->alterar($objItemEntradaProdutoVO);
                }
            }
            # Executando comando #
            $objDaoHelper->execute(TRUE);
            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #

            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
        # Retorno da Resposta #
        return $objDaoHelper->getRetornoOperacao();
    }

    /**
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return boolean
     * @access public
     */
    public function excluir($objEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.ENTRADA_PRODUTO
                                    SET    
                                           DATA_ALTERACAO    = sysdate,
                                           USUARIO_ALTERACAO = :USUARIO_ALTERACAO,
                                           EXCLUIDO          = :EXCLUIDO
                                    WHERE  
                                           ID_ENTRADA_PRODUTOS        = :ID_ENTRADA_PRODUTOS");


            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_ENTRADA_PRODUTOS", $objEntradaProdutoVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objEntradaProdutoVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objEntradaProdutoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 1); //ATIVO 0 -- INATIVO 1
//            echo $objDaoHelper->getSql();die;
            // Executando comando
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objDaoHelper->setRetornoOperacao(TRUE);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param EntradaProdutoVO $objEntradaProdutoVO
     * @return boolean
     * @access public
     */
    public function selecionar(EntradaProdutoVO $objEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        EP.ID_ENTRADA_PRODUTOS,
                                        EP.ID_FUNCIONARIO_USUARIO,
                                        EP.NOME_ENTREGADOR,
                                        EP.OBSERVACAO,
                                        EP.EXCLUIDO,
                                        EP.USUARIO_INCLUSAO,
                                        EP.DATA_INCLUSAO     
                                        FROM NOVOFRAMEWORK.ENTRADA_PRODUTO EP 
                                   WHERE     
                                        EP.EXCLUIDO = :EXCLUIDO
                                        AND ID_ENTRADA_PRODUTOS = :ID_ENTRADA_PRODUTOS");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_ENTRADA_PRODUTOS", $objEntradaProdutoVO->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
//            echo $objDaoHelper->getSql();exit();
            // Executando comando            
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objEntradaProdutoVO = new EntradaProdutoVO();
            foreach ($objDaoHelper->fetchAll() as $entradaProduto) {

                $objEntradaProdutoVO->bind($entradaProduto);
            }

            $objDaoHelper->setRetornoOperacao($objEntradaProdutoVO);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    public function relatorioDeEntrada(EntradaProdutoVO $objEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";


            //Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        EP.ID_ENTRADA_PRODUTOS, 
                                        EP.ID_FUNCIONARIO_USUARIO, 
                                        EP.NOME_ENTREGADOR, 
                                        EP.DATA_INCLUSAO, 
                                        EP.OBSERVACAO 
                                    FROM
                                        NOVOFRAMEWORK.ENTRADA_PRODUTO EP 
                                    WHERE 
                                        EXCLUIDO = '0' 
                                        AND (EP.DATA_INCLUSAO BETWEEN :DATA_INICIAL and :DATA_FINAL OR :DATA_INICIAL IS NULL OR :DATA_FINAL IS NULL)");


            // Atribuindo valores
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            $objDaoHelper->bindValue(":DATA_FINAL", $objEntradaProdutoVO->getDataFinal());
            $objDaoHelper->bindValue(":DATA_INICIAL", $objEntradaProdutoVO->getDataInicial());
//            var_dump($objEntradaProdutoVO->getDataInclusao()); die;
//            echo $objDaoHelper->getSql();
//            exit();
            $objDaoHelper->execute();

            // Instanciando classes  apoio
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $relatorio) {
                $objEntradaProdutoVO = new EntradaProdutoVO();
                $objEntradaProdutoVO->bind($relatorio);
                $arrayIterator->append($objEntradaProdutoVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }
        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param RelatorioProdutoVO $objRelatorioProdutoVO
     * @return boolean
     * @access public
     */
}
