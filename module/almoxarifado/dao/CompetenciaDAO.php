<?php

namespace module\almoxarifado\dao;

use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\vo\CompetenciaVO;

/**
 * Classe de persistência referente a Competencia
 * @access public
 * @package siapp
 * @subpackage dao
 */
class CompetenciaDAO extends AbstractDAO {

    /**
     * Método que realiza a listagem de Competencias ativas
     * @param CompetenciaVO $objCompetenciaVO
     * @return ArrayIterator
     * @throws Exception Em caso de erro de banco de dados
     */
    public function listar(CompetenciaVO $objCompetenciaVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objDaoHelper = new DaoHelper();

        try {
            /**
             *  Obtendo conexao
             */
            $objDaoHelper->setConexao(parent::getInstance('SIAPP'));

            /**
             *  Comando SQL
             */
            $objDaoHelper->setSql(" SELECT 
                                            CO.ID_COMPETENCIA,
                                            CO.DESCRICAO,                                            
                                            CO.EXCLUIDO
                                    FROM SIAPP.COMPETENCIA CO
                                    WHERE CO.EXCLUIDO  = :EXCLUIDO                                    
                                    AND (  UPPER( CO.DESCRICAO ) like UPPER( :DESCRICAO ) OR :DESCRICAO IS NULL)
                                    order by CO.DESCRICAO asc
                                    ");
            
            /**
             *  Atribuindo valores
             */
            
            $objDaoHelper->bindValue(":EXCLUIDO", 0, false);
            $objDaoHelper->bindValue(":DESCRICAO", '%' . $objCompetenciaVO->getDescricao() . '%');


            /**
             *  Executando comando
             */
            //echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute();

            /**
             *  Construindo resultado
             */
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $competencia) {
                $objCompetenciaVO = new CompetenciaVO();
                $objCompetenciaVO->bind($competencia);
                $arrayIterator->append($objCompetenciaVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            /**
             *  Fechando conexão
             */
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        /**
         *  Retornando resultado
         */
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que adiciona as competencias 
     * @param CompetenciaVO $objCompetenciaVO
     * @return ArrayIterator
     * @throws Exception Em caso de erro de banco de dados
     */
    public function inserir(CompetenciaVO $objCompetenciaVO) {

        $objDaoHelper = new DaoHelper();

        try {
            $objDaoHelper->setConexao(parent::getInstance('SIAPP'));

            $objCompetenciaVO->setId($this->getSequenceNextVal("SIAPP.SEQ_COMPETENCIA", $objDaoHelper->getConexao()));

            $objDaoHelper->setSql("INSERT INTO SIAPP.COMPETENCIA
                                (
                                    ID_COMPETENCIA, 
                                    DESCRICAO,
                                    USUARIO_INCLUSAO, 
                                    DATA_INCLUSAO,
                                    EXCLUIDO
                                ) 
                                 VALUES
                                 ( 
                                    :ID_COMPETENCIA,
                                    :DESCRICAO,
                                    :USUARIO_INCLUSAO,
                                    :DATA_INCLUSAO,
                                    :EXCLUIDO)"
            );


            $objDaoHelper->bindValue(":ID_COMPETENCIA", $objCompetenciaVO->getId(), false);
            $objDaoHelper->bindValue(":DESCRICAO", $objCompetenciaVO->getDescricao());
            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objCompetenciaVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objCompetenciaVO->getUsuarioInclusao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0, false);

            //echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute();


            $objDaoHelper->setRetornoOperacao(TRUE);

            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        return $objDaoHelper->getRetorno();
    }

    public function alterar(CompetenciaVO $objCompetenciaVO) {

        $objDaoHelper = new DaoHelper();

        try {
            $objDaoHelper->setConexao(parent::getInstance('SIAPP'));

            $objDaoHelper->setSql("UPDATE SIAPP.COMPETENCIA
                                    SET    
                                           DESCRICAO          = :DESCRICAO,
                                           USUARIO_ALTERACAO  = :USUARIO_ALTERACAO,
                                           DATA_ALTERACAO     = :DATA_ALTERACAO
                                    WHERE  
                                           ID_COMPETENCIA = :ID_COMPETENCIA");

            $objDaoHelper->bindValue(":ID_COMPETENCIA", $objCompetenciaVO->getId(), false);
            $objDaoHelper->bindValue(":DESCRICAO", $objCompetenciaVO->getDescricao());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objCompetenciaVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objCompetenciaVO->getUsuarioAlteracao());

            // echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute();

            $objDaoHelper->setRetornoOperacao(TRUE);

            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que realiza a listagem de Competencias ativas
     * @param CompetenciaVO $objCompetenciaVO
     * @return ArrayIterator
     * @throws Exception Em caso de erro de banco de dados
     */
    public function listarPorPlanoCursoEixoIntegrador(CompetenciaVO $objCompetenciaVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objDaoHelper = new DaoHelper();

        try {
            /**
             *  Obtendo conexao
             */
            $objDaoHelper->setConexao(parent::getInstance('SIAPP'));

            /**
             *  Comando SQL
             */
            $objDaoHelper->setSql(" SELECT distinct ECA.ID_EIXO_INTEGRADOR, C.ID_COMPETENCIA, C .DESCRICAO 
                                    FROM SIAPP.EIXO_COMPETENCIA_HABILIDADE ECA
                                    JOIN SIAPP.COMPETENCIA C ON C.ID_COMPETENCIA = ECA.ID_COMPETENCIA 
                                    JOIN SIAPP.PLANO_CURSO PC ON PC.ID_PLANO_CURSO = :ID_PLANO_CURSO
                                    WHERE ECA.ID_EIXO_INTEGRADOR  IN(:ID_EIXO_INTEGRADOR)
                                    AND ECA.ID_DISCIPLINA = PC.ID_COMPONENTE_CURRICULAR
                                    AND ECA.ID_SERIE = PC.ID_SERIE
                                    AND ECA.ID_MODALDADE = PC.ID_MODALIDADE
                                    order by C.ID_COMPETENCIA
                                    ");

            /**
             *  Atribuindo valores
             */
            $objDaoHelper->bindValue(":ID_EIXO_INTEGRADOR", $objCompetenciaVO->getIdEixoIntegradorTransient(), false);
            $objDaoHelper->bindValue(":ID_PLANO_CURSO", $objCompetenciaVO->getIdPlanoCursoTransient(), false);

            /**
             *  Executando comando
             */
            $objDaoHelper->execute();

            /**
             *  Construindo resultado
             */
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $competencia) {
                $objCompetenciaVO = new CompetenciaVO();
                $objCompetenciaVO->bind($competencia);
                $arrayIterator->append($objCompetenciaVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            /**
             *  Fechando conexão
             */
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        /**
         *  Retornando resultado
         */
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que realiza a seleção de um determinado Competencia
     * @param CompetenciaVO $objCompetenciaVO
     * @return CompetenciaVO
     * @throws Exception Em caso de erro de banco de dados
     */
    public function selecionar(CompetenciaVO $objCompetenciaVO) {

        /**
         *  Criando instância da classe de apoio da camada
         */
        $objDaoHelper = new DaoHelper();

        try {

            /**
             *  Obtendo conexao
             */
            $objDaoHelper->setConexao(parent::getInstance('SIAPP'));

            /**
             *  Comando SQL
             */
            $objDaoHelper->setSql("SELECT 
                                       ID_COMPETENCIA, DESCRICAO, USUARIO_INCLUSAO, 
                                       USUARIO_ALTERACAO, DATA_INCLUSAO, DATA_ALTERACAO, 
                                       EXCLUIDO
                                    FROM SIAPP.COMPETENCIA
                                    WHERE EXCLUIDO = :EXCLUIDO
                                    AND ID_COMPETENCIA = :ID_COMPETENCIA
                                    ");

            /**
             *  Atribuindo valores
             */
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":ID_COMPETENCIA", $objCompetenciaVO->getId());

            /**
             *  Executando comando
             */
            $objDaoHelper->execute();

            /**
             *  Construindo resultado
             */
            foreach ($objDaoHelper->fetchAll() as $competencia) {
                $objCompetenciaVO = new CompetenciaVO();
                $objCompetenciaVO->bind($competencia);
            }

            $objDaoHelper->setRetornoOperacao($objCompetenciaVO);

            /**
             *  Fechando conexão
             */
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        /**
         *  Retornando resultado
         */
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param CompetenciaVO $objCompetenciaVO
     * @return boolean
     * @access public
     */
    public function excluir(CompetenciaVO $objCompetenciaVO) {


        $objDaoHelper = new DaoHelper();


        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('SIAPP'));

            // Comando SQL
            $objDaoHelper->setSql("UPDATE SIAPP.COMPETENCIA
                                    SET    DATA_ALTERACAO    = :DATA_ALTERACAO,
                                           EXCLUIDO          = :EXCLUIDO,
                                           USUARIO_ALTERACAO = :USUARIO_ALTERACAO
                                    WHERE  ID_COMPETENCIA        = :ID_COMPETENCIA");


            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_COMPETENCIA", $objCompetenciaVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objCompetenciaVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objCompetenciaVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 1);


            // Executando comando
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objDaoHelper->setRetornoOperacao(TRUE);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param CompetenciaVO $objCompetenciaVO
     * @return boolean
     * @access public
     */
    public function verificaDependencia(CompetenciaVO $objCompetenciaVO) {


        $objDaoHelper = new DaoHelper();


        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('SIAPP'));

            // Comando SQL
            $objDaoHelper->setSql("SELECT
                                         count(ID_COMPETENCIA) as TOTAL
                                    FROM SIAPP.EIXO_COMPETENCIA_HABILIDADE
                                    WHERE 
                                        ID_COMPETENCIA = :ID_COMPETENCIA
                                    ");


            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_COMPETENCIA", $objCompetenciaVO->getId());
            // Executando comando            
            $objDaoHelper->execute();

            foreach ($objDaoHelper->fetchAll() as $competencia) {
                if ($competencia['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetornoOperacao();
    }

}
