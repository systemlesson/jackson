<?php

namespace module\almoxarifado\dao;

use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\vo\ItemEntradaProdutoVO;
use module\almoxarifado\bo\ItemEntradaProdutoBO;
use module\almoxarifado\vo\EntradaProdutoVO;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;

/**
 * Classe de persistencia ItemEntradaProduto
 */
class ItemEntradaProdutoDAO extends AbstractDAO {

    /**
     * @param ItemEntradaProdutoVO $objItemEntradaProdutoVO
     * @return boolean
     * @access public
     */
    public function inserir(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();


        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            //Setando Id do grupo
            $objItemEntradaProdutoVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_ITEM_ENTRADA_PRODUTO", $objDaoHelper->getConexao()));

            // Comando SQL
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.ITEM_ENTRADA_PRODUTO IP
                                  (
                                   IP.ID_ITEM_ENTRADA_PRODUTO,
                                   IP.ID_PRODUTO,
                                   IP.ID_ENTRADA_PRODUTOS,
                                   IP.QUANTIDADE,
                                   IP.EXCLUIDO,
                                   IP.USUARIO_INCLUSAO,
                                   IP.DATA_INCLUSAO)                                    
                                 VALUES 
                                 ( 
                                   :ID_ITEM_ENTRADA_PRODUTOS,
                                   :ID_PRODUTO,
                                   :ID_ENTRADA_PRODUTO,
                                   :QUANTIDADE,
                                   :EXCLUIDO,
                                   :USUARIO_INCLUSAO,
                                   SYSDATE
                                             )
                                     ");

            // Atribuindo valores

            $objDaoHelper->bindValue(":ID_ITEM_ENTRADA_PRODUTOS", $objItemEntradaProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_PRODUTO", $objItemEntradaProdutoVO->getIdProduto()->getId());
            $objDaoHelper->bindValue(":ID_ENTRADA_PRODUTO", $objItemEntradaProdutoVO->getIdEntradaProdutos()->getId());
            $objDaoHelper->bindValue(":QUANTIDADE", $objItemEntradaProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objItemEntradaProdutoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objItemEntradaProdutoVO->getUsuarioInclusao());

            // Executando comando
//            echo($objDaoHelper->getSql()); die;
            $objDaoHelper->execute(false);
            

            
                $objProdutoVO = new ProdutoVO;
                $objProdutoBO = new ProdutoBO;

                /* @var $objItemEntradaProdutoVO ItemEntradaProdutoVO */
                $objProdutoVO->setId($objItemEntradaProdutoVO->getIdProduto()->getId());
                $objProdutoVO->setQuantidade($objItemEntradaProdutoVO->getQuantidade());
                $objProdutoBO->adicionarProdutoEstoque($objProdutoVO);
            
            
            $objDaoHelper->commit();
            
            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objDaoHelper->setRetornoOperacao(TRUE);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    public function selecionar(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("SELECT
                                        IP.ID_ITEM_ENTRADA_PRODUTO,
                                        IP.ID_PRODUTO,
                                        IP.ID_ENTRADA_PRODUTOS,
                                        IP.QUANTIDADE,
                                        IP.EXCLUIDO,
                                        IP.USUARIO_INCLUSAO,
                                        IP.DATA_INCLUSAO
                                        FROM NOVOFRAMEWORK.ITEM_ENTRADA_PRODUTO IP 
                                   WHERE     
                                        IP.EXCLUIDO = :EXCLUIDO
                                        AND ID_ITEM_ENTRADA_PRODUTO = :ID_ITEM_ENTRADA_PRODUTO");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_ITEM_ENTRADA_PRODUTO", $objItemEntradaProdutoVO->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
//            echo $objDaoHelper->getSql();exit();
            // Executando comando            
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objItemEntradaProdutoVO = new ItemEntradaProdutoVO();
            foreach ($objDaoHelper->fetchAll() as $itemProduto) {
                $objItemEntradaProdutoVO->bind($itemProduto);
            }
            $objDaoHelper->setRetornoOperacao($objItemEntradaProdutoVO);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }
        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    public function alterar(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { /* Obtendo conexao */
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            $objProdutoBO = new ProdutoBO;
            $objProdutoVO = new ProdutoVO;

            $objItemEntradaProdutoAuxBO = new ItemEntradaProdutoBO();
            $objItemEntradaProdutoAuxVO = $objItemEntradaProdutoAuxBO->listar($objItemEntradaProdutoVO)['retornoOperacao'];
            $quantidadeAnterior = $objItemEntradaProdutoAuxVO->getQuantidade();
//            var_dump($quantidadeAnterior); die;
            $novaQuantidade = $objItemEntradaProdutoVO->getQuantidade();
            $qtdSolicitada = $quantidadeAnterior - $novaQuantidade;
            $objProdutoVO->setId($objItemEntradaProdutoVO->getIdProduto()->getId());
            
//            var_dump($qtdSolicitada); die;
            if ($qtdSolicitada < 0) {
                $objProdutoVO->setQuantidade($qtdSolicitada * -1);
                $objProdutoBO->adicionarProdutoEstoque($objProdutoVO);
            } else  if ($qtdSolicitada > 0){
                $objProdutoVO->setQuantidade($qtdSolicitada);
                $objProdutoBO->retirarProdutoEstoque($objProdutoVO);
            }



            /* Comando SQL */
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.ITEM_ENTRADA_PRODUTO
                                        SET    
                                                QUANTIDADE                  = :QUANTIDADE,
                                                DATA_ALTERACAO              = SYSDATE,
                                                USUARIO_ALTERACAO           = :USUARIO_ALTERACAO,
                                                EXCLUIDO                    = :EXCLUIDO
                                        WHERE  
                                                ID_ITEM_ENTRADA_PRODUTO     = :ID_ITEM_ENTRADA_PRODUTO
                                                                                                        ");

            /* Atribuindo valores */
            $objDaoHelper->bindValue(":ID_ITEM_ENTRADA_PRODUTO", $objItemEntradaProdutoVO->getId());
            $objDaoHelper->bindValue(":QUANTIDADE", $novaQuantidade);
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objItemEntradaProdutoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", $objItemEntradaProdutoVO->getExcluido());

//            echo $objDaoHelper->getSql(); die;
            $objDaoHelper->execute(false);

            /* Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. */
            $objDaoHelper->setRetornoOperacao(TRUE);

            /* Fechando conexão */
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        /* Retornando resposta */
        return $objDaoHelper->getRetorno();
    }

    public function listarItemEntradaProduto(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        IP.ID_ITEM_ENTRADA_PRODUTO,
                                        IP.ID_ENTRADA_PRODUTOS,
                                        IP.ID_PRODUTO,
                                        IP.QUANTIDADE,
                                        IP.EXCLUIDO,
                                        IP.USUARIO_INCLUSAO,
                                        IP.DATA_INCLUSAO,
                                        PT.NOME,
                                        PT.QUANTIDADE AS QUANTIDADE_PRODUTO
                                        FROM NOVOFRAMEWORK.ITEM_ENTRADA_PRODUTO IP
                                        INNER JOIN NOVOFRAMEWORK.PRODUTO PT ON IP.ID_PRODUTO = PT.ID_PRODUTO
                                   WHERE     
                                        IP.EXCLUIDO = :EXCLUIDO
                                        AND ID_ENTRADA_PRODUTOS = :ID_ENTRADA_PRODUTOS");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_ENTRADA_PRODUTOS", $objItemEntradaProdutoVO->getIdEntradaProdutos()->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
//            echo $objDaoHelper->getSql();exit();
            // Executando comando            
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.

            $arrayIterator = new \ArrayIterator();
            foreach ($objDaoHelper->fetchAll() as $ItemEntradaProduto) {
                $objItemEntradaProdutoVO = new ItemEntradaProdutoVO();
                $objItemEntradaProdutoVO->bind($ItemEntradaProduto);
                $objItemEntradaProdutoVO->getIdProduto()->setQuantidade($ItemEntradaProduto['QUANTIDADE_PRODUTO']);
                $arrayIterator->append($objItemEntradaProdutoVO);
            }
            $objDaoHelper->setRetornoOperacao($arrayIterator);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    public function listar(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        IP.ID_ITEM_ENTRADA_PRODUTO,
                                        IP.ID_ENTRADA_PRODUTOS,
                                        IP.ID_PRODUTO,
                                        IP.QUANTIDADE,
                                        IP.EXCLUIDO
                                    FROM NOVOFRAMEWORK.ITEM_ENTRADA_PRODUTO IP                                    
                                    WHERE IP.EXCLUIDO = :EXCLUIDO
                                    AND ID_ITEM_ENTRADA_PRODUTO = :ID_ITEM_ENTRADA_PRODUTO");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_ITEM_ENTRADA_PRODUTO", $objItemEntradaProdutoVO->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            // Executando comando            
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.

            foreach ($objDaoHelper->fetchAll() as $ItemEntradaProduto) {
                $objItemEntradaProdutoVO = new ItemEntradaProdutoVO();
                $objItemEntradaProdutoVO->bind($ItemEntradaProduto);
            }
            $objDaoHelper->setRetornoOperacao($objItemEntradaProdutoVO);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    public function listarByProduto(ItemEntradaProdutoVO $objItemEntradaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        IP.ID_ITEM_ENTRADA_PRODUTO,
                                        IP.ID_ENTRADA_PRODUTOS,
                                        IP.ID_PRODUTO,
                                        IP.QUANTIDADE,
                                        IP.EXCLUIDO,
                                        IP.USUARIO_INCLUSAO,
                                        IP.DATA_INCLUSAO,
                                        PT.NOME,
                                        PT.QUANTIDADE AS QUANTIDADE_PRODUTO
                                        FROM NOVOFRAMEWORK.ITEM_ENTRADA_PRODUTO IP
                                        INNER JOIN NOVOFRAMEWORK.PRODUTO PT ON IP.ID_PRODUTO = PT.ID_PRODUTO
                                   WHERE     
                                        IP.EXCLUIDO = :EXCLUIDO
                                        AND ID_PRODUTO = :ID_PRODUTO");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_PRODUTO", $objItemEntradaProdutoVO->getIdProduto()->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
//            echo $objDaoHelper->getSql();exit();
            // Executando comando            
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.

            $arrayIterator = new \ArrayIterator();
            foreach ($objDaoHelper->fetchAll() as $ItemEntradaProduto) {
                $objItemEntradaProdutoVO = new ItemEntradaProdutoVO();
                $objItemEntradaProdutoVO->bind($ItemEntradaProduto);
                $objItemEntradaProdutoVO->getIdProduto()->setQuantidade($ItemEntradaProduto['QUANTIDADE_PRODUTO']);
                $arrayIterator->append($objItemEntradaProdutoVO);
            }
            $objDaoHelper->setRetornoOperacao($arrayIterator);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

}
