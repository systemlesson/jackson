<?php

namespace module\almoxarifado\dao;

use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\vo\FuncionarioVO;
use core\helper\FormatHelper;

/**
 * Classe de persistência referente à Unidade
 * @acess public
 * @package almoxarifado
 * @subpackage dao
 */
class FuncionarioDAO extends AbstractDAO {

    /**
     * Método que realiza a listagem de Funcionários cadastrados
     * @param FuncionarioVO $objFuncionarioVO
     * @return ArrayIterator
     * @throws Excepcion em caso de erro de banco de dados
     */
    public function listar(FuncionarioVO $objFuncionarioVO) {
        # Instanciando classe de apoio da camada #
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";

            if (!is_null($objFuncionarioVO)) {
                if (strlen($objFuncionarioVO->getNome()) > 0) {
                    $auxSQL = "AND TRANSLATE(UPPER(UPPER(FC.NOME)), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE UPPER(:FC.NOME)";
                }

                if (strlen($objFuncionarioVO->getMatricula()) > 0) {
                    $auxSQL .= "AND MATRICULA LIKE :MATRICULA";
                }

                if (strlen($objFuncionarioVO->getCpf()) > 0) {
                    $auxSQL .= "AND CPF LIKE :CPF";
                }
            }

            # Comando SQL #
            $objDaoHelper->setSql("SELECT FC.ID_FUNCIONARIO_USUARIO,
                                          FC.ID_SETOR,
                                          FC.MATRICULA,
                                          FC.NOME,
                                          FC.PASSOWORD,
                                          FC.CPF,
                                          FC.TELEFONE,
                                          FC.CELULAR,
                                          FC.TIPO_USUARIO,
                                          SE.NOME AS NOME_SETOR
                                   FROM NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC
                                   INNER JOIN NOVOFRAMEWORK.SETOR SE ON FC.ID_SETOR = SE.ID_SETOR
                                   WHERE FC.EXCLUIDO = :EXCLUIDO
                                   AND (SE.ID_SETOR = :ID_SETOR OR :ID_SETOR IS NULL)
                                   AND (FC.ID_FUNCIONARIO_USUARIO = :ID_FUNCIONARIO_USUARIO OR :ID_FUNCIONARIO_USUARIO IS NULL)
                                   ORDER BY MATRICULA DESC
                                   $auxSQL");

            if (!is_null($objFuncionarioVO)) {

                if (strlen($objFuncionarioVO->getNome()) > 0) {
                    $objDaoHelper->bindValue(":FC.NOME", '%' . FormatHelper::removerAcentos($objFuncionarioVO->getNome()) . '%');
                }

                if (strlen($objFuncionarioVO->getCpf()) > 0) {
                    $objDaoHelper->bindValue(":CPF", FormatHelper::removeMask($objFuncionarioVO->getCpf()) . '%');
                }
            }

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":MATRICULA", $objFuncionarioVO->getMatricula());
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objFuncionarioVO->getId());
            $objDaoHelper->bindValue(":ID_SETOR", $objFuncionarioVO->getIdSetor()->getId());

            //echo $objDaoHelper->getSql();
            # Executando comando #
            $objDaoHelper->execute();

            # Instanciando classes de apoio, construindo resultado #
            $arrayIterator = new \ArrayIterator();
            foreach ($objDaoHelper->fetchAll() as $funcionario) {
                $objFuncionarioVO = new FuncionarioVO();
                $objFuncionarioVO->bind($funcionario);
                $objFuncionarioVO->getIdSetor()->setNome($funcionario['NOME_SETOR']);
                $arrayIterator->append($objFuncionarioVO);
            }

            # Setando retorno em caso de sucesso #
            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que adiciona Unidades
     * @param FuncionarioVO $objFuncionarioVO
     * @return ArrayIterator
     * @throws Excepcion em caso de erro de banco de dados
     */
    public function inserir(FuncionarioVO $objFuncionarioVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Setando ID do grupo #
            $objFuncionarioVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_FUNCIONARIO_USUARIO", $objDaoHelper->getConexao()));

            # Comando SQL #
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.FUNCIONARIO_USUARIO
                               (
                                ID_FUNCIONARIO_USUARIO,
                                ID_SETOR,
                                MATRICULA,
                                NOME,
                                PASSOWORD,
                                CPF,
                                TELEFONE,
                                CELULAR,
                                TIPO_USUARIO,
                                DATA_INCLUSAO,
                                USUARIO_INCLUSAO,
                                EXCLUIDO)
                             VALUES
                             (
                                :ID_FUNCIONARIO_USUARIO,
                                :ID_SETOR,
                                :MATRICULA,
                                :NOME,
                                :PASSOWORD,
                                :CPF,
                                :TELEFONE,
                                :CELULAR,
                                :TIPO_USUARIO,
                                sysdate,
                                :USUARIO_INCLUSAO,
                                :EXCLUIDO)");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objFuncionarioVO->getId());
            $objDaoHelper->bindValue(":ID_SETOR", $objFuncionarioVO->getIdSetor()->getId());
            $objDaoHelper->bindValue(":MATRICULA", $objFuncionarioVO->getMatricula());
            $objDaoHelper->bindValue(":NOME", $objFuncionarioVO->getNome());
            $objDaoHelper->bindValue(":CPF", FormatHelper::removeMask($objFuncionarioVO->getCpf()));
            $objDaoHelper->bindValue(":PASSOWORD", $objFuncionarioVO->getSenha());
            $objDaoHelper->bindValue(":TELEFONE", FormatHelper::removeMask($objFuncionarioVO->getTelefone()));
            $objDaoHelper->bindValue(":CELULAR", FormatHelper::removeMask($objFuncionarioVO->getCelular()));
            $objDaoHelper->bindValue(":TIPO_USUARIO", $objFuncionarioVO->getTipoUsuario());

            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objFuncionarioVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objFuncionarioVO->getUsuarioInclusao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

//            echo $objDaoHelper->getSql(); die;
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que altera as Unidades existentes
     * @param FuncionarioVO $objFuncionarioVO
     * @return mixed
     * @access public
     */
    public function alterar(FuncionarioVO $objFuncionarioVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC
                                    SET     
                                            FC.NOME                   = :NOME,
                                            FC.ID_SETOR               = :ID_SETOR,
                                            FC.MATRICULA              = :MATRICULA,
                                            FC.CPF                    = :CPF,
                                            FC.PASSOWORD              = :PASSOWORD,
                                            FC.TELEFONE               = :TELEFONE,
                                            FC.CELULAR                = :CELULAR,
                                            FC.TIPO_USUARIO           = :TIPO_USUARIO,
                                            FC.DATA_ALTERACAO         = SYSDATE,
                                            FC.USUARIO_ALTERACAO      = :USUARIO_ALTERACAO
                                    WHERE   
                                            FC.ID_FUNCIONARIO_USUARIO = :ID_FUNCIONARIO_USUARIO
                                            AND FC.EXCLUIDO           = :EXCLUIDO ");

            # Atribuiindo valores #
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objFuncionarioVO->getId());
            $objDaoHelper->bindValue(":NOME", $objFuncionarioVO->getNome());
            $objDaoHelper->bindValue(":ID_SETOR", $objFuncionarioVO->getIdSetor()->getId());
            $objDaoHelper->bindValue(":MATRICULA", $objFuncionarioVO->getMatricula());
            $objDaoHelper->bindValue(":CPF", FormatHelper::removeMask($objFuncionarioVO->getCpf()));
            $objDaoHelper->bindValue(":PASSOWORD", $objFuncionarioVO->getSenha());
            $objDaoHelper->bindValue(":TELEFONE", FormatHelper::removeMask($objFuncionarioVO->getTelefone()));
            $objDaoHelper->bindValue(":CELULAR", FormatHelper::removeMask($objFuncionarioVO->getCelular()));
            $objDaoHelper->bindValue(":TIPO_USUARIO", $objFuncionarioVO->getTipoUsuario());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objFuncionarioVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objFuncionarioVO->getUsuarioAlteracao());

//            echo $objDaoHelper->getSql(); exit;
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que exclui os Funcionários existentes
     * @param FuncionarioVO $objFuncionarioVO
     * @return boolean
     * @access public
     */
    public function excluir(FuncionarioVO $objFuncionarioVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC
                                    SET   FC.DATA_ALTERACAO      = :DATA_ALTERACAO,
                                          FC.USUARIO_ALTERACAO   = :USUARIO_ALTERACAO,
                                          FC.EXCLUIDO            = :EXCLUIDO
                                    WHERE ID_FUNCIONARIO_USUARIO = :ID_FUNCIONARIO_USUARIO");

            # Atribuiindo valores #
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objFuncionarioVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objFuncionarioVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 1);
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objFuncionarioVO->getId());

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que seleciona os Funcionários existentes
     * @param $objFuncionarioVO
     * @return boolean
     * @access public
     */
    public function selecionar($objFuncionarioVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT
                                    FC.ID_FUNCIONARIO_USUARIO,
                                    FC.ID_SETOR,
                                    FC.MATRICULA,
                                    FC.NOME,
                                    FC.CPF,
                                    FC.TELEFONE,
                                    FC.CELULAR,
                                    FC.PASSOWORD,
                                    FC.TIPO_USUARIO,
                                    FC.DATA_INCLUSAO,
                                    FC.DATA_ALTERACAO,
                                    FC.USUARIO_INCLUSAO,
                                    FC.USUARIO_ALTERACAO
                                  FROM NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC
                                  WHERE FC.EXCLUIDO             = :EXCLUIDO
                                  AND FC.ID_FUNCIONARIO_USUARIO = :ID_FUNCIONARIO_USUARIO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objFuncionarioVO->getId());
//            echo $objDaoHelper->getSql(); exit;
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->fetchAll() as $funcionario) {
                $objFuncionarioVO = new FuncionarioVO();
                $objFuncionarioVO->bind($funcionario);
            }
            $objDaoHelper->setRetornoOperacao($objFuncionarioVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Verifica a existência de um registro
     * @param FuncionarioVO $objFuncionarioVO, $exceto = FALSE
     * @return boolean
     * @access public
     */
    public function existeMatricula(FuncionarioVO $objFuncionarioVO, $exceto = FALSE) {
        $objDaoHelper = new DaoHelper();
        $sqlAuxiliar = '';

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            if ($objFuncionarioVO->getId() != null) {
                $sqlAuxiliar = ' AND ID_FUNCIONARIO_USUARIO <> :ID_FUNCIONARIO_USUARIO ';
            }

            # Comando SQL #
            $objDaoHelper->setSql("SELECT COUNT(MATRICULA) as TOTAL
                                    FROM NOVOFRAMEWORK.FUNCIONARIO_USUARIO 
                                    WHERE EXCLUIDO = :EXCLUIDO
                                    AND MATRICULA = :MATRICULA
                                    $sqlAuxiliar");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objFuncionarioVO->getId());
            $objDaoHelper->bindValue(":MATRICULA", $objFuncionarioVO->getMatricula());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            if ($objFuncionarioVO->getId() != null) {
                $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objFuncionarioVO->getId());
            }

            # Executando comando #
//            echo $objDaoHelper->getSql(); die;
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE #
            foreach ($objDaoHelper->fetchAll() as $funcionario) {
                if ($funcionario['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
        # Retornando resposta #

        return $objDaoHelper->getRetornoOperacao();
    }

    /**
     * Puxa a tabela de Setor para a combo box da tela de Funcionario
     * @return String
     */
    public function listarCombo() {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo Conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT FC.ID_SETOR,
                                          FC.NOME
                                   FROM NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC
                                   WHERE FC.EXCLUIDO = 0
                                   ORDER BY NOME");

            # Executando comando #
            $objDaoHelper->execute();

            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $funcionario) {
                $objFuncionarioVO = new FuncionarioVO();
                var_dump($funcionario);
                $objFuncionarioVO->bind($funcionario);
                $arrayIterator->append($objFuncionarioVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando Conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function autenticar(FuncionarioVO $objFuncionarioVO) {

        $objDaoHelper = new DaoHelper();

        try {

            // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL

            $objDaoHelper->setSql(" SELECT COUNT(FU.CPF) AS TOTAL 
                                        FROM NOVOFRAMEWORK.FUNCIONARIO_USUARIO FU
                                    WHERE 
                                        FU.CPF = :CPF
                                        AND EXCLUIDO = :EXCLUIDO
                                        AND FU.PASSOWORD = :PASSOWORD");

            // Atribuindo valores
            $objDaoHelper->bindValue(":CPF", FormatHelper::removeMask($objFuncionarioVO->getCpf(), '###.###.###-##'));
            $objDaoHelper->bindValue(":PASSOWORD", $objFuncionarioVO->getSenha());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

//            echo $objDaoHelper->getSql();
            // Executando comando
            $objDaoHelper->execute();

            // Obtendo resposta
            foreach ($objDaoHelper->fetchAll() as $autenticar) {
                if ($autenticar['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogManipulador::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que seleciona os Funcionários existentes
     * @param $objFuncionarioVO
     * @return boolean
     * @access public
     */
    public function selecionarByCpf($objFuncionarioVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT
                                    FC.ID_FUNCIONARIO_USUARIO,
                                    FC.ID_SETOR,
                                    FC.MATRICULA,
                                    FC.NOME,
                                    FC.CPF,
                                    FC.TELEFONE,
                                    FC.CELULAR,
                                    FC.TIPO_USUARIO,
                                    FC.DATA_INCLUSAO,
                                    FC.DATA_ALTERACAO,
                                    FC.USUARIO_INCLUSAO,
                                    FC.USUARIO_ALTERACAO
                                  FROM NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC
                                  WHERE FC.EXCLUIDO             = :EXCLUIDO
                                  AND FC.CPF = :CPF");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":CPF", $objFuncionarioVO->getCpf());
//            echo $objDaoHelper->getSql(); exit;
            # Executando comando #
            $objDaoHelper->execute();
//            var_dump($objDaoHelper->fetchAll()); die;
            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->fetchAll() as $funcionario) {
                $objFuncionarioVO = new FuncionarioVO();
                $objFuncionarioVO->bind($funcionario);
            }
            $objDaoHelper->setRetornoOperacao($objFuncionarioVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

}
