<?php

namespace module\almoxarifado\dao;

use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\bo\CategoriaBO;
use module\almoxarifado\vo\CategoriaVO;
use core\helper\FormatHelper;

/**
 * Classe de persistência referente à Unidade
 * @acess public
 * @package almoxarifado
 * @subpackage dao
 */
class CategoriaDAO extends AbstractDAO {

    /**
     * Método que realiza a listagem de Unidades cadastradas
     * @param UnidadeVO $objCategoriaVO
     * @return ArrayIterator
     * @throws Excepcion em caso de erro de banco de dados
     */
    public function listar(CategoriaVO $objCategoriaVO) {
        # Instanciando classe de apoio da camada #
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";
            
            # Substituição de caracteres #
            if (!is_null($objCategoriaVO)) {
                if (strlen($objCategoriaVO->getNome()) > 0) {
                    $auxSQL = "AND TRANSLATE(UPPER(UPPER(NOME)), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE UPPER(:NOME)";
                }
            }
            
            # Comando SQL #
            $objDaoHelper->setSql("SELECT CT.ID_CATEGORIA,
                                          CT.NOME,
                                          CT.USUARIO_ALTERACAO,
                                          CT.DATA_INCLUSAO
                                   FROM NOVOFRAMEWORK.CATEGORIA CT 
                                   WHERE CT.EXCLUIDO = :EXCLUIDO
                                   $auxSQL");                      
                                    
            if (!is_null($objCategoriaVO)) {
                if (strlen($objCategoriaVO->getNome()) > 0) {
                    $objDaoHelper->bindValue(":NOME", '%' . FormatHelper::removerAcentos($objCategoriaVO->getNome()) . '%');
                }
            } 
            
            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            # Executando comando #
            $objDaoHelper->execute();

            # Instanciando classes de apoio, construindo resultado #
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $categoria) {
                $objCategoriaVO = new CategoriaVO();
                $objCategoriaVO->bind($categoria);
                $arrayIterator->append($objCategoriaVO);
            }

            # Setando retorno em caso de sucesso #
            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resultado #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que adiciona Categoria
     * @param UnidadeVO $objCategoriaVO
     * @return ArrayIterator
     * @throws Excepcion em caso de erro de banco de dados
     */
    public function inserir(CategoriaVO $objCategoriaVO) {
        
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            
            # Setando ID do grupo (precisa?) #
            $objCategoriaVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_CATEGORIA", $objDaoHelper->getConexao()));

            # Comando SQL (checar) #
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.CATEGORIA C
                                  (
                                   C.ID_CATEGORIA,
                                   C.NOME,                                  
                                   C.DATA_INCLUSAO,
                                   C.USUARIO_INCLUSAO,
                                   C.EXCLUIDO )                                    
                                 VALUES 
                                 ( 
                                   :ID_CATEGORIA,
                                   :NOME,                        
                                   sysdate,
                                   :USUARIO_INCLUSAO,
                                   :EXCLUIDO ) 
                                  ");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objCategoriaVO->getId());
            $objDaoHelper->bindValue(":NOME", $objCategoriaVO->getNome());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objCategoriaVO->getUsuarioInclusao());
            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objCategoriaVO->getDataInclusao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            
          //echo($objDaoHelper->getSql());die;
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que altera as Unidades existentes
     * @param UnidadeVO $objUnidadeVO
     * @return mixed
     * @access public
     */
    public function alterar(CategoriaVO $objCategoriaVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL (checar) #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.CATEGORIA
                                    SET    DATA_ALTERACAO    = :DATA_ALTERACAO,                                          
                                           EXCLUIDO          = :EXCLUIDO,                                                                               
                                           NOME              = :NOME,
                                           USUARIO_ALTERACAO = :USUARIO_ALTERACAO
                                    WHERE  ID_CATEGORIA      = :ID_CATEGORIA
                                    ");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objCategoriaVO->getDataAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objCategoriaVO->getId());
            $objDaoHelper->bindValue(":NOME", $objCategoriaVO->getNome());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objCategoriaVO->getUsuarioAlteracao());

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que exclui as Unidades existentes
     * @param UnidadeVO $objUnidadeVO
     * @return boolean
     * @access public
     */
     public function excluir(CategoriaVO $objCategoriaVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.CATEGORIA
                                    SET    DATA_ALTERACAO    = :DATA_ALTERACAO,
                                           EXCLUIDO          = :EXCLUIDO,
                                           USUARIO_ALTERACAO = :USUARIO_ALTERACAO
                                    WHERE  ID_CATEGORIA        = :ID_CATEGORIA");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objCategoriaVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objCategoriaVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objCategoriaVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 1);
            //echo $objDaoHelper->getsql();
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que seleciona as Unidades existentes
     * @param $objUnidadeVO
     * @return boolean
     * @access public
     */
    public function selecionar($objCategoriaVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT 
                                       ID_CATEGORIA,
                                       NOME,                                       
                                       DATA_INCLUSAO,
                                       DATA_ALTERACAO,
                                       USUARIO_INCLUSAO,
                                       USUARIO_ALTERACAO
                                    FROM NOVOFRAMEWORK.CATEGORIA
                                    WHERE EXCLUIDO = :EXCLUIDO 
                                    AND ID_CATEGORIA = :ID_CATEGORIA");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":EXCLUIDO", 0); # ATIVO 0 -- INATIVO 1 #
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objCategoriaVO->getId());

            # Executando comando #
            $objDaoHelper->execute();
            

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->fetchAll() as $recurso) { //ver esse recurso
                $objCategoriaVO = new CategoriaVO;
                $objCategoriaVO->bind($recurso);
            }
            $objDaoHelper->setRetornoOperacao($objCategoriaVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Verifica a existência de um registro
     * @param CategoriaVO $objCategoriaVO
     * @return boolean
     * @access public
     */
    public function existe($objCategoriaVO) {
        $objDaoHelper = new DaoHelper();
//        $sqlAuxiliar = '';

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT  COUNT(NOME) as TOTAL
                                   FROM NOVOFRAMEWORK.CATEGORIA
                                   WHERE EXCLUIDO = :EXCLUIDO                                   
                                   AND (ID_CATEGORIA != :ID_CATEGORIA or :ID_CATEGORIA = NULL
                                   AND TRANSLATE(UPPER(C.NOME), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE :NOME)
                                    ");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":EXCLUIDO",0); # ATIVO 0 -- INATIVO 1 #
            $objDaoHelper->bindValue(":NOME", $objCategoriaVO->getNome());
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objCategoriaVO->getId());

            # Executando comando #
            $objDaoHelper->execute();


            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->fetchAll() as $categoria) {
                if ($categoria['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetornoOperacao();
    }

            
        public function verificaCategoria (CategoriaVO $objCategoriaVO, $exceto = FALSE) {
        $objDaoHelper = new DaoHelper();
        $sqlAuxiliar = '';

        # Obtendo conexão #
        try {
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            if ($objCategoriaVO->getId() != null) {
                $sqlAuxiliar = ' AND C.ID_CATEGORIA <> :ID_CATEGORIA ';
            }

            $objDaoHelper->setSql("SELECT COUNT(NOME) as TOTAL
                                       FROM NOVOFRAMEWORK.CATEGORIA C
                                       WHERE C.EXCLUIDO = :EXCLUIDO
                                       AND TRANSLATE(UPPER(C.NOME), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE :NOME
                                       $sqlAuxiliar");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":NOME", strtoupper(FormatHelper::removerAcentos($objCategoriaVO->getNome())));
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            if ($objCategoriaVO->getId() != null) {
                $objDaoHelper->bindValue(":ID_CATEGORIA", $objCategoriaVO->getId());
            }

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE #
            foreach ($objDaoHelper->fetchAll() as $categoria) {
                if ($categoria['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                } else {
                    $objDaoHelper->setRetornoOperacao(FALSE);
                }
            }

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetornoOperacao();
    }
     
     public function listarCategoria() {
        $objDaoHelper = new DaoHelper();
        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $objDaoHelper->setSql("SELECT CT.ID_CATEGORIA,
                                          CT.NOME,
                                          CT.USUARIO_ALTERACAO,
                                          CT.DATA_INCLUSAO,
                                          CT.EXCLUIDO,
                                          CT.USUARIO_INCLUSAO,
                                          CT.DATA_ALTERACAO
                                    FROM 
                                          NOVOFRAMEWORK.CATEGORIA CT
                                    WHERE 
                                          EXCLUIDO = :EXCLUIDO");
            
            
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

             
            $objDaoHelper->execute();
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $categoria) {
                $objCategoriaVO = new CategoriaVO();
                $objCategoriaVO->bind($categoria);
                $arrayIterator->append($objCategoriaVO);
            }
//            var_dump($arrayIterator);die;
            $objDaoHelper->setRetornoOperacao($arrayIterator);
            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }
    
    
    
}
