<?php

namespace module\almoxarifado\dao;

use core\exception\AppException;
use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\bo\ItemSolicitacaoProdutoBO;
use module\almoxarifado\vo\ItemSolicitacaoProdutoVO;
use module\almoxarifado\vo\SolicitacaoVO;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;
use core\helper\FormatHelper;

# Classe de persistência ItemSolicitacaoProduto #

class ItemSolicitacaoProdutoDAO extends AbstractDAO {

    /**
     * @param ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO
     * @return boolean
     * @access public
     */
    public function inserir(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Setando ID do grupo #
            $objItemSolicitacaoProdutoVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_ITEM_SOLICITACAO_PRODUTO", $objDaoHelper->getConexao()));
            //
            # Comando SQL #
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.ITEM_SOLICITACAO_PRODUTO ISP
                                    (ISP.ID_ITEM_SOLICITACAO_PRODUTO,
                                     ISP.ID_SOLICITACAO_PRODUTO,
                                     ISP.ID_PRODUTO,
                                     ISP.QUANTIDADE,
                                     ISP.DATA_INCLUSAO,
                                     ISP.USUARIO_INCLUSAO,
                                     ISP.EXCLUIDO)
                                   VALUES
                                    (:ID_ITEM_SOLICITACAO_PRODUTO,
                                     :ID_SOLICITACAO_PRODUTO,
                                     :ID_PRODUTO,
                                     :QUANTIDADE,
                                     sysdate,
                                     :USUARIO_INCLUSAO,
                                     :EXCLUIDO)");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_ITEM_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getIdSolicitacao()->getId());
            $objDaoHelper->bindValue(":ID_PRODUTO", $objItemSolicitacaoProdutoVO->getIdProduto()->getId());
            $objDaoHelper->bindValue(":QUANTIDADE", $objItemSolicitacaoProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objItemSolicitacaoProdutoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objItemSolicitacaoProdutoVO->getUsuarioInclusao());
//            
//            echo ($objDaoHelper->getSql()); exit;
            # Executando comando e enviando para o bando de dados #
            $objDaoHelper->execute(false);


            $objProdutoVO = new ProdutoVO;
            $objProdutoBO = new ProdutoBO;

            /* @var $objItemEntradaProdutoVO ItemEntradaProdutoVO */
            $objProdutoVO->setId($objItemSolicitacaoProdutoVO->getIdProduto()->getId());
            $objProdutoVO->setQuantidade($objItemSolicitacaoProdutoVO->getQuantidade());
            $objProdutoBO->retirarProdutoEstoque($objProdutoVO);
            
//            var_dump($objItemSolicitacaoProdutoVO);die;
            
           
            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function selecionar(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT ISP.ID_ITEM_SOLICITACAO_PRODUTO,
                                          ISP.ID_SOLICITACAO_PRODUTO,
                                          ISP.ID_PRODUTO,
                                          ISP.QUANTIDADE,
                                          ISP.DATA_INCLUSAO,
                                          ISP.USUARIO_INCLUSAO,
                                          ISP.EXCLUIDO
                                   FROM NOVOFRAMEWORK.ITEM_SOLICITACAO_PRODUTO ISP
                                   WHERE ISP.EXCLUIDO = :EXCLUIDO
                                   AND ID_ITEM_SOLICITACAO_PRODUTO = :ID_ITEM_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":ID_ITEM_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getId());

//            echo $objDaoHelper->getSql(); exit;
//            
            # Executando comando #
            $objDaoHelper->execute();

            foreach ($objDaoHelper->fetchAll() as $itemSolicitacao) {
                $objItemSolicitacaoProdutoVO->bind($itemSolicitacao);
            }

            # Setando Retorno #
            $objDaoHelper->setRetornoOperacao($objItemSolicitacaoProdutoVO);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Rertornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function alterar(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));


            $objProdutoBO = new ProdutoBO;
            $objProdutoVO = new ProdutoVO;

            $objItemSolicitacaoProdutoBO = new ItemSolicitacaoProdutoBO();
            $objItemSolicitacaoProdutoAuxVO = $objItemSolicitacaoProdutoBO->listar($objItemSolicitacaoProdutoVO)['retornoOperacao'];

            $quantidadeAnterior = $objItemSolicitacaoProdutoAuxVO->getQuantidade();
            $novaQuantidade = $objItemSolicitacaoProdutoVO->getQuantidade();
            $qtdSolicitada = $quantidadeAnterior - $novaQuantidade;
//                var_dump($qtdSolicitada); die;

            $objProdutoVO->setId($objItemSolicitacaoProdutoVO->getIdProduto()->getId());

            if ($qtdSolicitada > 0) {
//                die('aaa');
                $objProdutoVO->setQuantidade($qtdSolicitada);
                $objProdutoBO->adicionarProdutoEstoque($objProdutoVO);
            } else if ($qtdSolicitada < 0) {
//                die('bbb');
                $objProdutoVO->setQuantidade($qtdSolicitada * -1);
                $objProdutoBO->retirarProdutoEstoque($objProdutoVO);
            }
            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.ITEM_SOLICITACAO_PRODUTO
                                   SET   QUANTIDADE                  = :QUANTIDADE,
                                         DATA_ALTERACAO              = SYSDATE,
                                         USUARIO_ALTERACAO           = :USUARIO_ALTERACAO,
                                         EXCLUIDO                    = :EXCLUIDO
                                   WHERE ID_ITEM_SOLICITACAO_PRODUTO = :ID_ITEM_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_ITEM_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getId());
            $objDaoHelper->bindValue(":QUANTIDADE", $objItemSolicitacaoProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objItemSolicitacaoProdutoVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objItemSolicitacaoProdutoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", $objItemSolicitacaoProdutoVO->getExcluido());

//            echo $objDaoHelper->getSql();die;
            # Executando comando #
            $objDaoHelper->execute(TRUE);


            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function listar(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            $objDaoHelper->setSql("SELECT ISP.ID_ITEM_SOLICITACAO_PRODUTO,
                                          ISP.ID_SOLICITACAO_PRODUTO,
                                          ISP.ID_PRODUTO,
                                          ISP.QUANTIDADE,
                                          ISP.DATA_INCLUSAO,
                                          ISP.USUARIO_INCLUSAO,
                                          ISP.EXCLUIDO
                                   FROM NOVOFRAMEWORK.ITEM_SOLICITACAO_PRODUTO ISP
                                   WHERE ISP.EXCLUIDO = :EXCLUIDO
                                   AND ID_ITEM_SOLICITACAO_PRODUTO = :ID_ITEM_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_ITEM_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_PRODUTO", $objItemSolicitacaoProdutoVO->getIdProduto()->getId());
            $objDaoHelper->bindValue(":QUANTIDADE", $objItemSolicitacaoProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

//            echo $objDaoHelper->getSql(); exit;
//            
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso #
            foreach ($objDaoHelper->fetchAll() as $itemSolicitacao) {
                $objItemSolicitacaoProdutoVO = new ItemSolicitacaoProdutoVO();
                $objItemSolicitacaoProdutoVO->bind($itemSolicitacao);
//                $arrayIterator->append($objItemSolicitacaoProdutoVO);
            }
            $objDaoHelper->setRetornoOperacao($objItemSolicitacaoProdutoVO);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function listarPorSolicitacao(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            $objDaoHelper->setSql("SELECT ISP.ID_ITEM_SOLICITACAO_PRODUTO,
                                          ISP.ID_SOLICITACAO_PRODUTO,
                                          ISP.ID_PRODUTO,
                                          PD.NOME AS NOME_PRODUTO,
                                          ISP.QUANTIDADE,
                                          ISP.DATA_INCLUSAO,
                                          ISP.USUARIO_INCLUSAO,
                                          ISP.EXCLUIDO
                                   FROM NOVOFRAMEWORK.ITEM_SOLICITACAO_PRODUTO ISP
                                   INNER JOIN NOVOFRAMEWORK.PRODUTO PD ON PD.ID_PRODUTO = ISP.ID_PRODUTO
                                   WHERE ISP.EXCLUIDO = :EXCLUIDO
                                   AND ID_SOLICITACAO_PRODUTO = :ID_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_ITEM_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_PRODUTO", $objItemSolicitacaoProdutoVO->getIdProduto()->getId());
            $objDaoHelper->bindValue(":NOME", $objItemSolicitacaoProdutoVO->getIdProduto()->getNome());
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getIdSolicitacao()->getId());
            $objDaoHelper->bindValue(":QUANTIDADE", $objItemSolicitacaoProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

//            echo $objDaoHelper->getSql(); exit;
//            
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso #
            $arrayIterator = new \ArrayIterator();
            foreach ($objDaoHelper->fetchAll() as $itemSolicitacao) {
                $objItemSolicitacaoProdutoVO = new ItemSolicitacaoProdutoVO();
                $objItemSolicitacaoProdutoVO->bind($itemSolicitacao);
                $objItemSolicitacaoProdutoVO->getIdProduto()->setNome($itemSolicitacao['NOME_PRODUTO']);
                $arrayIterator->append($objItemSolicitacaoProdutoVO);
            }
            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function listarPorSaida(ItemSolicitacaoProdutoVO $objItemSolicitacaoProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            $objDaoHelper->setSql("SELECT ISP.ID_ITEM_SOLICITACAO_PRODUTO,
                                          SL.ID_SOLICITACAO_PRODUTO,
                                          SP.ID_SAIDA_PRODUTO,
                                          ISP.QUANTIDADE,
                                          ISP.EXCLUIDO,
                                          isp.id_produto
                                   FROM NOVOFRAMEWORK.ITEM_SOLICITACAO_PRODUTO ISP
                                   INNER JOIN NOVOFRAMEWORK.SOLICITACAO_PRODUTO SL ON ISP.ID_SOLICITACAO_PRODUTO = SL.ID_SOLICITACAO_PRODUTO
                                   INNER JOIN NOVOFRAMEWORK.SAIDA_PRODUTO SP ON SP.ID_SOLICITACAO_PRODUTO = SL.ID_SOLICITACAO_PRODUTO
                                   WHERE ISP.EXCLUIDO = :EXCLUIDO
                                   AND SP.ID_SAIDA_PRODUTO = :ID_SAIDA_PRODUTO");

            //var_dump($objItemSolicitacaoProdutoVO);die();
            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_ITEM_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objItemSolicitacaoProdutoVO->getIdSolicitacao()->getId());
            $objDaoHelper->bindValue(":ID_SAIDA_PRODUTO", $objItemSolicitacaoProdutoVO->getIdDevolucao()->getIdSaidaProduto()->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

//            echo $objDaoHelper->getSql(); exit;
//            
            # Executando comando #
            $objDaoHelper->execute(false);

            # Setando retorno em caso de sucesso #
            $arrayIterator = new \ArrayIterator();
            foreach ($objDaoHelper->fetchAll() as $itemSolicitacao) {

                $objItemSolicitacaoProdutoVO = new ItemSolicitacaoProdutoVO();
                $objItemSolicitacaoProdutoVO->bind($itemSolicitacao);
//                $objItemSolicitacaoProdutoVO->getIdProduto()->setNome($itemSolicitacao['NOME_PRODUTO']);
                $arrayIterator->append($objItemSolicitacaoProdutoVO);
            }
            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

}
