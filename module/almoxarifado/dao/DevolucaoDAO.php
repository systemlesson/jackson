<?php

namespace module\almoxarifado\dao;

use core\exception\AppException;
use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use core\helper\FormatHelper;
use module\almoxarifado\vo\DevolucaoVO;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;
use module\almoxarifado\vo\ItemSolicitacaoProdutoVO;
use module\almoxarifado\bo\ItemSolicitacaoProdutoBO;

# Classe de persistência Devolução #

class DevolucaoDAO extends AbstractDAO {

    /**
     * @param void
     * @return ArrayIterator
     * @access public
     */
    public function listar(DevolucaoVO $objDevolucaoVO) {
        $objDaoHelper = new DaoHelper();

        # Obtendo conexão #
        try {
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));


            # Comando SQL #
            $objDaoHelper->setSql("SELECT DE.ID_DEVOLUCAO_PRODUTO,
                                          SP.ID_SAIDA_PRODUTO,
                                          SP.ID_SOLICITACAO_PRODUTO,
                                          FP.NOME,
                                          DE.USUARIO_CADASTRO,
                                          DE.OBSERVACAO,
                                          DE.DATA_INCLUSAO
                                   FROM NOVOFRAMEWORK.DEVOLUCAO_PRODUTO DE
                                   INNER JOIN NOVOFRAMEWORK.SAIDA_PRODUTO SP ON DE.ID_SAIDA_PRODUTO = SP.ID_SAIDA_PRODUTO
                                   INNER JOIN NOVOFRAMEWORK.SOLICITACAO_PRODUTO SOL ON SP.ID_SOLICITACAO_PRODUTO = SOL.ID_SOLICITACAO_PRODUTO
                                   INNER JOIN NOVOFRAMEWORK.FUNCIONARIO_USUARIO FP ON SOL.ID_FUNCIONARIO_USUARIO = FP.ID_FUNCIONARIO_USUARIO
                                   WHERE DE.EXCLUIDO = :EXCLUIDO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            $objDaoHelper->execute();

            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $devolucao) {
                $objDevolucaoVO = new DevolucaoVO();
                $objDevolucaoVO->bind($devolucao);
                $objDevolucaoVO->getIdFuncionarioUsuario()->setNome($devolucao['NOME']);
                $arrayIterator->append($objDevolucaoVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new AppException($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function visualizar(DevolucaoVO $objDevolucaoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT DE.ID_DEVOLUCAO_PRODUTO,
                                          SP.ID_SAIDA_PRODUTO,
                                          DE.USUARIO_CADASTRO,
                                          DE.OBSERVACAO,
                                          DE.DATA_INCLUSAO
                                   FROM NOVOFRAMEWORK.DEVOLUCAO_PRODUTO DE
                                   INNER JOIN NOVOFRAMEWORK.SAIDA_PRODUTO SP ON DE.ID_SAIDA_PRODUTO = SP.ID_SAIDA_PRODUTO
                                   WHERE DE.EXCLUIDO = :EXCLUIDO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":ID_SOLICITACAO", $objDevolucaoVO->getId());

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso #       
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $devolucao) {
                $objDevolucaoVO = new DevolucaoVO();
                $objDevolucaoVO->bind($devolucao);
                $arrayIterator->append($objDevolucaoVO);
//                $objDevolucaoVO->getIdFuncionarioUsuario()->setId($devolucao['ID_SAIDA_PRODUTO']);
            }
            $objDaoHelper->setRetornoOperacao($objDevolucaoVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function selecionar(DevolucaoVO $objDevolucaoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Coamando SQL #
            $objDaoHelper->setSql("SELECT DE.ID_DEVOLUCAO_PRODUTO,
                                          DE.ID_SAIDA_PRODUTO,
                                          SOL.ID_SOLICITACAO_PRODUTO,
                                          FP.NOME,
                                          DE.OBSERVACAO,
                                          DE.USUARIO_CADASTRO,
                                          DE.DATA_INCLUSAO,
                                          DE.EXCLUIDO
                                   FROM NOVOFRAMEWORK.DEVOLUCAO_PRODUTO DE
                                   INNER JOIN NOVOFRAMEWORK.SAIDA_PRODUTO SP ON DE.ID_SAIDA_PRODUTO = SP.ID_SAIDA_PRODUTO
                                   INNER JOIN NOVOFRAMEWORK.SOLICITACAO_PRODUTO SOL ON SP.ID_SOLICITACAO_PRODUTO = SOL.ID_SOLICITACAO_PRODUTO
                                   INNER JOIN NOVOFRAMEWORK.FUNCIONARIO_USUARIO FP ON SOL.ID_FUNCIONARIO_USUARIO = FP.ID_FUNCIONARIO_USUARIO
                                   WHERE ID_DEVOLUCAO_PRODUTO = :ID_DEVOLUCAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_DEVOLUCAO_PRODUTO", $objDevolucaoVO->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);


//            echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso #       
            $objDevolucaoVO = new DevolucaoVO();
            foreach ($objDaoHelper->fetchAll() as $devolucao) {
                $objDevolucaoVO->bind($devolucao);
                $objDevolucaoVO->getIdSaidaProduto()->getIdSolicitacaoProduto()->setId($devolucao['ID_SOLICITACAO_PRODUTO']);
                $objDevolucaoVO->getIdFuncionarioUsuario()->setNome($devolucao['NOME']);
            }
            $objDaoHelper->setRetornoOperacao($objDevolucaoVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function devolver(DevolucaoVO $objDevolucaoVO) {
        $objDaoHelper = new DaoHelper();
        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            $objDevolucaoVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_DEVOLUCAO_PRODUTO", $objDaoHelper->getConexao()));


            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.DEVOLUCAO_PRODUTO DE
                                   (
                                    DE.ID_DEVOLUCAO_PRODUTO,
                                    DE.ID_SAIDA_PRODUTO,
                                    DE.USUARIO_CADASTRO,
                                    DE.OBSERVACAO,
                                    DE.DATA_INCLUSAO,
                                    DE.USUARIO_INCLUSAO,
                                    DE.EXCLUIDO)
                                   VALUES
                                   (
                                    :ID_DEVOLUCAO_PRODUTO,
                                    :ID_SAIDA_PRODUTO,
                                    :USUARIO_CADASTRO,
                                    :OBSERVACAO,
                                    sysdate,
                                    :USUARIO_INCLUSAO,
                                    :EXCLUIDO)");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_DEVOLUCAO_PRODUTO", $objDevolucaoVO->getId());
            $objDaoHelper->bindValue(":ID_SAIDA_PRODUTO", $objDevolucaoVO->getIdSaidaProduto()->getId());
            $objDaoHelper->bindValue(":USUARIO_CADASTRO", $objDevolucaoVO->getUsuarioInclusao());
            $objDaoHelper->bindValue(":OBSERVACAO", $objDevolucaoVO->getObservacao());

            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objDevolucaoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objDevolucaoVO->getUsuarioInclusao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

//            echo $objDaoHelper->getSql();die;

            $objDaoHelper->execute(false);

            $objProdutoBO = new ProdutoBO();
            $objProdutoVO = new ProdutoVO();
            $objItemSolicitacaoProdutoBO = new ItemSolicitacaoProdutoBO();
            $objItemSolicitacaoProdutoVO = new ItemSolicitacaoProdutoVO();
            
            $objItemSolicitacaoProdutoVO->getIdSolicitacao()->setId($objDevolucaoVO->getIdSaidaProduto()->getIdSolicitacaoProduto()->getId());
            $objItemSolicitacaoProdutoVO->setIdDevolucao($objDevolucaoVO);
            $arrayItItemSolicitacao = $objItemSolicitacaoProdutoBO->listarPorSaida($objItemSolicitacaoProdutoVO)['retornoOperacao'];
            
            $objItemSolicitacaoProdutoVO = NULL;
            
            foreach ($arrayItItemSolicitacao as $objItemSolicitacaoProdutoVO) {
                $objProdutoVO->setId($objItemSolicitacaoProdutoVO->getIdProduto()->getId());
                $objProdutoVO->setQuantidade($objItemSolicitacaoProdutoVO->getQuantidade());
                $retornoAdicionaProduto = $objProdutoBO->adicionarProdutoEstoque($objProdutoVO);
            }

            $objDaoHelper->commit();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao($retornoAdicionaProduto);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

}
