<?php

namespace module\almoxarifado\dao;

use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\bo\UnidadeBO;
use module\almoxarifado\vo\UnidadeVO;
use core\helper\FormatHelper;

/**
 * Classe de persistência referente à Unidade
 * @acess public
 * @package almoxarifado
 * @subpackage dao
 */
class UnidadeDAO extends AbstractDAO {

    /**
     * Método que realiza a listagem de Unidades cadastradas
     * @param UnidadeVO $objUnidadeVO
     * @return ArrayIterator
     * @throws Excepcion em caso de erro de banco de dados
     */
    public function listar(UnidadeVO $objUnidadeVO) {
        # Instanciando classe de apoio da camada #
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";
            
            if (!is_null($objUnidadeVO)) {
                if (strlen($objUnidadeVO->getNome()) > 0 ) {
                    $auxSQL = "AND TRANSLATE(UPPER(UPPER(NOME)), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE UPPER(:NOME)";
                }
            }
  
            # Comando SQL #
            $objDaoHelper->setSql("SELECT UN.ID_UNIDADE,
                                          UN.NOME, 
                                          UN.USUARIO_INCLUSAO,
                                          UN.DATA_INCLUSAO
                                   FROM NOVOFRAMEWORK.UNIDADE UN
                                   WHERE UN.EXCLUIDO = :EXCLUIDO
                                   $auxSQL");
            
            if (!is_null($objUnidadeVO)) {
                if (strlen($objUnidadeVO->getNome()) > 0 ) {
                    $objDaoHelper->bindValue(":NOME", '%'. FormatHelper::removerAcentos($objUnidadeVO->getNome()) . '%');
                }
            }

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            # Executando comando #
            $objDaoHelper->execute();

            # Instanciando classes de apoio, construindo resultado #
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $unidade) {
                $objUnidadeVO = new UnidadeVO();
                $objUnidadeVO->bind($unidade);
                $arrayIterator->append($objUnidadeVO);
            }

            # Setando retorno em caso de sucesso #
            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resultado #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que adiciona Unidades
     * @param UnidadeVO $objUnidadeVO
     * @return ArrayIterator
     * @throws Excepcion em caso de erro de banco de dados
     */
    public function inserir(UnidadeVO $objUnidadeVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Setando ID do grupo #
            $objUnidadeVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_UNIDADE", $objDaoHelper->getConexao()));

            # Comando SQL (checar) #
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.UNIDADE
                                  (
                                   ID_UNIDADE,
                                   NOME,                                  
                                   DATA_INCLUSAO,
                                   USUARIO_INCLUSAO,
                                   EXCLUIDO)                                    
                                 VALUES 
                                 ( 
                                   :ID_UNIDADE,
                                   :NOME,                        
                                   sysdate,
                                   :USUARIO_INCLUSAO,
                                   :EXCLUIDO) 
                                  ");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":ID_UNIDADE", $objUnidadeVO->getId());
            $objDaoHelper->bindValue(":NOME", $objUnidadeVO->getNome());
            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objUnidadeVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objUnidadeVO->getUsuarioInclusao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0) /* ATIVO 0 -- INATIVO 1 */;

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que altera as Unidades existentes
     * @param UnidadeVO $objUnidadeVO
     * @return mixed
     * @access public
     */
    public function alterar(UnidadeVO $objUnidadeVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL (checar) #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.UNIDADE
                                    SET    DATA_ALTERACAO    = :DATA_ALTERACAO,                                          
                                           EXCLUIDO          = :EXCLUIDO,                                                                               
                                           USUARIO_ALTERACAO = :USUARIO_ALTERACAO,
                                           NOME              = :NOME
                                    WHERE  ID_UNIDADE        = :ID_UNIDADE
                                    ");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            $objDaoHelper->bindValue(":NOME", $objUnidadeVO->getNome());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objUnidadeVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objUnidadeVO->getDataAlteracao());
            $objDaoHelper->bindValue(":ID_UNIDADE", $objUnidadeVO->getId());

#           var_dump($objDaoHelper); die;
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que exclui as Unidades existentes
     * @param UnidadeVO $objUnidadeVO
     * @return boolean
     * @access public
     */
    public function excluir(UnidadeVO $objUnidadeVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.UNIDADE SE
                                    SET    SE.DATA_ALTERACAO    = :DATA_ALTERACAO,
                                           SE.EXCLUIDO          = :EXCLUIDO,
                                           SE.USUARIO_ALTERACAO = :USUARIO_ALTERACAO
                                    WHERE  SE.ID_UNIDADE        = :ID_UNIDADE");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":ID_UNIDADE", $objUnidadeVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objUnidadeVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objUnidadeVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 1);
            
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que seleciona as Unidades existentes
     * @param $objUnidadeVO
     * @return boolean
     * @access public
     */
    public function selecionar($objUnidadeVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT 
                                       UN.ID_UNIDADE,
                                       UN.NOME,                                       
                                       UN.DATA_INCLUSAO,
                                       UN.DATA_ALTERACAO,
                                       UN.USUARIO_INCLUSAO,
                                       UN.USUARIO_ALTERACAO
                                    FROM NOVOFRAMEWORK.UNIDADE UN
                                    WHERE UN.EXCLUIDO = :EXCLUIDO
                                    AND UN.ID_UNIDADE = :ID_UNIDADE");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":EXCLUIDO", 0); # ATIVO 0 -- INATIVO 1 #
            $objDaoHelper->bindValue(":ID_UNIDADE", $objUnidadeVO->getId());

//            var_dump($objUnidadeVO->getId()); exit;
            //echo($objDaoHelper->getSql());die;
            
            # Executando comando #
            $objDaoHelper->execute();

//            $objUnidadeVO = false;

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->fetchAll() as $recurso) {
                $objUnidadeVO = new UnidadeVO();
                $objUnidadeVO->bind($recurso);
            }
            $objDaoHelper->setRetornoOperacao($objUnidadeVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Verifica a existência de um registro (não utilizado)
     * @param UnidadeVO $objUnidadeVO
     * @return boolean
     * @access public
     */
    public function existe($objUnidadeVO) {
        $objDaoHelper = new DaoHelper();
//        $sqlAuxiliar = '';

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT  COUNT(NOME) as TOTAL
                                   FROM NOVOFRAMEWORK.UNIDADE
                                   WHERE EXCLUIDO = :EXCLUIDO                                   
                                   AND (ID_UNIDADE != :ID_UNIDADE or :ID_UNIDADE = NULL
                                   AND TRANSLATE(UPPER(UN.NOME), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE :NOME)
                                    ");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":EXCLUIDO", 0); # ATIVO 0 -- INATIVO 1 #
            $objDaoHelper->bindValue(":NOME", $objUnidadeVO->getNome());
            $objDaoHelper->bindValue(":ID_UNIDADE", $objUnidadeVO->getId());

            # Executando comando #
            $objDaoHelper->execute();


            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->fetchAll() as $unidade) {
                if ($unidade['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetornoOperacao();
    }

    public function listarUnidade() {
        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $objDaoHelper->setSql("SELECT UN.ID_UNIDADE, UN.NOME FROM NOVOFRAMEWORK.UNIDADE UN WHERE UN.EXCLUIDO = 0 ORDER BY NOME");

            $objDaoHelper->execute();
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $unidade) {
                $objUnidadeVO = new UnidadeVO();
                $objUnidadeVO->bind($unidade);
                $arrayIterator->append($objUnidadeVO);
            }
//            var_dump($arrayIterator);die;
            $objDaoHelper->setRetornoOperacao($arrayIterator);
            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * Verifica a existência de um registro
     * @param UnidadeVO $objUnidadeVO
     * @return boolean
     * @access public
     */
    public function verificaUnidade(UnidadeVO $objUnidadeVO, $exceto = FALSE) {
        $objDaoHelper = new DaoHelper();
        $sqlAuxiliar = '';

        # Obtendo conexão #
        try {
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            if ($objUnidadeVO->getId() != null) {
                $sqlAuxiliar = ' AND UN.ID_UNIDADE <> :ID_UNIDADE ';
            }

            $objDaoHelper->setSql("SELECT COUNT(NOME) as TOTAL
                                       FROM NOVOFRAMEWORK.UNIDADE UN
                                       WHERE UN.EXCLUIDO = :EXCLUIDO
                                       AND TRANSLATE(UPPER(UN.NOME), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE :NOME
                                       $sqlAuxiliar");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":NOME", strtoupper(FormatHelper::removerAcentos($objUnidadeVO->getNome())));
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            if ($objUnidadeVO->getId() != null) {
                $objDaoHelper->bindValue(":ID_UNIDADE", $objUnidadeVO->getId());
            }

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE #
            foreach ($objDaoHelper->fetchAll() as $unidade) {
                if ($unidade['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetornoOperacao();
    }

}
