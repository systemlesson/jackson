<?php

namespace module\almoxarifado\dao;

use core\exception\AppException;
use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use core\helper\FormatHelper;
use module\almoxarifado\vo\SolicitacaoVO;
use module\almoxarifado\bo\ItemSolicitacaoProdutoBO;
use module\almoxarifado\vo\ItemSolicitacaoProdutoVO;
use module\almoxarifado\bo\SaidaProdutoBO;
use module\almoxarifado\vo\SaidaProdutoVO;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;

# Classe de persistência Solicitação #

class SolicitacaoDAO extends AbstractDAO {

    /**
     * @param void
     * @return ArrayIterator
     * @access public
     */
    public function listar(SolicitacaoVO $objSolicitacaoVO) {
        $objDaoHelper = new DaoHelper();

        # Obtendo conexão #
        try {
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));


            # Comando SQL #
            $objDaoHelper->setSql("SELECT SC.ID_SOLICITACAO_PRODUTO,
                                          FC.ID_FUNCIONARIO_USUARIO,
                                          FC.NOME AS SOLICITANTE,
                                          SC.DATA_INCLUSAO,
                                          SC.SITUACAO,
                                          SC.OBSERVACAO
                                   FROM NOVOFRAMEWORK.SOLICITACAO_PRODUTO SC
                                   INNER JOIN NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC ON SC.ID_FUNCIONARIO_USUARIO = FC.ID_FUNCIONARIO_USUARIO
                                   WHERE SC.EXCLUIDO = :EXCLUIDO
                                   ORDER BY DATA_INCLUSAO  DESC ");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objSolicitacaoVO->getIdFuncionarioUsuario()->getId());

//            echo $objDaoHelper->getSql(); exit;

            if (!is_null($objSolicitacaoVO)) {
                if (strlen($objSolicitacaoVO->getSituacao()) > 0) {
                    $objDaoHelper->bindValue(":SITUACAO", '%' . FormatHelper::removerAcentos($objSolicitacaoVO->getSituacao()) . '%');
                }
            }

//            var_dump($objSolicitacaoVO->getIdFuncionarioUsuario()); exit;

            $objDaoHelper->execute();

            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $solicitacao) {
                $objSolicitacaoVO = new SolicitacaoVO();
                $objSolicitacaoVO->bind($solicitacao);
                $objSolicitacaoVO->getIdFuncionarioUsuario()->setNome($solicitacao['SOLICITANTE']);
                $arrayIterator->append($objSolicitacaoVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new AppException($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return boolean
     * @access public
     */
    public function inserir(SolicitacaoVO $objSolicitacaoVO) {
        $objDaoHelper = new DaoHelper();


        # Obtendo conexão #
        try {
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Setando ID do grupo #
            $objSolicitacaoVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_SOLICITACAO_PRODUTO", $objDaoHelper->getConexao()));

            # Comando SQL #
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.SOLICITACAO_PRODUTO SC
                                    (
                                     SC.ID_SOLICITACAO_PRODUTO,
                                     SC.ID_FUNCIONARIO_USUARIO,
                                     SC.SITUACAO,
                                     SC.OBSERVACAO,
                                     SC.DATA_INCLUSAO,
                                     SC.USUARIO_INCLUSAO,
                                     EXCLUIDO )     
                                   VALUES
                                    (
                                     :ID_SOLICITACAO_PRODUTO,
                                     :ID_FUNCIONARIO_USUARIO,
                                     :SITUACAO,
                                     :OBSERVACAO,
                                     sysdate,
                                     :USUARIO_INCLUSAO,
                                     :EXCLUIDO) ");

            # AtrIbuindo valores #
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objSolicitacaoVO->getId());
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objSolicitacaoVO->getIdFuncionarioUsuario()->getId());
            $objDaoHelper->bindValue(":SITUACAO", $objSolicitacaoVO->getSituacao());
            $objDaoHelper->bindValue(":OBSERVACAO", $objSolicitacaoVO->getObservacao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objSolicitacaoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objSolicitacaoVO->getUsuarioInclusao());

//            echo $objDaoHelper->getSql(); exit;
            # Executando comando (FALSE para não enviar para o banco de dados) #
            $objDaoHelper->execute(false);

            foreach ($objSolicitacaoVO->getItemSolicitacaoProdutoArrayIterator() as $objItemSolicitacaoVO) {
                $objProdutoVO = new ProdutoVO;
                $objProdutoBO = new ProdutoBO;
                /* @var $objItemSolicitacaoVO ItemSolicitacaoVO  */
                $objItemSolicitacaoBO = new ItemSolicitacaoProdutoBO();
                $objItemSolicitacaoVO->getIdSolicitacao()->setId($objSolicitacaoVO->getId());

                $objItemSolicitacaoBO->inserir($objItemSolicitacaoVO);
//                var_dump($objItemSolicitacaoVO->getQuantidade()); die;

                $objProdutoVO->setId($objItemSolicitacaoVO->getIdProduto()->getId());
                $objProdutoVO->setQuantidade($objItemSolicitacaoVO->getQuantidade());

                $retornoRetirarProduto = $objProdutoBO->retirarProdutoEstoque($objProdutoVO);
            }

            $objDaoHelper->commit();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao($retornoRetirarProduto);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return boolean
     * @access public
     */
    public function alterar(SolicitacaoVO $objSolicitacaoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.SOLICITACAO_PRODUTO
                                   SET ID_SOLICITACAO_PRODUTO   = :ID_SOLICITACAO_PRODUTO,
                                       ID_FUNCIONARIO_USUARIO   = :ID_FUNCIONARIO_USUARIO,
                                       SITUACAO                 = :SITUACAO,
                                       OBSERVACAO               = :OBSERVACAO,
                                       DATA_ALTERACAO           = :DATA_ALTERACAO,
                                       USUARIO_ALTERACAO        = :USUARIO_ALTERACAO
                                   WHERE ID_SOLICITACAO_PRODUTO = :ID_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objSolicitacaoVO->getId());
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objSolicitacaoVO->getIdFuncionarioUsuario()->getId());
            $objDaoHelper->bindValue(":SITUACAO", $objSolicitacaoVO->getSituacao());
            $objDaoHelper->bindValue(":OBSERVACAO", $objSolicitacaoVO->getObservacao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objSolicitacaoVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", ($objSolicitacaoVO->getUsuarioAlteracao()));

//            echo($objDaoHelper->getSql()); exit;
            $objDaoHelper->execute(false);

            $objItemSolicitacaoProdutoBO = new ItemSolicitacaoProdutoBO();

            foreach ($objSolicitacaoVO->getItemSolicitacaoProdutoArrayIterator() as $objItemSolicitacaoVO) {
                if (empty($objItemSolicitacaoVO->getId())) {
                    $objItemSolicitacaoProdutoBO->inserir($objItemSolicitacaoVO);
                } else {
                    
                    $objItemSolicitacaoProdutoBO->alterar($objItemSolicitacaoVO);
                }
            }

            # Executando comando #
            $objDaoHelper->commit();
            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param SolicitacaoVO $objSolicitacaoVO
     * @return boolean
     * @access public
     */
    public function excluir(SolicitacaoVO $objSolicitacaoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.SOLICITACAO_PRODUTO
                                   SET DATA_ALTERACAO            = :DATA_ALTERACAO,
                                       USUARIO_ALTERACAO         = :USUARIO_ALTERACAO,
                                       EXCLUIDO                  = :EXCLUIDO
                                   WHERE ID_SOLICITACAO_PRODUTO  = :ID_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objSolicitacaoVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objSolicitacaoVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objSolicitacaoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 1);

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function selecionar(SolicitacaoVO $objSolicitacaoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT SC.ID_SOLICITACAO_PRODUTO,
                                          SC.ID_FUNCIONARIO_USUARIO,
                                          FC.NOME AS SOLICITANTE,
                                          SC.SITUACAO,
                                          SC.OBSERVACAO,
                                          SC.DATA_INCLUSAO,
                                          SC.USUARIO_INCLUSAO,
                                          SC.EXCLUIDO
                                   FROM NOVOFRAMEWORK.SOLICITACAO_PRODUTO SC
                                   INNER JOIN NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC ON SC.ID_FUNCIONARIO_USUARIO = FC.ID_FUNCIONARIO_USUARIO
                                   WHERE SC.EXCLUIDO = :EXCLUIDO
                                   AND ID_SOLICITACAO_PRODUTO = :ID_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objSolicitacaoVO->getId());
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objSolicitacaoVO->getIdFuncionarioUsuario()->getId());
            $objDaoHelper->bindValue(":NOME", $objSolicitacaoVO->getIdFuncionarioUsuario()->getId());

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso #            
            foreach ($objDaoHelper->fetchAll() as $solicitacao) {
                $objSolicitacaoVO->bind($solicitacao);
                $objSolicitacaoVO->getIdFuncionarioUsuario()->setNome($solicitacao['SOLICITANTE']);
            }
            $objDaoHelper->setRetornoOperacao($objSolicitacaoVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function existe(SolicitacaoVO $objSolicitacaoVO, $exceto = FALSE) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL (checar) #
            if ($exceto && $objSolicitacaoVO->getId() != NULL) {
                
            }

            $objDaoHelper->setSql("SELECT COUNT(FC.NOME) as TOTAL
                                   FROM NOVOFRAMEWORK.SOLICITACAO_PRODUTO SC
                                   INNER JOIN NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC ON SC.ID_FUNCIONARIO_USUARIO = FC.ID_FUNCIONARIO_USUARIO
                                   WHERE SC.EXCLUIDO = :EXCLUIDO
                                   AND SC.ID_FUNCIONARIO_USUARIO = :ID_FUNCIONARIO_USUARIO 
                                   AND SC.ID_SOLICITACAO_PRODUTO = :ID_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objSolicitacaoVO->getIdFuncionarioUsuario()->getId());
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objSolicitacaoVO->getId());

//            var_dump($objDaoHelper->getSql()); exit;

            if ($exceto) {
                $objDaoHelper->bindValue(":ID_FUNCIONARIO_USUARIO", $objSolicitacaoVO->getIdFuncionarioUsuario()->getId());
            }

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão o setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->fetchAll() as $solicitacao) {
                if ($solicitacao['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    // RAFAEL
    public function relatorioSolicitacaoNaoAtendidas(SolicitacaoVO $objSolicitacaoVO) {
        $objDaoHelper = new DaoHelper();

        # Obtendo conexão #
        try {
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";


            # Comando SQL #
            $objDaoHelper->setSql("SELECT SC.ID_SOLICITACAO_PRODUTO,
                                          FC.ID_FUNCIONARIO_USUARIO,
                                          FC.NOME AS NOME_FUNCIONARIO_USUARIO,
                                          SC.SITUACAO,
                                          SC.OBSERVACAO
                                   FROM 
                                        NOVOFRAMEWORK.SOLICITACAO_PRODUTO SC
                                   INNER JOIN 
                                        NOVOFRAMEWORK.FUNCIONARIO_USUARIO FC ON SC.ID_FUNCIONARIO_USUARIO = FC.ID_FUNCIONARIO_USUARIO
                                   WHERE
                                        SC.EXCLUIDO = :EXCLUIDO
                                        AND (SC.SITUACAO = :SITUACAO OR :SITUACAO IS NULL)
                                        ");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":SITUACAO", 'N');


//            echo $objDaoHelper->getSql();die;

            $objDaoHelper->execute();

            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $solicitacao) {
                $objSolicitacaoVO = new SolicitacaoVO();
                $objSolicitacaoVO->bind($solicitacao);
                $objSolicitacaoVO->getIdFuncionarioUsuario()->setNome($solicitacao['NOME_FUNCIONARIO_USUARIO']);
                $arrayIterator->append($objSolicitacaoVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new AppException($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function atender(SolicitacaoVO $objSolicitacaoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.SOLICITACAO_PRODUTO
                                        SET 
                                            DATA_ALTERACAO            = :DATA_ALTERACAO,
                                            USUARIO_ALTERACAO         = :USUARIO_ALTERACAO,
                                            SITUACAO                  = :SITUACAO
                                        WHERE 
                                            ID_SOLICITACAO_PRODUTO  = :ID_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objSolicitacaoVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objSolicitacaoVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objSolicitacaoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":SITUACAO", 'A');

            # Executando comando #
//            echo $objDaoHelper->getSql(); die;
            $objDaoHelper->execute(false);

            $objSaidaProdutoVO = new SaidaProdutoVO();
            $objSaidaProdutoBO = new SaidaProdutoBO();

            $objSaidaProdutoVO->getIdSolicitacaoProduto()->setId($objSolicitacaoVO->getId());
            $objSaidaProdutoVO->setUsuarioInclusao($objSolicitacaoVO->getUsuarioAlteracao());
            $objSaidaProdutoBO->inserir($objSaidaProdutoVO);


            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    public function cancelar(SolicitacaoVO $objSolicitacaoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.SOLICITACAO_PRODUTO
                                   SET DATA_ALTERACAO            = :DATA_ALTERACAO,
                                       USUARIO_ALTERACAO         = :USUARIO_ALTERACAO,
                                       SITUACAO                  = :SITUACAO
                                   WHERE ID_SOLICITACAO_PRODUTO  = :ID_SOLICITACAO_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objSolicitacaoVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objSolicitacaoVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objSolicitacaoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":SITUACAO", 'C');

            # Executando comando #
            $objDaoHelper->execute(false);

            $objItemSolicitacaoProdutoBO = new ItemSolicitacaoProdutoBO();
            $objItemSolicitacaoProdutoVO = new ItemSolicitacaoProdutoVO();
            

            $objItemSolicitacaoProdutoVO->getIdSolicitacao()->setId($objSolicitacaoVO->getId());
            $arrayItItemSolicitacao = $objItemSolicitacaoProdutoBO->listarPorSolicitacao($objItemSolicitacaoProdutoVO)['retornoOperacao'];

            foreach ($arrayItItemSolicitacao as $objItemSolicitacaoProdutoVO) {
            $objProdutoBO = new ProdutoBO();
            $objProdutoVO = new ProdutoVO();
                $objProdutoVO->setId($objItemSolicitacaoProdutoVO->getIdProduto()->getId());
                $objProdutoVO->setQuantidade($objItemSolicitacaoProdutoVO->getQuantidade());
                $objProdutoVO = $objProdutoBO->adicionarProdutoEstoque($objProdutoVO);
            }

            $objDaoHelper->commit();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

}
