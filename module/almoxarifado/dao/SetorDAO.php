<?php

namespace module\almoxarifado\dao;

use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\bo\SetorBO;
use module\almoxarifado\vo\SetorVO;
use core\helper\FormatHelper;

/**
 * Classe de persistência referente à Setor
 * @acess public
 * @package almoxarifado
 * @subpackage dao
 */
class SetorDAO extends AbstractDAO {

    /**
     * Método que realiza a listagem de Unidades cadastradas
     * @param SetorVO $objSetorVO
     * @return ArrayIterator
     * @throws Excepcion em caso de erro de banco de dados
     */
    public function listar(SetorVO $objSetorVO) {
        # Instanciando classe de apoio da camada #
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";
            
            if (!is_null($objSetorVO)) {
                if (strlen($objSetorVO->getNome()) > 0 ) {
                    $auxSQL = "AND TRANSLATE(UPPER(UPPER(NOME)), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE UPPER(:NOME)";
                }
            }

            # Comando SQL #
            $objDaoHelper->setSql("SELECT SE.ID_SETOR,
                                          SE.NOME,
                                          SE.USUARIO_INCLUSAO,
                                          SE.DATA_INCLUSAO
                                   FROM NOVOFRAMEWORK.SETOR SE
                                   WHERE SE.EXCLUIDO = :EXCLUIDO
                                   $auxSQL");
            
            if (!is_null($objSetorVO)) {
                if (strlen($objSetorVO->getNome()) > 0 ) {
                    $objDaoHelper->bindValue(":NOME", '%'. FormatHelper::removerAcentos($objSetorVO->getNome()) . '%');
                }
            }

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            # Executando comando #
            $objDaoHelper->execute();

            # Instanciando classes de apoio, construindo resultado #
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $setor) {
                $objSetorVO = new SetorVO();
                $objSetorVO->bind($setor);
                $arrayIterator->append($objSetorVO);
            }

            # Setando retorno em caso de sucesso #
            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resultado #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que adiciona Setores
     * @param SetorVO $objSetorVO
     * @return ArrayIterator
     * @throws Excepcion em caso de erro de banco de dados
     */
    public function inserir(SetorVO $objSetorVO) {
        $objDaoHelper = new DaoHelper();

        try {
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Setando ID do grupo #
            $objSetorVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_SETOR", $objDaoHelper->getConexao()));

            # Comando SQL #
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.SETOR
                                  (
                                   ID_SETOR,
                                   NOME,
                                   DATA_INCLUSAO,
                                   USUARIO_INCLUSAO,
                                   EXCLUIDO)                                    
                                 VALUES 
                                 ( 
                                   :ID_SETOR,
                                   :NOME,                        
                                   sysdate,
                                   :USUARIO_INCLUSAO,
                                   :EXCLUIDO)");
            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":ID_SETOR", $objSetorVO->getId());
            $objDaoHelper->bindValue(":NOME", $objSetorVO->getNome());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objSetorVO->getUsuarioInclusao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0) /* ATIVO 0 -- INATIVO 1 */;

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que altera os Setores existentes
     * @param SetorVO $objSetorVO
     * @return mixed
     * @access public
     */
    public function alterar(SetorVO $objSetorVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.SETOR SE
                                    SET    SE.DATA_ALTERACAO    = :DATA_ALTERACAO,                                          
                                           SE.EXCLUIDO          = :EXCLUIDO,                                                                               
                                           SE.USUARIO_ALTERACAO = :USUARIO_ALTERACAO,
                                           SE.NOME              = :NOME
                                    WHERE  SE.ID_SETOR          = :ID_SETOR");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            $objDaoHelper->bindValue(":NOME", $objSetorVO->getNome());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objSetorVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objSetorVO->getDataAlteracao());
            $objDaoHelper->bindValue(":ID_SETOR", $objSetorVO->getId());

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que exclui os setores existentes
     * @param SetorVO $objSetorVO
     * @return boolean
     * @access public
     */
    public function excluir(SetorVO $objSetorVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            
            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.SETOR SE
                                   SET    SE.DATA_ALTERACAO    = :DATA_ALTERACAO,
                                          SE.EXCLUIDO          = :EXCLUIDO,
                                          SE.USUARIO_ALTERACAO = :USUARIO_ALTERACAO
                                   WHERE  SE.ID_SETOR          = :ID_SETOR");
            
            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_SETOR", $objSetorVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objSetorVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objSetorVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 1);
//            var_dump($objDaoHelper->getSql()); exit;
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
        
        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Método que seleciona as Unidades existentes
     * @param $objSetorVO
     * @return boolean
     * @access public
     */
    public function selecionar($objSetorVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT 
                                       SE.ID_SETOR,
                                       SE.NOME,                                       
                                       SE.DATA_INCLUSAO,
                                       SE.DATA_ALTERACAO,
                                       SE.USUARIO_INCLUSAO,
                                       SE.USUARIO_ALTERACAO
                                    FROM NOVOFRAMEWORK.SETOR SE
                                    WHERE SE.EXCLUIDO = :EXCLUIDO
                                    AND SE.ID_SETOR   = :ID_SETOR");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":EXCLUIDO", 0); # ATIVO 0 -- INATIVO 1 #
            $objDaoHelper->bindValue(":ID_SETOR", $objSetorVO->getId());

            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->fetchAll() as $setor) {
                $objSetorVO = new SetorVO();
                $objSetorVO->bind($setor);
            }
            $objDaoHelper->setRetornoOperacao($objSetorVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Puxa a tabela de Setor para a combo box da tela de Funcionário
     * @return String
     */
    public function listarCombo() {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            $objDaoHelper->setSql("SELECT SE.ID_SETOR,
                                          SE.NOME
                                   FROM NOVOFRAMEWORK.SETOR SE
                                   WHERE SE.EXCLUIDO = 0
                                   ORDER BY NOME");

            $objDaoHelper->execute();

            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $setor) {
                $objSetorVO = new SetorVO();
                $objSetorVO->bind($setor);
                $arrayIterator->append($objSetorVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * Verifica a existência de um registro
     * @param SetorVO $objSetorVO
     * @return boolean
     * @access public
     */
    public function existe(SetorVO $objSetorVO, $exceto = FALSE) {
        $objDaoHelper = new DaoHelper();
        $sqlAuxiliar = '';

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            if ($objSetorVO->getId() != null) {
                $sqlAuxiliar = ' AND SE.ID_SETOR <> :ID_SETOR ';
            }

            $objDaoHelper->setSql("SELECT COUNT(NOME) as TOTAL
                                   FROM NOVOFRAMEWORK.SETOR SE
                                   WHERE SE.EXCLUIDO = :EXCLUIDO
                                   AND TRANSLATE(UPPER(SE.NOME), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE :NOME
                                   $sqlAuxiliar");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":NOME", strtoupper(FormatHelper::removerAcentos($objSetorVO->getNome())));
            $objDaoHelper->bindValue(":EXCLUIDO", 0);

            if ($objSetorVO->getId() != null) {
                $objDaoHelper->bindValue(":ID_SETOR", $objSetorVO->getId());
            }
//            var_dump($objDaoHelper); exit;
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE #
            foreach ($objDaoHelper->fetchAll() as $setor) {
                if ($setor['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetornoOperacao();
    }

}
