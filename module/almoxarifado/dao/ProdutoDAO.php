<?php

namespace module\almoxarifado\dao;

use core\exception\AppException;
use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use core\helper\FormatHelper;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;
use module\almoxarifado\vo\ItemSolicitacaoProdutoVO;

/**
 * Classe de persistencia Produto
 */
class ProdutoDAO extends AbstractDAO {

    /**
     * @param void
     * @return ArrayIterator
     * @access public
     */
    public function listar(ProdutoVO $objProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexão
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";

            if (!is_null($objProdutoVO)) {
//                var_dump($objProdutoVO->getNome()); die;
                if (strlen($objProdutoVO->getNome()) > 0) {
                    $auxSQL = "AND PD.NOME LIKE :NOME";
                }

                if (strlen($objProdutoVO->getCodigo()) > 0) {
                    $auxSQL .= "AND CODIGO LIKE :CODIGO";
                }
            }

            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                            PD.ID_PRODUTO,
                                            PD.ID_UNIDADE,
                                            CT.ID_CATEGORIA,
                                            PD.NOME AS NOME_PRODUTO,
                                            U.NOME AS NOME_UNIDADE,
                                            CT.NOME AS NOME_CATEGORIA,
                                            PD.CODIGO,
                                            PD.QUANTIDADE,
                                            PD.DESCRICAO,
                                            PD.OBSERVACAO
                                        FROM NOVOFRAMEWORK.PRODUTO PD 
                                        INNER JOIN
                                        NOVOFRAMEWORK.UNIDADE U ON PD.ID_UNIDADE = U.ID_UNIDADE
                                        INNER JOIN
                                        NOVOFRAMEWORK.CATEGORIA CT ON PD.ID_CATEGORIA = CT.ID_CATEGORIA
                                        WHERE PD.EXCLUIDO = :EXCLUIDO
                                        AND (U.ID_UNIDADE = :ID_UNIDADE OR :ID_UNIDADE IS NULL)
                                        AND (CT.ID_CATEGORIA = :ID_CATEGORIA OR :ID_CATEGORIA IS NULL)
                                        $auxSQL");


            // Atribuindo valores
            $objDaoHelper->bindValue(":CODIGO", $objProdutoVO->getCodigo());
            $objDaoHelper->bindValue(":ID_UNIDADE", $objProdutoVO->getIdUnidade()->getId());
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objProdutoVO->getIdCategoria()->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1


            if (!is_null($objProdutoVO)) {
                if (strlen($objProdutoVO->getNome()) > 0) {
                    $objDaoHelper->bindValue(":NOME", '%' . FormatHelper::removerAcentos($objProdutoVO->getNome()) . '%');
                }
            }


            //echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute();

            // Instanciando classes de apoio
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $produto) {
                $objProdutoVO = new ProdutoVO();
                /* @var $objProdutoVO ProdutoVO */
                $objProdutoVO->bind($produto);
                $objProdutoVO->setNome($produto['NOME_PRODUTO']);
                $objProdutoVO->getIdUnidade()->setNome($produto['NOME_UNIDADE']);
                $objProdutoVO->getIdCategoria()->setNome($produto['NOME_CATEGORIA']);
                $arrayIterator->append($objProdutoVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new AppException($ex->getMessage());
        }
        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param ProdutoVO $objProdutoVO
     * @return boolean
     * @access public
     */
    public function inserir(ProdutoVO $objProdutoVO) {

        $objDaoHelper = new DaoHelper();

        if (!is_null($objProdutoVO)) {
            if (strlen($objProdutoVO->getUsuarioInclusao()) > 0) {
                $auxSQL = " AND TRANSLATE(UPPER(UPPER(USUARIO_INCLUSAO)), 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'AAAAAAAAEEEEIIOOOOOOUUUUCC') LIKE UPPER(:USUARIO_INCLUSAO)";
            }
        }



        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            //Setando Id do grupo
            $objProdutoVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_PRODUTO", $objDaoHelper->getConexao()));

            // Comando SQL
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.PRODUTO
                                  (
                                   ID_PRODUTO,
                                   ID_UNIDADE,
                                   ID_CATEGORIA,
                                   DESCRICAO,
                                   NOME,
                                   CODIGO,
                                   OBSERVACAO,
                                   QUANTIDADE,
                                   DATA_INCLUSAO,
                                   USUARIO_INCLUSAO,
                                   EXCLUIDO )                                    
                                 VALUES 
                                 ( 
                                   :ID_PRODUTO,
                                   :ID_UNIDADE,
                                   :ID_CATEGORIA,
                                   :DESCRICAO,
                                   :NOME,
                                   :CODIGO,
                                   :OBSERVACAO,
                                   :QUANTIDADE,
                                   sysdate,
                                   :USUARIO_INCLUSAO,
                                   :EXCLUIDO )
                                     ");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_PRODUTO", $objProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_UNIDADE", $objProdutoVO->getIdUnidade()->getId());
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objProdutoVO->getIdCategoria()->getId());
            $objDaoHelper->bindValue(":DESCRICAO", $objProdutoVO->getDescricao());
            $objDaoHelper->bindValue(":NOME", $objProdutoVO->getNome());
            $objDaoHelper->bindValue(":CODIGO", $objProdutoVO->getCodigo());
            $objDaoHelper->bindValue(":OBSERVACAO", $objProdutoVO->getObservacao());
            $objDaoHelper->bindValue(":QUANTIDADE", $objProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1

            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objProdutoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", FormatHelper::removerAcentos($objProdutoVO->getUsuarioInclusao()));

            // Executando comando
            //echo($objDaoHelper->getSql());die;
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objDaoHelper->setRetornoOperacao(TRUE);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param ProdutoVO $objProdutoVO
     * @return mixed
     * @access public
     */
    public function alterar(ProdutoVO $objProdutoVO) {
        $objDaoHelper = new DaoHelper();
        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL (checar) #
            $objDaoHelper->setSql(" UPDATE 
                                            NOVOFRAMEWORK.PRODUTO
                                    SET 
                                            ID_PRODUTO        = :ID_PRODUTO,
                                            ID_UNIDADE        = :ID_UNIDADE,
                                            ID_CATEGORIA      = :ID_CATEGORIA,
                                            DESCRICAO         = :DESCRICAO,
                                            NOME              = :NOME,
                                            CODIGO            = :CODIGO,
                                            OBSERVACAO        = :OBSERVACAO,
                                            QUANTIDADE        = :QUANTIDADE,
                                            DATA_ALTERACAO    = :DATA_ALTERACAO,                                          
                                            EXCLUIDO          = :EXCLUIDO,                                                                               
                                            USUARIO_ALTERACAO = :USUARIO_ALTERACAO
                                    WHERE  
                                            ID_PRODUTO        = :ID_PRODUTO
                                    ");

            # Atribuindo valores (checar) #
            $objDaoHelper->bindValue(":ID_PRODUTO", $objProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_UNIDADE", $objProdutoVO->getIdUnidade()->getId());
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objProdutoVO->getIdCategoria()->getId());
            $objDaoHelper->bindValue(":DESCRICAO", $objProdutoVO->getDescricao());
            $objDaoHelper->bindValue(":NOME", $objProdutoVO->getNome());
            $objDaoHelper->bindValue(":CODIGO", $objProdutoVO->getCodigo());
            $objDaoHelper->bindValue(":OBSERVACAO", $objProdutoVO->getObservacao());
            $objDaoHelper->bindValue(":QUANTIDADE", $objProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objProdutoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objProdutoVO->getDataAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            //echo $objDaoHelper->getSql();exit();
            # Executando comando #
            $objDaoHelper->execute();

            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param ProdutoVO $objProdutoVO
     * @return boolean
     * @access public
     */
    public function excluir($objProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.PRODUTO
                                    SET    
                                           DATA_ALTERACAO    = :DATA_ALTERACAO,
                                           EXCLUIDO          = :EXCLUIDO,
                                           USUARIO_ALTERACAO = :USUARIO_ALTERACAO
                                    WHERE  
                                           ID_PRODUTO        = :ID_PRODUTO");


            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_PRODUTO", $objProdutoVO->getId());
            $objDaoHelper->bindValue(":DATA_ALTERACAO", $objProdutoVO->getDataAlteracao());
            $objDaoHelper->bindValue(":USUARIO_ALTERACAO", $objProdutoVO->getUsuarioAlteracao());
            $objDaoHelper->bindValue(":EXCLUIDO", 1); //ATIVO 0 -- INATIVO 1
            // Executando comando
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objDaoHelper->setRetornoOperacao(TRUE);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param ProdutoVO $objProdutoVO
     * @return boolean
     * @access public
     */
    public function selecionar($objProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        PD.ID_PRODUTO,
                                        PD.ID_UNIDADE,
                                        PD.ID_CATEGORIA,
                                        PD.DESCRICAO,
                                        PD.NOME,
                                        PD.CODIGO,
                                        PD.OBSERVACAO,
                                        PD.QUANTIDADE,
                                        PD.DATA_ALTERACAO,
                                        PD.USUARIO_ALTERACAO
                                        FROM NOVOFRAMEWORK.PRODUTO PD 
                                   WHERE     
                                        PD.EXCLUIDO = :EXCLUIDO
                                        AND ID_PRODUTO = :ID_PRODUTO");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_PRODUTO", $objProdutoVO->getId());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            //echo $objDaoHelper->getSql();exit();
            // Executando comando            
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objProdutoVO = new ProdutoVO();
            foreach ($objDaoHelper->fetchAll() as $produto) {
                $objProdutoVO->bind($produto);
            }
            $objDaoHelper->setRetornoOperacao($objProdutoVO);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param ProdutoVO $objProdutoVO
     * @return boolean
     * @access public
     */
    public function existe(ProdutoVO $objProdutoVO, $exceto = FALSE) {

        $objDaoHelper = new DaoHelper();
        $sqlAuxiliar = '';

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL

            if ($exceto && $objProdutoVO->getId() != null) {
                $sqlAuxiliar = "AND ID_PRODUTO != :ID_PRODUTO";
            }

            $objDaoHelper->setSql("SELECT  COUNT(CODIGO) as TOTAL
                                   FROM NOVOFRAMEWORK.PRODUTO
                                   WHERE EXCLUIDO = :EXCLUIDO                                   
                                   AND CODIGO = :CODIGO 
                                    $sqlAuxiliar                                   
                                    ");
            // Atribuindo valores
            $objDaoHelper->bindValue(":EXCLUIDO", 0);
            $objDaoHelper->bindValue(":CODIGO", $objProdutoVO->getCodigo());

            if ($exceto) {
                $objDaoHelper->bindValue(":ID_PRODUTO", $objProdutoVO->getId());
            }
            //echo $objDaoHelper->getSql();exit();
            // Executando comando
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            foreach ($objDaoHelper->fetchAll() as $produto) {
                if ($produto['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param ProdutoVO $objProdutoVO
     * @return boolean
     * @access public
     */
    public function existeDependencia(ProdutoVO $objProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $objDaoHelper->setSql("SELECT 
                                        COUNT(ID_CURSO) as TOTAL
                                   FROM 
                                       NOVOFRAMEWORK.DIPLOMA
                                   WHERE
                                        EXCLUIDO = :EXCLUIDO                                   
                                        AND ID_CURSO = :ID_CURSO
                                   ");
            // Atribuindo valores
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            $objDaoHelper->bindValue(":ID_CURSO", $objProdutoVO->getId());
            //echo $objDaoHelper->getSql();exit();
            // Executando comando
            $objDaoHelper->execute();
            foreach ($objDaoHelper->fetchAll() as $produto) {
                if ($produto['TOTAL'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                } else {
                    $objDaoHelper->setRetornoOperacao(FALSE);
                }
            }

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        // Retornando resposta
        return $objDaoHelper->getRetornoOperacao();
    }

    public function relatorioDeProduto(ProdutoVO $objProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexão
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";

            if (!is_null($objProdutoVO)) {
                if (strlen($objProdutoVO->getNome()) > 0) {
                    $auxSQL = "AND PD.NOME LIKE :NOME";
                }
            }

            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                            PD.ID_PRODUTO,
                                            PD.ID_UNIDADE,
                                            CT.ID_CATEGORIA,
                                            PD.NOME AS NOME_PRODUTO,
                                            U.NOME AS NOME_UNIDADE,
                                            CT.NOME AS NOME_CATEGORIA,
                                            PD.CODIGO,
                                            PD.QUANTIDADE,
                                            PD.DESCRICAO,
                                            PD.OBSERVACAO
                                        FROM NOVOFRAMEWORK.PRODUTO PD 
                                        INNER JOIN
                                        NOVOFRAMEWORK.UNIDADE U ON PD.ID_UNIDADE = U.ID_UNIDADE
                                        INNER JOIN
                                        NOVOFRAMEWORK.CATEGORIA CT ON PD.ID_CATEGORIA = CT.ID_CATEGORIA
                                        WHERE PD.EXCLUIDO = :EXCLUIDO
                                        AND (U.ID_UNIDADE = :ID_UNIDADE OR :ID_UNIDADE IS NULL)
                                        AND (CT.ID_CATEGORIA = :ID_CATEGORIA OR :ID_CATEGORIA IS NULL)
                                        $auxSQL");


            // Atribuindo valores
            $objDaoHelper->bindValue(":CODIGO", $objProdutoVO->getCodigo());
            $objDaoHelper->bindValue(":ID_UNIDADE", $objProdutoVO->getIdUnidade()->getId());
            $objDaoHelper->bindValue(":ID_CATEGORIA", $objProdutoVO->getIdCategoria()->getId());
            $objDaoHelper->bindValue(":NOME", $objProdutoVO->getNome());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1


            if (!is_null($objProdutoVO)) {
                if (strlen($objProdutoVO->getDescricao()) > 0) {
                    $objDaoHelper->bindValue(":NOME", '%' . FormatHelper::removerAcentos($objProdutoVO->getNome()) . '%');
                }
            }


            //echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute();

            // Instanciando classes de apoio
            $arrayIterator = new \ArrayIterator();

            foreach ($objDaoHelper->fetchAll() as $produto) {
                $objProdutoVO = new ProdutoVO();
                /* @var $objProdutoVO ProdutoVO */
                $objProdutoVO->bind($produto);
                $objProdutoVO->setNome($produto['NOME_PRODUTO']);
                $objProdutoVO->getIdUnidade()->setNome($produto['NOME_UNIDADE']);
                $objProdutoVO->getIdCategoria()->setNome($produto['NOME_CATEGORIA']);
                $arrayIterator->append($objProdutoVO);
            }

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new AppException($ex->getMessage());
        }
        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param ProdutoVO $objProdutoVO
     * @return mixed
     * @access public
     */
    public function adicionarProdutoEstoque(ProdutoVO $objProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.PRODUTO PR
                                   SET QUANTIDADE = (
                                       SELECT PR.QUANTIDADE + :QUANTIDADE 
                                       FROM NOVOFRAMEWORK.PRODUTO PR
                                       WHERE PR.ID_PRODUTO = :ID_PRODUTO)
                                   WHERE ID_PRODUTO = :ID_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_PRODUTO", $objProdutoVO->getId());
            $objDaoHelper->bindValue(":QUANTIDADE", $objProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1

            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objProdutoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", FormatHelper::removerAcentos($objProdutoVO->getUsuarioInclusao()));

//            echo ($objDaoHelper->getSql()); die;
            # Executando comando #
            $objDaoHelper->execute(false);
//            var_dump($objDaoHelper); die;
            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper:: registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param ProdutoVO $objProdutoVO
     * @return mixed
     * @access public
     */
    public function retirarProdutoEstoque(ProdutoVO $objProdutoVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            # Comando SQL #
            $objDaoHelper->setSql("UPDATE NOVOFRAMEWORK.PRODUTO 
                                   SET QUANTIDADE = (
                                       SELECT PR.QUANTIDADE - :QUANTIDADE 
                                       FROM NOVOFRAMEWORK.PRODUTO PR
                                       WHERE PR.ID_PRODUTO = :ID_PRODUTO)
                                   WHERE ID_PRODUTO = :ID_PRODUTO");

            # Atribuindo valores #
            $objDaoHelper->bindValue(":ID_PRODUTO", $objProdutoVO->getId());
            $objDaoHelper->bindValue(":QUANTIDADE", $objProdutoVO->getQuantidade());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1

            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objProdutoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", FormatHelper::removerAcentos($objProdutoVO->getUsuarioInclusao()));

//            echo ($objDaoHelper->getSql()); die;
//            
            # Executando comando #
            $objDaoHelper->execute(false);
            
            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            $objDaoHelper->setRetornoOperacao(TRUE);

            # Fechando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper:: registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        # Retornando resposta #
        return $objDaoHelper->getRetorno();
    }

    
    public function listarQuantidade($objProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("
                                    SELECT 
                                        PD.QUANTIDADE
                                    FROM 
                                        NOVOFRAMEWORK.PRODUTO PD
                                    WHERE ID_PRODUTO = :ID_PRODUTO"
                                        );

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_PRODUTO",$objProdutoVO->getId()); 
            // Executando comando            
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
           
            $objDaoHelper->setRetornoOperacao($objDaoHelper->fetch()[0]);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }
    
    
}
