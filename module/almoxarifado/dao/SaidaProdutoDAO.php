<?php

namespace module\almoxarifado\dao;

use core\exception\AppException;
use core\dao\AbstractDAO;
use core\helper\DaoHelper;
use module\almoxarifado\vo\SaidaProdutoVO;
use module\almoxarifado\bo\SaidaProdutoBO;
use module\almoxarifado\bo\SolicitacaoBO;
use module\almoxarifado\vo\SolicitacaoVO;

/**
 * Classe de persistencia SaidaProduto
 */
class SaidaProdutoDAO extends AbstractDAO {

    /**
     * @param void
     * @return ArrayIterator
     * @access public
     */
    public function inserir(SaidaProdutoVO $objSaidaProdutoVO) {

        $objDaoHelper = new DaoHelper();


        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            //Setando Id do grupo
            $objSaidaProdutoVO->setId($this->getSequenceNextVal("NOVOFRAMEWORK.SEQ_SAIDA_PRODUTO", $objDaoHelper->getConexao()));

            // Comando SQL
            $objDaoHelper->setSql("INSERT INTO NOVOFRAMEWORK.SAIDA_PRODUTO SP
                                  (
                                   SP.ID_SAIDA_PRODUTO,
                                   SP.ID_SOLICITACAO_PRODUTO,
                                   SP.OBSERVACAO,
                                   SP.EXCLUIDO,
                                   SP.USUARIO_INCLUSAO,
                                   SP.DATA_INCLUSAO)   
                                   
                                 VALUES 
                                 ( 
                                   :ID_SAIDA_PRODUTO,
                                   :ID_SOLICITACAO_PRODUTO,
                                   :OBSERVACAO,
                                   :EXCLUIDO,
                                   :USUARIO_INCLUSAO,
                                    sysdate )");

            // Atribuindo valores

            $objDaoHelper->bindValue(":ID_SAIDA_PRODUTO", $objSaidaProdutoVO->getId());
            $objDaoHelper->bindValue(":ID_SOLICITACAO_PRODUTO", $objSaidaProdutoVO->getIdSolicitacaoProduto()->getId());
            $objDaoHelper->bindValue(":OBSERVACAO", $objSaidaProdutoVO->getObservacao());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1

            $objDaoHelper->bindValue(":DATA_INCLUSAO", $objSaidaProdutoVO->getDataInclusao());
            $objDaoHelper->bindValue(":USUARIO_INCLUSAO", $objSaidaProdutoVO->getUsuarioInclusao());

            // Executando comando
//            echo($objDaoHelper->getSql());die;

            $objDaoHelper->execute(TRUE);


            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objDaoHelper->setRetornoOperacao(TRUE);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    public function listar(SaidaProdutoVO $objSaidaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexão
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));
            $auxSQL = "";



            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        SP.ID_SAIDA_PRODUTO, 
                                        SP.ID_SOLICITACAO_PRODUTO,
                                        SP.OBSERVACAO,
                                        SP.DATA_INCLUSAO,
                                        SOL.ID_FUNCIONARIO_USUARIO,
                                        FU.NOME,
                                        (select COUNT(D.ID_DEVOLUCAO_PRODUTO) FROM NOVOFRAMEWORK.DEVOLUCAO_PRODUTO D WHERE D.ID_SAIDA_PRODUTO = SP.ID_SAIDA_PRODUTO ) AS SAIDA
                                    FROM 
                                        NOVOFRAMEWORK.SAIDA_PRODUTO SP
                                    INNER JOIN 
                                        NOVOFRAMEWORK.SOLICITACAO_PRODUTO SOL ON SP.ID_SOLICITACAO_PRODUTO = SOL.ID_SOLICITACAO_PRODUTO
                                    INNER JOIN 
                                        NOVOFRAMEWORK.FUNCIONARIO_USUARIO FU ON SOL.ID_FUNCIONARIO_USUARIO = FU.ID_FUNCIONARIO_USUARIO
                                    WHERE 
                                        SP.EXCLUIDO = :EXCLUIDO
                                        AND (SP.DATA_INCLUSAO BETWEEN :DATA_INICIAL and :DATA_FINAL OR :DATA_INICIAL IS NULL OR :DATA_FINAL IS NULL)
                                        ORDER BY DATA_INCLUSAO DESC ");



            // Atribuindo valores

            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
            $objDaoHelper->bindValue(":DATA_FINAL", $objSaidaProdutoVO->getDataFinal());
            $objDaoHelper->bindValue(":DATA_INICIAL", $objSaidaProdutoVO->getDataInicial());
            $objDaoHelper->bindValue(":NOME", $objSaidaProdutoVO->getIdSolicitacaoProduto()->getIdFuncionarioUsuario()->getNome());
            $objDaoHelper->bindValue(":SAIDA", $objSaidaProdutoVO->getSaida());

//            echo $objDaoHelper->getSql();exit();
            $objDaoHelper->execute();

            // Instanciando classes de apoio
            $arrayIterator = new \ArrayIterator();
            foreach ($objDaoHelper->fetchAll() as $SaidaProduto) {
                $objSaidaProdutoVO = new SaidaProdutoVO();
                /* @var $objSaidaProdutoVO SaidaProdutoVO */
                $objSaidaProdutoVO->bind($SaidaProduto);
                $objSaidaProdutoVO->getIdSolicitacaoProduto()->getidFuncionarioUsuario()->setNome($SaidaProduto['NOME']);
                $arrayIterator->append($objSaidaProdutoVO);
//                var_dump($objSaidaProdutoVO);die;
            }
//            var_dump($arrayIterator); die;

            $objDaoHelper->setRetornoOperacao($arrayIterator);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new AppException($ex->getMessage());
        }
        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    /**
     * @param SaidaProdutoVO $objSaidaProdutoVO
     * @return boolean
     * @access public
     */
    public function selecionar(SaidaProdutoVO $objSaidaProdutoVO) {

        $objDaoHelper = new DaoHelper();

        try { // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('NOVOFRAMEWORK'));

            // Comando SQL
            $objDaoHelper->setSql("SELECT 
                                        SP.ID_SAIDA_PRODUTO, 
                                        SP.ID_SOLICITACAO_PRODUTO,
                                        SP.OBSERVACAO,
                                        SP.DATA_INCLUSAO,
                                        SOL.ID_FUNCIONARIO_USUARIO,
                                        FU.NOME 
                                    FROM 
                                        NOVOFRAMEWORK.SAIDA_PRODUTO SP
                                    INNER JOIN 
                                        NOVOFRAMEWORK.SOLICITACAO_PRODUTO SOL ON SP.ID_SOLICITACAO_PRODUTO = SOL.ID_SOLICITACAO_PRODUTO
                                    INNER JOIN 
                                        NOVOFRAMEWORK.FUNCIONARIO_USUARIO FU ON SOL.ID_FUNCIONARIO_USUARIO = FU.ID_FUNCIONARIO_USUARIO
                                    WHERE 
                                        SP.EXCLUIDO = :EXCLUIDO
                                        AND ID_SAIDA_PRODUTO = :ID_SAIDA_PRODUTO");

            // Atribuindo valores
            $objDaoHelper->bindValue(":ID_SAIDA_PRODUTO", $objSaidaProdutoVO->getId());
            $objDaoHelper->bindValue(":NOME", $objSaidaProdutoVO->getIdSolicitacaoProduto()->getIdFuncionarioUsuario()->getNome());
            $objDaoHelper->bindValue(":EXCLUIDO", 0); //ATIVO 0 -- INATIVO 1
//            echo $objDaoHelper->getSql();exit();
            // Executando comando            
            $objDaoHelper->execute();

            // Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE.
            $objSaidaProdutoVO = new SaidaProdutoVO();
            foreach ($objDaoHelper->fetchAll() as $saidaProduto) {
                $objSaidaProdutoVO->getIdSolicitacaoProduto()->getidFuncionarioUsuario()->setNome($saidaProduto['NOME']);

                $objSaidaProdutoVO->bind($saidaProduto);
            }
            $objDaoHelper->setRetornoOperacao($objSaidaProdutoVO);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            //      LogHelper::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }


        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

}
