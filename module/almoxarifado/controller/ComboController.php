<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use module\almoxarifado\bo\MunicipioBO;
use module\almoxarifado\vo\MunicipioVO;
use module\almoxarifado\bo\EscolaCodSecBO;
use module\almoxarifado\vo\EscolaCodSecVO;
use module\almoxarifado\bo\TipoEnsinoBO;
use module\almoxarifado\vo\TipoEnsinoVO;
use module\almoxarifado\bo\SerieBO;
use module\almoxarifado\vo\SerieVO;
use module\almoxarifado\bo\TurnoBO;
use module\almoxarifado\vo\TurnoVO;
use module\almoxarifado\bo\TurmaBO;
use module\almoxarifado\vo\TurmaVO;
use module\almoxarifado\bo\AreaConhecimentoBO;
use module\almoxarifado\vo\AreaConhecimentoVO;
use module\almoxarifado\bo\DisciplinaBO;
use module\almoxarifado\vo\DisciplinaVO;
use module\almoxarifado\bo\ProfessorBO;
use module\almoxarifado\vo\ProfessorVO;
use module\almoxarifado\bo\CompetenciaBO;
use module\almoxarifado\vo\CompetenciaVO;
use module\almoxarifado\bo\HabilidadeBO;
use module\almoxarifado\vo\HabilidadeVO;
use module\almoxarifado\vo\ServidorVO;
use module\almoxarifado\bo\ServidorBO;
use module\almoxarifado\vo\EixoIntegradorVO;
use module\almoxarifado\vo\EixoCompetenciaHabilidadeVO;
use module\almoxarifado\bo\EixoIntegradorBO;

class ComboController extends AbstractController {

    private function makeOptions($array, $value = NULL, $text = NULL, $defaultOption = FALSE, $selecionado = '') {

        if ($defaultOption === TRUE) {
            echo '<option value="">Selecione...</option>';
        } else if ($defaultOption === FALSE) {
            
        } else {
            echo '<option value="">' . $defaultOption . '</option>';
        }

        if (!is_null($value) && !is_null($text)) {

            $value = "get" . $value;
            $text = "get" . $text;

            foreach ($array as $key => $obj) {
                echo '<option value="' . $obj->$value() . '" data-subtext="" ' . ($obj->$value() === $selecionado ? 'selected' : '') . '>' . $obj->$text() . '</option>';
            }
        } else {
            foreach ($array as $key => $value) {
                echo '<option value="' . $key . '" data-subtext="" ' . ($key === $selecionado ? 'selected' : '') . '>' . $value . '</option>';
            }
        }
    }

    public function getMunicipios() {


        $idNte = $this->getParams()[0];

        empty($idNte) ? die : null;

        $objMunicipioBO = new MunicipioBO();
        $objMunicipioVO = new MunicipioVO();

        $objMunicipioVO->getIdNte()->setId($idNte);

        $municipioArrayIterator = $objMunicipioBO->listarPorIdNte($objMunicipioVO)['retornoOperacao'];

        $this->makeOptions($municipioArrayIterator, 'Id', 'Nome');
    }

    public function getEscolas() {

        $idMunicipio = $this->getParams()[0];

        empty($idMunicipio) ? die : null;

        $objEscolaCodSecBO = new EscolaCodSecBO();
        $objEscolaCodSecVO = new EscolaCodSecVO();

        $objEscolaCodSecVO->getIdMunicipio()->setId($idMunicipio);
        $municipioArrayIterator = $objEscolaCodSecBO->listarPorMunicipio($objEscolaCodSecVO)['retornoOperacao'];

        $this->makeOptions($municipioArrayIterator, 'Id', 'Descricao');
    }

    public function getEscolasSemAnexo() {

        $idMunicipio = $this->getParams()[0];

        empty($idMunicipio) ? die : null;

        $objEscolaCodSecBO = new EscolaCodSecBO();
        $objEscolaCodSecVO = new EscolaCodSecVO();

        $objEscolaCodSecVO->getIdMunicipio()->setId($idMunicipio);
        $municipioArrayIterator = $objEscolaCodSecBO->listarPorMunicipioSemAnexo($objEscolaCodSecVO)['retornoOperacao'];

        $this->makeOptions($municipioArrayIterator, 'Id', 'Descricao');
    }

    public function getTipoEnsino() {


        $idEscola = $this->getParams()[0];
        empty($idEscola) ? die : null;

        $objTipoEnsinoBO = new TipoEnsinoBO();
        $objTipoEnsinoVO = new TipoEnsinoVO();

        $objTipoEnsinoVO->setIdEscolaCodSecTransient($idEscola);

        $TipoEnsinoArrayIt = $objTipoEnsinoBO->listarPorEscola($objTipoEnsinoVO)['retornoOperacao'];
        $this->makeOptions($TipoEnsinoArrayIt, 'Id', 'TipoEnsino');
    }

    public function getSerie() {

        $idTipoEnsino = $this->getParams()[0];
        $idCodSec = $this->getParams()[2];

        empty($idTipoEnsino) || empty($idCodSec) ? die : null;

        $objSerieBO = new SerieBO();
        $objSerieVO = new SerieVO();

        $objSerieVO->setIdEscolaCodSecTransient($idCodSec);
        $objSerieVO->getIdTipoEnsino()->setId($idTipoEnsino);

        $TipoSerieArrayIt = $objSerieBO->listarPorCodSecETiPoEnsino($objSerieVO)['retornoOperacao'];
        $this->makeOptions($TipoSerieArrayIt, 'Id', 'Serie');
    }

    public function getSerieByTipoEnsino() {

        $idTipoEnsino = $this->getParams()[0];

        empty($idTipoEnsino) ? die : null;

        $objSerieBO = new SerieBO();
        $objSerieVO = new SerieVO();

        $objSerieVO->getIdTipoEnsino()->setId($idTipoEnsino);

        $TipoSerieArrayIt = $objSerieBO->listarByTiPoEnsino($objSerieVO)['retornoOperacao'];

        $this->makeOptions($TipoSerieArrayIt, 'Id', 'Serie');
    }

    public function getEixoIntegradorByEixoCompetenciaHabilidade() {
        $objEixoCompetenciaHabilidadeVO = new EixoCompetenciaHabilidadeVO();

        $idDisciplina = $this->getParams()[0];
        $idAreaConhecimento = $this->getParams()[2];
        $idSerie = $this->getParams()[3];
        $idTipoEnsino = $this->getParams()[4];

//        disciplina,IDAREA_CONHECIMENTO,IDSERIE,IDTIPO_ENSINO

        $objEixoCompetenciaHabilidadeVO->getIdTipoEnsino()->setId($idTipoEnsino);
        $objEixoCompetenciaHabilidadeVO->getIdSerie()->setId($idSerie);
        $objEixoCompetenciaHabilidadeVO->getIdAreaConhecimento()->setId($idAreaConhecimento);
        $objEixoCompetenciaHabilidadeVO->getIdDisciplina()->setId($idDisciplina);

        empty($idTipoEnsino) ? die : null;


        $objEixoIntegradorBO = new EixoIntegradorBO();
        $objEixoIntegradorVO = new EixoIntegradorVO();

        $objEixoIntegradorVO->setIdEixoCompetenciaHabilidadeTransient($objEixoCompetenciaHabilidadeVO);
        $arrayItEixoIntegrador = $objEixoIntegradorBO->listarPorEixoCompetenciaHabilidade($objEixoIntegradorVO)['retornoOperacao'];


        $this->makeOptions($arrayItEixoIntegrador, 'Id', 'descricao');
    }

    public function getTurno() {


        $idSerie = $this->getParams()[0];
        $idCodSec = $this->getParams()[2];
        $idTipoEnsino = $this->getParams()[3];

        empty($idSerie) || empty($idCodSec) || empty($idTipoEnsino) ? die : null;

        $objTurnoBO = new TurnoBO();
        $objTurnoVO = new TurnoVO();

        $objTurnoVO->setIdSerieTransient($idSerie);
        $objTurnoVO->setIdTipoEnsinoTransient($idTipoEnsino);
        $objTurnoVO->setIdcodSecTransient($idCodSec);

        $TipoTunoArrayIt = $objTurnoBO->listarPorCodSec($objTurnoVO)['retornoOperacao'];
        $this->makeOptions($TipoTunoArrayIt, 'Id', 'Periodo');
    }

    public function getTurma() {


        $params = $this->getParams();

        $idTurno = $params[0];
        $idCodSec = $params[2];
        $idSerie = $params[3];
        $idTipoEnsino = $params[4];
        $idExercicio = $params[5];

        empty($idTurno) || empty($idCodSec) || empty($idSerie) || empty($idTipoEnsino) || empty($idExercicio) ? die : null;

        $objTurmaBO = new TurmaBO();
        $objTurmaVO = new TurmaVO();

        $objTurmaVO->setIdPeriodoDoDia($idTurno);
        $objTurmaVO->getIdEscolaCodSec()->setId($idCodSec);
        $objTurmaVO->getIdSerie()->setId($idSerie);
        $objTurmaVO->getIdSerie()->getIdTipoEnsino()->setId($idTipoEnsino);
        $objTurmaVO->setIdExercicio($idExercicio);


        $turmaArrayIt = $objTurmaBO->listarPorTurnoCodSecSerieTipoAno($objTurmaVO)['retornoOperacao'];
        $this->makeOptions($turmaArrayIt, 'Id', 'Turma');
    }

    public function getAreaDeConhecimento() {

        $params = $this->getParams();
        $idSerie = $params[0];

        empty($idSerie) ? die : null;

        $objAreaConhecimentoBO = new AreaConhecimentoBO();
        $objAreaConhecimentoVO = new AreaConhecimentoVO();

        $objAreaConhecimentoVO->setSerieTransient($idSerie);
        $areaConhecimentoArrayIt = $objAreaConhecimentoBO->listarPorSerie($objAreaConhecimentoVO)['retornoOperacao'];
        $this->makeOptions($areaConhecimentoArrayIt, 'Id', 'AreaConhecimento', FALSE);
    }

    public function getDisciplinas() {


        $params = $this->getParams();

        $idArea = $params[0];
        $idSerie = $params[2];


        empty($idArea) || empty($idSerie) ? die : null;

        $objDisciplinasBO = new DisciplinaBO();
        $objDisciplinasVO = new DisciplinaVO();

        $objDisciplinasVO->setSerieTransient($idSerie);
        $objDisciplinasVO->getIdAreaConhecimento()->setId($idArea);
        $disciplinasArrayIt = $objDisciplinasBO->listarPorSerie($objDisciplinasVO)['retornoOperacao'];
        $this->makeOptions($disciplinasArrayIt, 'Id', 'Disciplina');
    }

    public function getProfessor() {

        $params = $this->getParams();

        $idSec = $params[0];

        empty($idSec) ? die : null;

        $objProfessorBO = new ProfessorBO();
        $objProfessorVO = new ProfessorVO();

        $objProfessorVO->getIdEscolaCodSec()->setId($idSec);
        $objProfessorVO->getIdEscolaCodSecComplementacao()->setId($idSec);
        $professorArrayIt = $objProfessorBO->listarProfessoresPorEscola($objProfessorVO)['retornoOperacao'];
        $this->makeOptions($professorArrayIt, 'Cpf', 'Nome');
    }

    public function getCompetenciasPorEixoPlanoCurso() {

        $params = $this->getParams();

        empty($params[0]) ? die : null;

        $idsEixoIntegrador = explode(',', $params[0]);
        is_array($idsEixoIntegrador) ? $idsEixoIntegrador = implode(',', $idsEixoIntegrador) : null;

        $idsPlanoCurso = $params[2];

        $objCompetenciaBO = new CompetenciaBO();
        $objCompetenciaVO = new CompetenciaVO();

        $objCompetenciaVO->setIdEixoIntegradorTransient($idsEixoIntegrador);
        $objCompetenciaVO->setIdPlanoCursoTransient($idsPlanoCurso);

        $competenciaArayIt = $objCompetenciaBO->listarPorPlanoCursoEixoIntegrador($objCompetenciaVO)['retornoOperacao'];
        $this->makeOptions($competenciaArayIt, 'Id', 'Descricao');
    }

    public function getHabilidadesPorCompetenciaEixoPlanocurso() {

        $params = $this->getParams();

        empty($params[0]) ? die : null;

        $idsCompetencias = explode(',', $params[0]);
        is_array($idsCompetencias) ? $idsCompetencias = implode(',', $idsCompetencias) : null;

        $idsPlanoCurso = $params[2];

        $objHabilidadeBO = new HabilidadeBO();
        $objHabilidadeVO = new HabilidadeVO();

        $objHabilidadeVO->setIdCompetenciaTransient($idsCompetencias);
        $objHabilidadeVO->setIdPlanoCursoTransient($idsPlanoCurso);

        $competenciaArayIt = $objHabilidadeBO->listarPorPlanoCursoCompetencia($objHabilidadeVO)['retornoOperacao'];
        $this->makeOptions($competenciaArayIt, 'IdEixoCompetenciaHabilidadeTransient', 'Descricao');
    }

    public function getHabilidadesNaoVinculadaCompetenciaEixo() {

        $params = $this->getParams();





        $objHabilidadeBO = new HabilidadeBO();
        $objHabilidadeVO = new HabilidadeVO();

        $idCompetencia = $this->getParams()[0];
        $idEixoIntegrador = $this->getParams()[2];
        $idDisciplina = $this->getParams()[3];
        $idAreaConhecimento = $this->getParams()[4];
        $idSerie = $this->getParams()[5];
        $idTipoEnsino = $this->getParams()[6];

        empty($idTipoEnsino) ? die : null;

        $objEixoCompetenciaHabilidadeVO = new EixoCompetenciaHabilidadeVO();
        $objEixoCompetenciaHabilidadeVO->getIdTipoEnsino()->setId($idTipoEnsino);
        $objEixoCompetenciaHabilidadeVO->getIdSerie()->setId($idSerie);
        $objEixoCompetenciaHabilidadeVO->getIdAreaConhecimento()->setId($idAreaConhecimento);
        $objEixoCompetenciaHabilidadeVO->getIdDisciplina()->setId($idDisciplina);
        $objEixoCompetenciaHabilidadeVO->getIdCompetencia()->setId($idCompetencia);
        $objEixoCompetenciaHabilidadeVO->getIdEixoIntegrador()->setId($idEixoIntegrador);

        $objHabilidadeVO->setIdEixoCompetenciaHabilidadeTransient($objEixoCompetenciaHabilidadeVO);
        $arrayItHabilidade = $objHabilidadeBO->getHabilidadesNaoVinculadaCompetenciaEixo($objHabilidadeVO)['retornoOperacao'];

        $this->makeOptions($arrayItHabilidade, 'id', 'Descricao');
    }

    public function getProfessoresPorEscola() {

        $params = $this->getParams();

        empty($params[0]) ? die : null;

        $idEscola = $params[0];


        $objServidorBO = new ServidorBO();
        $objServidorVO = new ServidorVO();
        $objServidorVO->getIdEscolaCodSec()->setId($idEscola);
        $objServidorVO->getIdEscolaCodSecComplementacao()->setId($idEscola);
        $professores = $objServidorBO->listarProfessoresPorEscola($objServidorVO)['retornoOperacao'];

        $this->makeOptions($professores, 'cadastro', 'nome');
    }

    #################################  

    public function getSeriePorTipoEnsino() {

        $idTipoEnsino = $this->getParams()[0];

        empty($idTipoEnsino) ? die : null;

        $objSerieBO = new SerieBO();
        $objSerieVO = new SerieVO();

        $objSerieVO->getIdTipoEnsino()->setId($idTipoEnsino);

        $TipoSerieArrayIt = $objSerieBO->listarByTiPoEnsino($objSerieVO)['retornoOperacao'];
        $this->makeOptions($TipoSerieArrayIt, 'Id', 'Serie');
    }

}
