<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use core\view\View;
use module\almoxarifado\bo\DevolucaoBO;
use module\almoxarifado\vo\DevolucaoVO;
use module\almoxarifado\bo\SaidaProdutoBO;
use module\almoxarifado\vo\SaidaProdutoVO;
use module\almoxarifado\bo\SolicitacaoBO;
use module\almoxarifado\vo\SolicitacaoVO;
use module\almoxarifado\bo\ItemSolicitacaoProdutoBO;
use module\almoxarifado\vo\ItemSolicitacaoProdutoVO;

class DevolucaoController extends AbstractController {

    public function inicio() {
        $view = new View('devolucao/inicio', parent::pathToController());
        $view->setStyles(array('all/css/padrao.css'));
        $view->setScripts(array('almoxarifado/devolucao/js/inicio.js'));
        $view->pageTitle('Consultar Devolução');
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Consultar Devolução de Produtos'));

        $session = $this->getSession();

        $objDevolucaoBO = new DevolucaoBO();
        $objDevolucaoVO = new DevolucaoVO();
//        $objSaidaProdutoBO = new SaidaProdutoBO();
//        $objSaidaProdutoVO = new SaidaProdutoVO();

//        exit("aaaaa");

        if ($this->isPost()) {
            try {
                $post = $this->getAllRequestPost();
                $objDevolucaoVO->bind($post);
                $retornoDevolucao = $objDevolucaoBO->listar($objDevolucaoVO);

                $view->setVariable('arrayDevolucao', $retornoDevolucao['retornoOperacao']);
                $view->setVariable('post', $post);

                $objDevolucaoVO->setDataInicial($post['dataInicial']);
                $objDevolucaoVO->setDataFinal($post['dataFinal']);

                $retornoDataDevolucao = $objDevolucaoBO->selecionar($objDevolucaoVO);
                $view->setVariable('arrayDataDevolucao', $retornoDataDevolucao['retornoOperacao']);
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
        } else {
            $retornoDevolucao = $objDevolucaoBO->listar($objDevolucaoVO);
            $view->setVariable('arrayDevolucao', $retornoDevolucao['retornoOperacao']);
            $view->setVariable('post', array());

//            $retornoSaidaProduto = $objSaidaProdutoBO->listar($objSaidaProdutoVO);
//            $view->setVariable('arrayItSaidaProduto', $retornoSaidaProduto['retornoOperacao']);
        }

        $view->renderize();
    }

    public function visualizar() {
        $view = new View('devolucao/visualizar', parent::pathToController());
        $view->pageTitle('Visualizar Devolução');
        $view->setStyles(array('all/css/padrao.css'));
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Devolução de Produtos', 'Visualizar Devolução'));

        # Devolução #
        $objDevolucaoBO = new DevolucaoBO();
        $objDevolucaoVO = new DevolucaoVO();
        $objSaidaProdutoBO = new SaidaProdutoBO();
        $objSaidaProdutoVO = new SaidaProdutoVO();

        $objDevolucaoVO->setId($this->getParams()[0]);
        $objDevolucaoVO = $objDevolucaoBO->selecionar($objDevolucaoVO)['retornoOperacao'];

        # Item Solicitação #
        $objItemSolicitacaoProdutoBO = new ItemSolicitacaoProdutoBO();
        $objItemSolicitacaoProdutoVO = new ItemSolicitacaoProdutoVO();

        $objItemSolicitacaoProdutoVO->getIdSolicitacao()->setId($objDevolucaoVO->getIdSaidaProduto()->getIdSolicitacaoProduto()->getId());
        $itemSolicitacaoProdutoArrayIterator = $objItemSolicitacaoProdutoBO->listarPorSolicitacao($objItemSolicitacaoProdutoVO)['retornoOperacao'];
        $objDevolucaoVO->setItemSolicitacaoProdutoArrayIterator($itemSolicitacaoProdutoArrayIterator);


        $view->setVariable('objDevolucaoVO', $objDevolucaoVO);
        $view->disableNavbar();
        $view->disableTopbar();
        $view->renderizeReport();
    }

}
