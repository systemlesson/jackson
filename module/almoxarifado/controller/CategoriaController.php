<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use core\view\View;
use module\almoxarifado\bo\CategoriaBO;
use module\almoxarifado\vo\CategoriaVO;

class CategoriaController extends AbstractController {

    public function inicio() {

        $view = new View('categoria/inicio', parent::pathToController());
        $view->setStyles(array('all/css/padrao.css'));
        $view->setScripts(array('almoxarifado/categoria/js/inicio.js')); //criar js
        $view->pageTitle('Consultar Categoria');
        $view->breadcrumb('fa-calendar', array("Administrativo", "Categoria", "Consultar Categoria"));

        $post = null;

        $objCategoriaBO = new CategoriaBO();
        $objCategoriaVO = new CategoriaVO();

        if ($this->isPost()) {
            try {
                $post = $this->getAllRequestPost();
                $objCategoriaVO->bind($post);
                $retornoCategoria = $objCategoriaBO->listar($objCategoriaVO);
                $view->setVariable('arrayCategoria', $retornoCategoria['retornoOperacao']);
                $view->setVariable('post', $post);
            } catch (Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
        } else {
            $retornoCategoria = $objCategoriaBO->listar($objCategoriaVO);
            $view->setVariable('arrayCategoria', $retornoCategoria['retornoOperacao']);
        $view->setVariable('post', array());
        }

        $view->renderize();
    }

    public function adicionar() {
        $session = $this->getSession();
        $view = new View('categoria/adicionar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', "Categoria", 'Adicionar Categoria',));
        $view->pageTitle('Adcionar Categoria');
        $view->setScripts(array('almoxarifado/categoria/js/categoria.js'));

        $objCategoriaBO = new CategoriaBO();
        $objCategoriaVO = new CategoriaVO();

        if ($this->isPost()) {
            try {
                $session = $this->getSession();

                $post = $this->getAllRequestPost();

                #informações principais
                $objCategoriaVO->bind($post);

                #informações de cadastro
                $objCategoriaVO->setUsuarioInclusao($session['usuId']);
                $objCategoriaVO->setDataInclusao(date('d/m/Y H:i:s'));


                $retornoCategoria = $objCategoriaBO->inserir($objCategoriaVO);

                echo $this->returnDefaultSuccessJson($retornoCategoria, 'categoria/inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }

        $retornoCategoria = $objCategoriaBO->listar($objCategoriaVO);

        $view->setVariable('arrayCategoria', $retornoCategoria['retornoOperacao']);
        $view->renderize();
    }

    public function alterar() {
        $view = new View('categoria/alterar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Relação de Categoria', 'Alterar Categoria'));
        $view->pageTitle('Alterar Categoria');
        $view->setScripts(array('almoxarifado/categoria/js/categoria.js'));

        $objCategoriaBO = new CategoriaBO();
        $objCategoriaVO = new CategoriaVO();

        $post = $this->getAllRequestPost();

        if ($this->isPost()) {

            try {
                $session = $this->getSession();

                #informações principais
                $objCategoriaVO->bind($post);

                #informações de cadastro
                $objCategoriaVO->setUsuarioAlteracao($session['usuId']);
                $objCategoriaVO->setDataAlteracao(date('d/m/Y H:i:s'));
                $retorno = $objCategoriaBO->alterar($objCategoriaVO);

                echo $this->returnDefaultSuccessJson($retorno, 'categoria');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }


        $objCategoriaVO->setId($this->getParams()[0]);

        $retorno = $objCategoriaBO->selecionar($objCategoriaVO);

        $view->setVariable('objCategoriaVO', $retorno);

        $view->renderize();
    }

    public function excluir() {
        try {
            $objCategoriaBO = new CategoriaBO();
            $objCategoriaVO = new CategoriaVO();

            $parametors = $this->getParams();
            $session = $this->getSession();

            $objCategoriaVO->setId($parametors[0]);
            $objCategoriaVO->setUsuarioAlteracao($session['usuId']);
            $objCategoriaVO->setDataAlteracao(date('d/m/Y H:i:s'));

            $retorno = $objCategoriaBO->excluir($objCategoriaVO);

            echo $this->returnDefaultSuccessJson($retorno, 'categoria/inicio');
        } catch (\Exception $ex) {
            echo $this->returnDefaultFailJson($ex->getMessage(), 'categoria/inicio');
        }
    }

    public function escolher() {
        $view = new View('categoria/escolher', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Selecionar categoria'));
        $view->pageTitle('Selecionar categoria');
        $view->setScripts(array('almoxarifado/categoria/js/escolher.js')); //criar js

        $objCategoriaBO = new CategoriaBO();
        $objCategoriaVO = new CategoriaVO();

        $retorno = $objCategoriaBO->listar($objCategoriaVO);
        $view->setVariable('arrayCategoria', $retorno['retornoOperacao']);

        $view->renderize();
    }

    public function verificaCategoria() {
        try {
            $objCategoriaBO = new CategoriaBO();
            $objCategoriaVO = new CategoriaVO();

            $post = $this->getAllRequestPost();
            $objCategoriaVO->bind($post);

            $retorno = $objCategoriaBO->verificaCategoria($objCategoriaVO);


            echo json_encode($retorno);
        } catch (\Exception $ex) {
            throw new \Exception("Não foi possível realizar a operação");
        }
    }

}
