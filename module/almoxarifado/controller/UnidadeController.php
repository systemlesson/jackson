<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use core\view\View;
use module\almoxarifado\bo\UnidadeBO;
use module\almoxarifado\vo\UnidadeVO;

class UnidadeController extends AbstractController {

    public function inicio() {

        $view = new View('unidade/inicio', parent::pathToController());
        $view->setStyles(array('all/css/padrao.css'));
        $view->setScripts(array('almoxarifado/unidade/js/inicio.js')); 
        $view->breadcrumb('fa-calendar', array("Administrativo", "Unidade", "Consultar Unidade"));
        $view->pageTitle('Unidade');

        $objUnidadeBO = new UnidadeBO();
        $objUnidadeVO = new UnidadeVO();

        if ($this->isPost()) {
            try {
                $post = $this->getAllRequestPost();
                $objUnidadeVO->bind($post);
                $retornoUnidade = $objUnidadeBO->listar($objUnidadeVO);
                $view->setVariable('arrayUnidade', $retornoUnidade['retornoOperacao']);
                $view->setVariable('post', $post);
            } catch (Exception $ex) {
                
            }
        } else {
            $retornoUnidade = $objUnidadeBO->listar($objUnidadeVO);
            $view->setVariable('arrayUnidade', $retornoUnidade['retornoOperacao']);
            $view->setVariable('post', array());
        }
        $view->renderize();
    }

    public function adicionar() {

        $session = $this->getSession();

        $view = new View('unidade/adicionar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo','Unidade', 'Adicionar Unidade'));
        $view->pageTitle('Adcionar Unidade');
        $view->setScripts(array('almoxarifado/unidade/js/unidade.js'));

        if ($this->isPost()) {
            try {
                $session = $this->getSession();

                $post = $this->getAllRequestPost();

                # informações principais #
                $objUnidadeVO = new UnidadeVO();
                $objUnidadeVO->bind($post);

                # informações de cadastro #
                $objUnidadeVO->setUsuarioInclusao($session['usuId']);
                $objUnidadeVO->setDataInclusao(date('d/m/Y'));

                # inserir #
                $objUnidadeBO = new UnidadeBO();
                $retornoUnidade = $objUnidadeBO->inserir($objUnidadeVO);

                echo $this->returnDefaultSuccessJson($retornoUnidade, 'unidade/inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }

        $objUnidadeVO = new UnidadeVO();
        $objUnidadeBO = new UnidadeBO();
        $retornoUnidade = $objUnidadeBO->listar($objUnidadeVO);

        $view->setVariable('unidadeArrayIt', $retornoUnidade['retornoOperacao']);
        $view->renderize();
    }

    public function alterar() {

        $view = new View('unidade/alterar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo','Unidade','Alterar Unidade'));
        $view->pageTitle('Alterar Unidade');
        $view->setScripts(array('almoxarifado/unidade/js/unidade.js'));

        $objUnidadeBO = new UnidadeBO();
        $objUnidadeVO = new UnidadeVO();

        $post = $this->getAllRequestPost();

        if ($this->isPost()) {
            try {
                $session = $this->getSession();

                #informações principais
                $objUnidadeVO->bind($post);

                #informações de cadastro
                $objUnidadeVO->setUsuarioAlteracao($session['usuId']);
                $objUnidadeVO->setDataAlteracao(date('d/m/Y H:i:s'));

                $retorno = $objUnidadeBO->alterar($objUnidadeVO);

                echo $this->returnDefaultSuccessJson($retorno, 'unidade');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }
        $objUnidadeVO->setId($this->getParams()[0]);

        $retorno = $objUnidadeBO->selecionar($objUnidadeVO);

        $retornoUnidade = $objUnidadeBO->listar($objUnidadeVO);
        $view->setVariable('arrayUnidade', $retornoUnidade);
        $view->setVariable('objUnidadeVO', $retorno['retornoOperacao']);
        $view->renderize();
    }

    public function excluir() {
        try {
            $objUnidadeBO = new UnidadeBO();
            $objUnidadeVO = new UnidadeVO();

            $parametors = $this->getParams();
            $session = $this->getSession();

            $objUnidadeVO->setId($parametors[0]);
            $objUnidadeVO->setUsuarioAlteracao($session['usuId']);
            $objUnidadeVO->setDataAlteracao(date('d/m/Y H:i:s'));

            $retorno = $objUnidadeBO->excluir($objUnidadeVO);

            echo $this->returnDefaultSuccessJson($retorno, 'unidade/inicio');
        } catch (\Exception $ex) {
            echo $this->returnDefaultFailJson($ex->getMessage(), 'unidade/inicio');
        }
    }

//    # Método que controla o campo de filtro #
//    public function escolher() {
//        $view = new View('unidade/escolher', parent::pathToController());
//        $view->breadcrumb('fa-calendar', array('Selecionar Unidade'));
//        $view->pageTitle('Selecionar Unidade');
//        $view->setScripts(array('almoxarifado/unidade/js/escolher.js'));
//
//        $objUnidadeBO = new UnidadeBO();
//        $objUnidadeVO = new UnidadeVO();
//
//        $retorno = $objUnidadeBO->listar($objUnidadeVO);
//        $view->setVariable('arrayUnidade', $retorno['retornoOperacao']);
//        
//        $view->renderize();
//    }

    public function verificaUnidade() {
        try {
            $objUnidadeBO = new UnidadeBO();
            $objUnidadeVO = new UnidadeVO();

            $post = $this->getAllRequestPost();
            $objUnidadeVO->bind($post);

            $retorno = $objUnidadeBO->verificaUnidade($objUnidadeVO);


            echo json_encode($retorno);
        } catch (\Exception $ex) {
            throw new \Exception("Não foi possível realizar a operação"); 
        }
    }

}
