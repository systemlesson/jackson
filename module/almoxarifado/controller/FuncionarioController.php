<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use core\view\View;
use module\almoxarifado\bo\FuncionarioBO;
use module\almoxarifado\vo\FuncionarioVO;
use module\almoxarifado\bo\SetorBO;
use module\almoxarifado\vo\SetorVO;

class FuncionarioController extends AbstractController {

    public function inicio() {

        $view = new View('funcionario/inicio', parent::pathToController());
        $view->setStyles(array('all/css/padrao.css'));
        $view->setScripts(array('almoxarifado/funcionario/js/inicio.js'));

        $objFuncionarioBO = new FuncionarioBO();
        $objFuncionarioVO = new FuncionarioVO();

        if ($this->isPost()) {
            try {
                $post = $this->getAllRequestPost();
                $objFuncionarioVO->bind($post);
                $retornoFuncionario = $objFuncionarioBO->listar($objFuncionarioVO);
//                var_dump($retornoFuncionario); exit;
                $view->setVariable('arrayFuncionario', $retornoFuncionario['retornoOperacao']);
                $view->setVariable('post', $post);
            } catch (\Exception $ex) {
                
            }
        } else {
            $retornoFuncionario = $objFuncionarioBO->listar($objFuncionarioVO);
            $view->setVariable('arrayFuncionario', $retornoFuncionario['retornoOperacao']);
            $view->setVariable('post', array());
        }

        $view->breadcrumb('fa-calendar', array('Administrativo', 'Funcionário', 'Consultar Funcionário'));
        ;
        $view->pageTitle('Consultar Funcionário');
        $view->renderize();
    }

    public function adicionar() {

        $session = $this->getSession();

        $view = new View('funcionario/adicionar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Funcionário', 'Adicionar Funcionário'));
        $view->pageTitle('Cadastro de Funcionário');
        $view->setScripts(array('almoxarifado/funcionario/js/funcionario.js'));

        $objSetorBO = new SetorBO();
        $objSetorVO = new SetorVO();

        $objFuncionarioBO = new FuncionarioBO();
        $objFuncionarioVO = new FuncionarioVO();

        if ($this->isPost()) {
            try {
                $session = $this->getSession();

                $post = $this->getAllRequestPost();

//                var_dump($post); exit;
                # informações principais #
                $objFuncionarioVO->bind($post);

                # informações de cadastro #
                $objFuncionarioVO->setUsuarioInclusao($session['usuNome']);
                $objFuncionarioVO->setDataInclusao(date('d/m/Y H:i:s'));

                # inserir #
                $retornoFuncionario = $objFuncionarioBO->inserir($objFuncionarioVO);

                echo $this->returnDefaultSuccessJson($retornoFuncionario, 'funcionario/inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }

        $retornoFuncionario = $objFuncionarioBO->listar($objFuncionarioVO);

        $retornoSetor = $objSetorBO->listarCombo($objSetorVO);
        $view->setVariable('arraySetor', $retornoSetor);

        $view->setVariable('arrayFuncionario', $retornoFuncionario['retornoOperacao']);
        $view->renderize();
    }

    public function alterar() {
        $view = new View('funcionario/alterar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Funcionário', 'Alterar Funcionário'));
        $view->pageTitle('Alterar Funcionário');
        $view->setScripts(array('almoxarifado/funcionario/js/funcionario.js'));

        $objSetorBO = new SetorBO();
        $objSetorVO = new SetorVO();

        $objFuncionarioBO = new FuncionarioBO();
        $objFuncionarioVO = new FuncionarioVO();

        $session = $this->getSession();
        $post = $this->getAllRequestPost();

        if ($this->isPost()) {
            try {

                #informações principais
                $objFuncionarioVO->bind($post);

                #informações de cadastro
                $objFuncionarioVO->setUsuarioAlteracao($session['usuNome']);
                $objFuncionarioVO->setDataAlteracao(date('d/m/Y H:i:s'));

                $retorno = $objFuncionarioBO->alterar($objFuncionarioVO);

                echo $this->returnDefaultSuccessJson($retorno, 'funcionario');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }

        $objFuncionarioVO->setId($this->getParams()[0]);

        $retorno = $objFuncionarioBO->selecionar($objFuncionarioVO);

        $retornoFuncionario = $objFuncionarioBO->listar($objFuncionarioVO);
        $view->setVariable('arrayFuncionario', $retornoFuncionario);
        $view->setVariable('objFuncionarioVO', $retorno['retornoOperacao']);

        $retornoSetor = $objSetorBO->listarCombo($objSetorVO);
        $view->setVariable('arraySetor', $retornoSetor);

        $view->renderize();
    }

    public function excluir() {
        try {
            $objFuncionarioBO = new FuncionarioBO();
            $objFuncionarioVO = new FuncionarioVO();

            $parametors = $this->getParams();
            $session = $this->getSession();

            $objFuncionarioVO->setId($parametors[0]);
            $objFuncionarioVO->setUsuarioAlteracao($session['usuId']);
            $objFuncionarioVO->setDataAlteracao(date('d/m/Y H:i:s'));

            $retorno = $objFuncionarioBO->excluir($objFuncionarioVO);

            echo $this->returnDefaultSuccessJson($retorno, 'funcionario/inicio');
        } catch (\Exception $ex) {
            echo $this->returnDefaultFailJson($ex->getMessage(), 'funcionario/inicio');
        }
    }

    public function escolher() {
        $view = new View('funcionario/escolher', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Cadastro Básico', 'Funcionário', 'Selecionar Funcionário'));
        $view->pageTitle('Alterar Funcionário');
        $view->setScripts(array('almoxarifado/produto/js/escolher.js'));

        $objFuncionarioBO = new FuncionarioBO();
        $objFuncionarioVO = new FuncionarioVO();

        $retorno = $objFuncionarioBO->listar($objFuncionarioVO);
        $view->setVariable('$objFuncionarioVO', $retorno['retornoOperacao']);

        $view->renderize();
    }

    public function existeMatricula() {
        try {
            $objFuncionarioBO = new FuncionarioBO();
            $objFuncionarioVO = new FuncionarioVO();

            $post = $this->getAllRequestPost();
            $objFuncionarioVO->bind($post);

            $retorno = $objFuncionarioBO->existeMatricula($objFuncionarioVO);

            echo json_encode(array('retorno' => $retorno));
        } catch (\Exception $ex) {
            throw new \Exception("Não foi possível realizar a operação");
        }
    }

}
