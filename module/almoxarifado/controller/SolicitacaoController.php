<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use core\view\View;
use module\almoxarifado\bo\SolicitacaoBO;
use module\almoxarifado\vo\SolicitacaoVO;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;
use module\almoxarifado\bo\FuncionarioBO;
use module\almoxarifado\vo\FuncionarioVO;
use module\almoxarifado\bo\ItemSolicitacaoProdutoBO;
use module\almoxarifado\vo\ItemSolicitacaoProdutoVO;
use module\almoxarifado\vo\SaidaProdutoVO;
use module\almoxarifado\bo\SaidaProdutoBO;
use module\almoxarifado\helper\SolicitacaoHelper;

class SolicitacaoController extends AbstractController {

    public function inicio() {
        $view = new View('solicitacao/inicio', parent::pathToController());
        $view->setStyles(array('all/css/padrao.css'));
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Consultar Solicitação de Produtos'));
        $view->pageTitle('Consultar Solicitação');
        $view->setScripts(array('almoxarifado/solicitacao/js/inicio.js'));
        $session = $this->getSession();

        $objSolicitacaoBO = new SolicitacaoBO();
        $objSolicitacaoVO = new SolicitacaoVO();
        $objFuncionarioBO = new FuncionarioBO();
        $objFuncionarioVO = new FuncionarioVO();

        if ($this->isPost()) {
            try {
                $post = $this->getAllRequestPost();
                $objSolicitacaoVO->bind($post);
                $retornoSolicitacao = $objSolicitacaoBO->listar($objSolicitacaoVO);
                $view->setVariable('arraySolicitacao', $retornoSolicitacao['retornoOperacao']);
                $view->setVariable('post', $post);
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
        } else {
            $retornoSolicitacao = $objSolicitacaoBO->listar($objSolicitacaoVO);
            $view->setVariable('arraySolicitacao', $retornoSolicitacao['retornoOperacao']);
            $view->setVariable('post', array());

            $retornoFuncionario = $objFuncionarioBO->listar($objFuncionarioVO);
            $view->setVariable('arrayItFuncionario', $retornoFuncionario['retornoOperacao']);
        }

        $view->renderize();
    }

    public function adicionar() {
        $view = new View('solicitacao/adicionar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Solicitação de Produtos', 'Adicionar Solicitação'));
        $view->pageTitle('Adicionar Solicitação');
        $view->setScripts(array('almoxarifado/solicitacao/js/solicitacao.js', 'almoxarifado/solicitacao/js/secDynamicGridV3.js'));
        $session = $this->getSession();

        $objSolicitacaoBO = new SolicitacaoBO();
        $objSolicitacaoVO = new SolicitacaoVO();
        $objFuncionarioBO = new FuncionarioBO();
        $objFuncionarioVO = new FuncionarioVO();
        $objProdutoBO = new ProdutoBO();
        $objProdutoVO = new ProdutoVO();
        $objItemSolicitacaoVO = new ItemSolicitacaoProdutoVO();

        if ($this->isPost()) {
            try {
                $session = $this->getSession();
                $post = $this->getAllRequestPost();

                # Informações Principais #
                $objSolicitacaoVO->bind($post);

                # Informações de cadastro #
                $objSolicitacaoVO->setUsuarioInclusao($session['usuNome']);
                $objSolicitacaoVO->setDataInclusao(date('d/m/Y'));
//                var_dump($objSolicitacaoVO->setUsuarioInclusao($session['usuNome'])); die;
                foreach ($post['gridSolicitacao'] as $itemSolicitacao) {
                    $objItemSolicitacaoVO = new ItemSolicitacaoProdutoVO();
                    $objItemSolicitacaoVO->bind($itemSolicitacao);

                    # Informações de cadastro #
                    $objItemSolicitacaoVO->setUsuarioInclusao($session['usuNome']);
                    $objItemSolicitacaoVO->setDataInclusao(date('d/m/Y'));

                    $objSolicitacaoVO->getItemSolicitacaoProdutoArrayIterator()->append($objItemSolicitacaoVO);
                }

                $objSolicitacaoVO->setSituacao("N");

                $retorno = $objSolicitacaoBO->inserir($objSolicitacaoVO);

                echo $this->returnDefaultSuccessJson($retorno, 'solicitacao/inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }

        $retornoProduto = $objProdutoBO->listar($objProdutoVO);
        $view->setVariable('arrayItProduto', $retornoProduto['retornoOperacao']);

        $retornoFuncionario = $objFuncionarioBO->listar($objFuncionarioVO);
        $view->setVariable('arrayItFuncionario', $retornoFuncionario['retornoOperacao']);

        $view->setVariable('initGridValues', json_encode(array('dados' => array(), 'view' => array())));
        $view->setVariable('arraySolicitacao', array());

        $view->renderize();
    }

    public function alterar() {
        $view = new View('solicitacao/alterar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Solicitação de Produtos', 'Alterar Solicitação'));
        $view->pageTitle('Alterar Solicitação');
        $view->setScripts(array('almoxarifado/solicitacao/js/secDynamicGridV3.js', 'almoxarifado/solicitacao/js/solicitacao.js'));

        $objSolicitacaoBO = new SolicitacaoBO();
        $objSolicitacaoVO = new SolicitacaoVO();
        $objProdutoBO = new ProdutoBO();
        $objProdutoVO = new ProdutoVO();
        $objItemSolicitacaoBO = new ItemSolicitacaoProdutoBO();
        $objItemSolicitacaoVO = new ItemSolicitacaoProdutoVO();
        $objFuncionarioBO = new FuncionarioBO();
        $objFuncionarioVO = new FuncionarioVO();

        $session = $this->getSession();

        # Informações de cadastro #
        $objSolicitacaoVO->setId($this->getParams()[0]);
        $objSolicitacaoVO->getIdFuncionarioUsuario()->setId(2);
        $objItemSolicitacaoVO->getIdSolicitacao()->setId($this->getParams()[0]);

        if ($this->isPost()) {
            try {
                # Informações principais #
                $post = $this->getAllRequestPost();
                $objSolicitacaoVO->bind($post);
                $objSolicitacaoVO->setSituacao('N');


                # Informações de cadastro #
                $objSolicitacaoVO->setUsuarioAlteracao($session['usuNome']);
                $objSolicitacaoVO->setDataAlteracao(date('d/m/Y H:i:s'));
                
                if (isset($post['gridSolicitacao'])) {
                    foreach ($post['gridSolicitacao'] as $itemSolicitacao) {
                        $objItemSolicitacaoVO = new ItemSolicitacaoProdutoVO();
                        $objItemSolicitacaoVO->bind($itemSolicitacao);

                        $objItemSolicitacaoVO->setUsuarioInclusao($session['usuId']);
                        $objItemSolicitacaoVO->setDataInclusao(date('d/m/Y'));
                        $objItemSolicitacaoVO->setUsuarioAlteracao($session['usuId']);
                        $objItemSolicitacaoVO->setDataAlteracao(date('d/m/Y'));
                        $objItemSolicitacaoVO->getIdSolicitacao()->setId($this->getParams()[0]);

                        $objSolicitacaoVO->getItemSolicitacaoProdutoArrayIterator()->append($objItemSolicitacaoVO);
                    }
                }
                
                $retorno = $objSolicitacaoBO->alterar($objSolicitacaoVO);

                echo $this->returnDefaultSuccessJson($retorno, 'solicitacao/inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }

        $objSolicitacaoVO = $objSolicitacaoBO->selecionar($objSolicitacaoVO)['retornoOperacao'];
        $view->setVariable('objSolicitacaoVO', $objSolicitacaoVO);

        $arrayItItemSolicitacaoVO = $objItemSolicitacaoBO->listarPorSolicitacao($objItemSolicitacaoVO)['retornoOperacao'];

        $initValues = SolicitacaoHelper::generateInit($arrayItItemSolicitacaoVO);

        $retornoProduto = $objProdutoBO->listar($objProdutoVO);
        $retornoFuncionario = $objFuncionarioBO->listar($objFuncionarioVO);

        $view->setVariable('objSolicitacaoVO', $objSolicitacaoVO);
        $view->setVariable('objItemSolicitacaoVO', $objItemSolicitacaoVO); 
        $view->setVariable('arrayProduto', $retornoProduto['retornoOperacao']);
        $view->setVariable('arrayItFuncionario', $retornoFuncionario['retornoOperacao']);
        $view->setVariable('arrayItProduto', array());
        $view->setVariable('initGridValues', $initValues);

        $view->renderize();
    }

    public function excluir() {
        try {
            $objSolicitacaoBO = new SolicitacaoBO();
            $objSolicitacaoVO = new SolicitacaoVO();

            $parametros = $this->getParams();
            $session = $this->getSession();

            $objSolicitacaoVO->setId($parametros[0]);
            $objSolicitacaoVO->setUsuarioAlteracao($session['usuNome']);
            $objSolicitacaoVO->setDataInclusao(date('d/m/Y H:i:s'));

            $retorno = $objSolicitacaoBO->excluir($objSolicitacaoVO);

            echo $this->returnDefaultSuccessJson($retorno, 'solicitacao/inicio');
        } catch (\Exception $ex) {
            echo $this->returnDefaultFailJson($ex->getMessage(), 'solicitacao/inicio');
        }
    }

    public function visualizar() {
        $view = new View('solicitacao/visualizar', parent::pathToController());
        $view->pageTitle('Visualizar Solicitação');
        $view->setStyles(array('all/css/padrao.css'));
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Solicitação de Produtos', 'Visualizar Solicitação'));

        # Solicitação #
        $objSolicitacaoBO = new SolicitacaoBO();
        $objSolicitacaoVO = new SolicitacaoVO();

        $objSolicitacaoVO->setId($this->getParams()[0]);
        $objSolicitacaoVO = $objSolicitacaoBO->selecionar($objSolicitacaoVO)['retornoOperacao'];

        # Item Solicitação #
        $objItemSolicitacaoProdutoBO = new ItemSolicitacaoProdutoBO();
        $objItemSolicitacaoProdutoVO = new ItemSolicitacaoProdutoVO();

        $objItemSolicitacaoProdutoVO->getIdSolicitacao()->setId($objSolicitacaoVO->getId());
        $itemSolicitacaoProdutoArrayIterator = $objItemSolicitacaoProdutoBO->listarPorSolicitacao($objItemSolicitacaoProdutoVO)['retornoOperacao'];
        $objSolicitacaoVO->setItemSolicitacaoProdutoArrayIterator($itemSolicitacaoProdutoArrayIterator);

        $view->setVariable('objSolicitacaoVO', $objSolicitacaoVO);
        $view->disableNavbar();
        $view->disableTopbar();
        $view->renderize();
    }

    // RAFAEL
    public function relatorioSolicitacaoNaoAtendidas() {
        $view = new View('solicitacao/relatorioSolicitacaoNaoAtendida', parent::pathToController());
        $view->breadcrumb('fa-calendar', array("Relatório de Entrada"));
        $view->setStyles(array('all/css/padrao.css'));
        //$view->setScripts(array('almoxarifado/filtro/js/inicio.js'));
        $view->pageTitle('Solicitações não Atendidas');

        try {

            if ($this->isPost()) {

                // Metodo POST:: LISTA RELATORIO 

                $post = $this->getAllRequestPost();

                $objSolicitacaoBO = new SolicitacaoBO();
                $objSolicitacaoVO = new SolicitacaoVO();

                //$objSolicitacaoVO->setNomeEntregador ($post['NOME_ENTREGADOR']);
                $retornoSolicitacao = $objSolicitacaoBO->relatorioSolicitacaoNaoAtendidas($objSolicitacaoVO);
                $view->setVariable('arraySolicitacao', $retornoSolicitacao['retornoOperacao']);

                $view->renderizeReport();
            } else {
                // Método GET :: FILTRO RELATÓRIO
                $view = new View('solicitacao/filtroRelatorioSolicitacao', parent::pathToController());
                $view->breadcrumb('fa-calendar', array('Administrativo', 'Relatório Estoque', 'Consultar Relatório'));
                $view->setScripts(array('almoxarifado/filtro/js/inicio.js'));

                $view->pageTitle('Relatório de Entrada');
                $view->renderize();
            }
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }
    }

    public function atender() {
        try {
            $objSolicitacaoBO = new SolicitacaoBO();
            $objSolicitacaoVO = new SolicitacaoVO();


            $session = $this->getSession();
            $objSolicitacaoVO->setId($this->getParams()[0]);
            $objSolicitacaoVO->setUsuarioAlteracao($session['usuNome']);
            $objSolicitacaoVO->setDataInclusao(date('d/m/Y H:i:s'));

            $retorno = $objSolicitacaoBO->atender($objSolicitacaoVO);
//            var_dump($retorno); die;

            echo $this->returnDefaultSuccessJson($retorno, 'solicitacao/inicio');
        } catch (\Exception $ex) {
            echo $this->returnDefaultFailJson($ex->getMessage(), 'solicitacao/inicio');
        }
    }

    public function cancelar() {
        try {
            $objSolicitacaoBO = new SolicitacaoBO();
            $objSolicitacaoVO = new SolicitacaoVO();

            $parametros = $this->getParams();
            $session = $this->getSession();

            $objSolicitacaoVO->setId($parametros[0]);
            $objSolicitacaoVO->setUsuarioAlteracao($session['usuNome']);
            $objSolicitacaoVO->setDataInclusao(date('d/m/Y H:i:s'));
            $objSolicitacaoVO->setId($this->getParams()[0]);
            $objSolicitacaoVO->setExcluido(0);

            $retorno = $objSolicitacaoBO->cancelar($objSolicitacaoVO);

            echo $this->returnDefaultSuccessJson($retorno, 'solicitacao/inicio');
        } catch (\Exception $ex) {
            echo $this->returnDefaultFailJson($ex->getMessage(), 'solicitacao/inicio');
        }
    }

}
