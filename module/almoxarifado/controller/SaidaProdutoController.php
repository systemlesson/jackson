<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use core\view\View;
use module\almoxarifado\vo\SaidaProdutoVO;
use module\almoxarifado\bo\SaidaProdutoBO;
use module\almoxarifado\bo\ItemSolicitacaoProdutoBO;
use module\almoxarifado\vo\ItemSolicitacaoProdutoVO;
use module\almoxarifado\bo\DevolucaoBO;
use module\almoxarifado\vo\DevolucaoVO;

class SaidaProdutoController extends AbstractController {

    public function inicio() {

        $view = new View('saidaProduto/inicio', parent::pathToController());
        $view->setStyles(array('all/css/padrao.css'));
        $view->setScripts(array('almoxarifado/saidaProduto/js/inicio.js'));
        $view->pageTitle('Saída de Produto');
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Saída de Produto', 'Consultar Produto'));

        $session = $this->getSession();


        $objSaidaProdutoBO = new SaidaProdutoBO();
        $objSaidaProdutoVO = new SaidaProdutoVO();

        if ($this->isPost()) {
            try {
                $post = $this->getAllRequestPost();
                $objSaidaProdutoVO->bind($post);
                $retornoSaidaProduto = $objSaidaProdutoBO->listar($objSaidaProdutoVO);

                $view->setVariable('arraySaidaProduto', $retornoSaidaProduto['retornoOperacao']);
                $view->setVariable('post', $post);
            } catch (\Exception $ex) {
                
            }
        } else {

            $retornoSaidaProduto = $objSaidaProdutoBO->listar($objSaidaProdutoVO);
//                var_dump($retornoSaidaProduto); die;
            $view->setVariable('arraySaidaProduto', $retornoSaidaProduto['retornoOperacao']);
            $view->setVariable('post', array());
        }


        $view->renderize();
    }

    public function visualizar() {

        $view = new View('saidaProduto/visualizar', parent::pathToController());
        $view->pageTitle('Relatorio de Saida');
        $view->setStyles(array('all/css/padrao.css'));

        //SAIDA PRODUTO
        $objSaidaProdutoBO = new SaidaProdutoBO();
        $objSaidaProdutoVO = new SaidaProdutoVO();

        $objSaidaProdutoVO->setId($this->getParams()[0]);
        $objSaidaProdutoVO = $objSaidaProdutoBO->selecionar($objSaidaProdutoVO)['retornoOperacao'];

        //ITEM SOLICITACAO
        $objItemSolicitacaoProdutoBO = new ItemSolicitacaoProdutoBO();
        $objItemSolicitacaoProdutoVO = new ItemSolicitacaoProdutoVO();

        $objItemSolicitacaoProdutoVO->getIdSolicitacao()->setId($objSaidaProdutoVO->getIdSolicitacaoProduto()->getId());
        $ItemEntradaProdutoArrayIterator = $objItemSolicitacaoProdutoBO->listarPorSolicitacao($objItemSolicitacaoProdutoVO)['retornoOperacao'];
        $objSaidaProdutoVO->setItemEntradaProdutoArrayIterator($ItemEntradaProdutoArrayIterator);

        $view->setVariable('objSaidaProdutoVO', $objSaidaProdutoVO);
        $view->disableNavbar();
        $view->disableTopbar();
        $view->renderize();
    }

    public function devolver() {

        $objDevolucaoBO = new DevolucaoBO();
        $objDevolucaoVO = new DevolucaoVO();

        $session = $this->getSession();

        $objDevolucaoVO->getIdSaidaProduto()->setId($this->getParams()[0]);
        $objDevolucaoVO->setDataInclusao(date('d/m/Y H:i:s'));
        $objDevolucaoVO->setUsuarioInclusao($session['usuNome']);
        $objDevolucaoVO->setExcluido(0);

        $objDevolucaoVO->getIdSaidaProduto()->getIdSolicitacaoProduto()->setId($this->getParams()[0]);

        try {

            $retorno = $objDevolucaoBO->devolver($objDevolucaoVO);
//            var_dump($retorno); die;
            
            echo $this->returnDefaultSuccessJson($retorno, 'saidaproduto/inicio');
        } catch (\Exception $exc) {
            echo $this->returnDefaultFailJson($exc->getMessage(), 'saidaproduto/inicio');
        }
    }

}
