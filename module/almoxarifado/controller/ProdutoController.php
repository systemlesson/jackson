<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use core\view\View;
use module\almoxarifado\vo\ProdutoVO;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\bo\UnidadeBO;
use module\almoxarifado\vo\UnidadeVO;
use module\almoxarifado\vo\CategoriaVO;
use module\almoxarifado\bo\CategoriaBO;
use module\almoxarifado\bo\SaidaProdutoBO;
use module\almoxarifado\vo\SaidaProdutoVO;

class ProdutoController extends AbstractController {

    public function inicio() {

        $view = new View('produto/inicio', parent::pathToController());
        $view->setStyles(array('all/css/padrao.css'));
        $view->setScripts(array('almoxarifado/produto/js/inicio.js'));
        $view->pageTitle('Consultar Produto');
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Produto', 'Consultar Produto'));

        $session = $this->getSession();


        $objProdutoBO = new ProdutoBO();
        $objProdutoVO = new ProdutoVO();
        if ($this->isPost()) {
            try {
                $post = $this->getAllRequestPost();
                $objProdutoVO->bind($post);
                $retornoProduto = $objProdutoBO->listar($objProdutoVO)['retornoOperacao'];
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
        } else {
            $retornoProduto = $objProdutoBO->listar($objProdutoVO)['retornoOperacao'];
        }
        $view->setVariable('arrayProduto', $retornoProduto);

        $view->renderize();
    }

    public function adicionar() {

        $session = $this->getSession();
        $view = new View('produto/adicionar', parent::pathToController());

        $view->breadcrumb('fa-calendar', array('Administrativo', 'Produto', 'Adicionar Produto'));
        $view->pageTitle('Adicionar Produto');
        $view->setScripts(array('almoxarifado/produto/js/produto.js'));

        $objUnidadeVO = new UnidadeVO();
        $objUnidadeBO = new UnidadeBO();

        $objCategoriaBO = new CategoriaBO();
        $objCategoriaVO = new CategoriaVO();

        $objProdutoBO = new ProdutoBO();
        $objProdutoVO = new ProdutoVO();

        if ($this->isPost()) {
            try {
                $session = $this->getSession();

                $post = $this->getAllRequestPost();

                #INFORMACOES PRINCIPAIS
                $objProdutoVO->bind($post);

                # INFORMACOES DE CADASTRO

                $objProdutoVO->setUsuarioInclusao($session['usuNome']);
                $objProdutoVO->setDataInclusao(date('d/m/Y H:i:s'));
                $objProdutoVO->setQuantidade(0);

                $retorno = $objProdutoBO->inserir($objProdutoVO);

                echo $this->returnDefaultSuccessJson($retorno, 'produto/inicio');
            } catch (\Exception $ex) {

                echo $this->returnDefaultFailJson($ex->getMessage());
            }

            $view->noRenderize();
        }
        $retornoCategoria = $objCategoriaBO->listarCategoria($objCategoriaVO);
        $view->setVariable('arrayItCategoria', $retornoCategoria);

        $retornoUnidade = $objUnidadeBO->listarUnidade($objUnidadeVO);
        $view->setVariable('arrayItUnidade', $retornoUnidade);

        $view->renderize();
    }

    public function alterar() {

        $view = new View('produto\alterar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Produto', 'Alterar Produto'));
        $view->pageTitle('Alterar Produto');
        $view->setScripts(array('almoxarifado/produto/js/produto.js'));

        $objProdutoBO = new ProdutoBO();
        $objProdutoVO = new ProdutoVO();

        $objUnidadeVO = new UnidadeVO();
        $objUnidadeBO = new UnidadeBO();

        $objCategoriaBO = new CategoriaBO();
        $objCategoriaVO = new CategoriaVO();

        $session = $this->getSession();


        if ($this->isPost()) {
            try {


                $post = $this->getAllRequestPost();
                #INFORMACOES PRINCIPAIS
                $objProdutoVO->bind($post);

                # INFORMACOES DE CADASTRO
                $objProdutoVO->setUsuarioAlteracao($session['usuId']);
                $objProdutoVO->setDataAlteracao(date('d/m/Y H:i:s'));
                $objProdutoVO->setQuantidade(0);

                $retorno = $objProdutoBO->alterar($objProdutoVO);

                echo $this->returnDefaultSuccessJson($retorno, 'produto/inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }

            $view->noRenderize();
        }

        $objProdutoVO->setId($this->getParams()[0]);

        $objProdutoVO = $objProdutoBO->selecionar($objProdutoVO)['retornoOperacao'];
        $view->setVariable('objProdutoVO', $objProdutoVO);


        $retornoCategoria = $objCategoriaBO->listarCategoria($objCategoriaVO);
        $view->setVariable('arrayItCategoria', $retornoCategoria);

        $retornoUnidade = $objUnidadeBO->listarUnidade($objUnidadeVO);
        $view->setVariable('arrayItUnidade', $retornoUnidade);

        $view->renderize();
    }

    public function excluir() {

        try {
            $objProdutoBO = new ProdutoBO();
            $objProdutoVO = new ProdutoVO();

            $parametros = $this->getParams();
            $session = $this->getSession();

            $objProdutoVO->setId($parametros[0]);

            $objProdutoVO->setUsuarioAlteracao($session['usuId']);
            $objProdutoVO->setDataAlteracao(date('d/m/Y H:i:s'));

            $retorno = $objProdutoBO->excluir($objProdutoVO);

            echo $this->returnDefaultSuccessJson($retorno, 'produto/inicio');
        } catch (\Exception $ex) {
            echo $this->returnDefaultFailJson($ex->getMessage(), 'produto/inicio');
        }
    }

    public function relatorioDeProduto() {

        $view = new View('Produto/relatorio', parent::pathToController());
        $view->breadcrumb('fa-calendar', array("Relatorio Produto"));
        $view->setStyles(array('all/css/padrao.css'));
        $view->pageTitle('Relatório de Produto em Estoque');

        try {
            if ($this->isPost()) {
                // Metodo POST:: LISTA RELATORIO 
                $post = $this->getAllRequestPost();

                $objProdutoBO = new ProdutoBO();
                $objProdutoVO = new ProdutoVO();

                $objProdutoVO->bind($post);
                $retornoProduto = $objProdutoBO->relatorioDeProduto($objProdutoVO);
                $view->setVariable('arrayProduto', $retornoProduto['retornoOperacao']);

                $view->renderizeReport();
            } else {
                // Método GET :: FILTRO RELATÓRIO
                $view = new View('Produto/filtroRelatorioProduto', parent::pathToController());
                $view->breadcrumb('fa-calendar', array('Administrativo', 'Relatório do Estoque', 'Consultar Relatório'));
                $view->pageTitle('Relatório de produto em Estoque');
                $view->renderize();
            }
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }
    }

    public function adicionaProdutoEstoque() {

        $session = $this->getSession();
        $objProdutoBO = new ProdutoBO();
        $objProdutoVO = new ProdutoVO();

        if ($this->isPost()) {
            try {

                $post = $this->getAllRequestPost();

                #INFORMACOES PRINCIPAIS
                $objProdutoVO->bind($post);

                # INFORMACOES DE CADASTRO

                $objProdutoVO->setUsuarioInclusao($session['usuNome']);
                $objProdutoVO->setDataInclusao(date('d/m/Y H:i:s'));
                $objProdutoVO->setQuantidade(0);

                $retorno = $objProdutoBO->adicionarProdutoEstoque($objProdutoVO);

                echo $this->returnDefaultSuccessJson($retorno, 'produto/inicio');
            } catch (\Exception $ex) {

                echo $this->returnDefaultFailJson($ex->getMessage());
            }

            $view->noRenderize();
        }
     
        $view->renderize();
    }

    // função subtrair a quantidade


    public function retirarProdutoEstoque() {

        $session = $this->getSession();
        $view = new View('produto/adicionar', parent::pathToController());

        $view->breadcrumb('fa-calendar', array('Administrativo', 'Produto', 'Adicionar Produto'));
        $view->pageTitle('Adicionar Produto');
        $view->setScripts(array('almoxarifado/produto/js/produto.js'));

        if ($this->isPost()) {
            try {
                $session = $this->getSession();
                $post = $this->getAllRequestPost();
                
                $objSaidaProdutoBO = new SaidaProdutoBO();
                $objSaidaProdutoVO = new SaidaProdutoVO();

                #INFORMACOES PRINCIPAIS
                $objSaidaProdutoVO->bind($post);

                # INFORMACOES DE CADASTRO
                $objSaidaProdutoVO->setUsuarioInclusao($session['usuNome']);
                $objSaidaProdutoVO->setDataInclusao(date('d/m/Y H:i:s'));
                $objSaidaProdutoVO->setQuantidade(0);

                $retorno = $objSaidaProdutoBO->inserir($objSaidaProdutoVO);

                echo $this->returnDefaultSuccessJson($retorno, 'produto/inicio');
            } catch (\Exception $ex) {

                echo $this->returnDefaultFailJson($ex->getMessage());
            }

            $view->noRenderize();
        }
        $retornoCategoria = $objSaidaProdutoBO->listarCategoria($objSaidaProdutoVO);
        $view->setVariable('arrayItCategoria', $retornoCategoria);

        $retornoSaidaProduto = $objSaidaProdutoBO->listarUnidade($objSaidaProdutoVO);
        $view->setVariable('arrayItUnidade', $retornoSaidaProduto);

        $view->renderize();
    }

}
