<?php

namespace module\almoxarifado\controller;

use core\controller\AbstractController;
use core\view\View;
use module\almoxarifado\bo\ProdutoBO;
use module\almoxarifado\vo\ProdutoVO;
use module\almoxarifado\vo\EntradaProdutoVO;
use module\almoxarifado\bo\EntradaProdutoBO;
use module\almoxarifado\bo\ItemEntradaProdutoBO;
use module\almoxarifado\vo\ItemEntradaProdutoVO;
use module\almoxarifado\vo\FuncionarioVO;
use module\almoxarifado\bo\FuncionarioBO;
use module\almoxarifado\helper\EntradaProdutoHelper;

class EntradaProdutoController extends AbstractController {

    public function inicio() {

        $view = new View('entradaproduto/inicio', parent::pathToController());
        $view->setStyles(array('all/css/padrao.css'));
        $view->setScripts(array('almoxarifado/produto/js/inicio.js'));
        $view->pageTitle('Entrada de Produto');
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Entrada Produto', 'Consultar Produto'));

        $session = $this->getSession();


        $objEntradaProdutoBO = new EntradaProdutoBO();
        $objEntradaProdutoVO = new EntradaProdutoVO();


        if ($this->isPost()) {
            try {
                $post = $this->getAllRequestPost();
                $objEntradaProdutoVO->bind($post);
                $retornoEntradaProduto = $objEntradaProdutoBO->listar($objEntradaProdutoVO);

                $view->setVariable('arrayEntradaProduto', $retornoEntradaProduto['retornoOperacao']);
                $view->setVariable('post', $post);
            } catch (\Exception $ex) {
                
            }
        } else {

            $retornoEntradaProduto = $objEntradaProdutoBO->listar($objEntradaProdutoVO);
            $view->setVariable('arrayEntradaProduto', $retornoEntradaProduto['retornoOperacao']);
            $view->setVariable('post', array());
        }


        $view->renderize();
    }

    public function adicionar() {

        $session = $this->getSession();

        $view = new View('entradaproduto/adicionar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Entrada Produto', 'Adcionar Produto'));
        $view->pageTitle('Adicionar Produto');
        $view->setScripts(array('almoxarifado/entradaProduto/js/secDynamicGridV3.js', 'almoxarifado/entradaProduto/js/EntradaProduto.js'));


        $objEntradaProdutoBO = new EntradaProdutoBO();
        $objEntradaProdutoVO = new EntradaProdutoVO();



        if ($this->isPost()) {
            try {
                #INFORMACOES PRINCIPAIS
                $session = $this->getSession();
                $post = $this->getAllRequestPost();

                #INFORMACOES PRINCIPAIS
                $objEntradaProdutoVO->bind($post);
                # INFORMACOES DE CADASTRO
                $objEntradaProdutoVO->setUsuarioInclusao($session['usuNome']);
                $objEntradaProdutoVO->setDataInclusao(date('d/m/Y H:i'));
                $objEntradaProdutoVO->setIdFuncionarioUsuario(2);

                foreach ($post['gridProdutos'] as $itemEntradaProduto) {
                    $objItemEntradaProdutoVO = new ItemEntradaProdutoVO();
                    $objItemEntradaProdutoVO->bind($itemEntradaProduto);

                    # INFORMACOES DE CADASTRO
                    $objItemEntradaProdutoVO->setUsuarioInclusao($session['usuNome']);
                    $objItemEntradaProdutoVO->setDataInclusao(date('d/m/Y'));

                    $objEntradaProdutoVO->getItemEntradaProdutoArrayIterator()->append($objItemEntradaProdutoVO);
                }

                $retorno = $objEntradaProdutoBO->inserir($objEntradaProdutoVO);

                echo $this->returnDefaultSuccessJson($retorno, 'entradaproduto/inicio');
            } catch (\Exception $ex) {

                echo $this->returnDefaultFailJson($ex->getMessage());
            }

            $view->noRenderize();
        }

        $objProdutoBO = new ProdutoBO();
        $objProdutoVO = new ProdutoVO();
        $retornoProduto = $objProdutoBO->listar($objProdutoVO);
        $view->setVariable('arrayProduto', $retornoProduto['retornoOperacao']);
        $view->setVariable('initGridValues', json_encode(array('dados' => array(), 'view' => array())));

        $view->setVariable('$arrayEntradaProduto', array());



        $view->renderize();
    }

    public function alterar() {

        $view = new View('entradaproduto\alterar', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Administrativo', 'Entrada Produto', 'Alterar Entrada Produto'));
        $view->pageTitle('Alterar Entrada Produto');
        $view->setScripts(array('almoxarifado/entradaProduto/js/secDynamicGridV3.js', 'almoxarifado/entradaProduto/js/EntradaProduto.js'));

        $objEntradaProdutoBO = new EntradaProdutoBO();
        $objEntradaProdutoVO = new EntradaProdutoVO();
        $objItemEntradaProdutoBO = new ItemEntradaProdutoBO();

        $session = $this->getSession();
        if ($this->isPost()) {
            try {

                #INFORMACOES PRINCIPAIS
                $post = $this->getAllRequestPost();
                $objEntradaProdutoVO->bind($post);
                # INFORMACOES DE CADASTRO
                $objEntradaProdutoVO->setUsuarioAlteracao($session['usuNome']);
                $objEntradaProdutoVO->setDataAlteracao(date('d/m/Y H:i:s'));
                $objEntradaProdutoVO->setIdFuncionarioUsuario($session['usuId']);
                
                foreach ($post['gridProdutos'] as $itemEntradaProduto) {
                    $objItemEntradaProdutoVO = new ItemEntradaProdutoVO();
                    $objItemEntradaProdutoVO->bind($itemEntradaProduto);
//                    var_dump($objItemEntradaProdutoVO); die;
//                    $objItemEntradaProdutoVO = $objItemEntradaProdutoBO->listar($objItemEntradaProdutoVO)['retornoOperacao'];

                    # INFORMACOES DE CADASTRO
                    $objItemEntradaProdutoVO->setUsuarioInclusao($session['usuNome']);
                    $objItemEntradaProdutoVO->setDataInclusao(date('d/m/Y'));
                    $objItemEntradaProdutoVO->setUsuarioAlteracao($session['usuNome']);
                    $objItemEntradaProdutoVO->setDataAlteracao(date('d/m/Y'));

                    $objEntradaProdutoVO->getItemEntradaProdutoArrayIterator()->append($objItemEntradaProdutoVO);
//                    var_dump($objEntradaProdutoVO->getItemEntradaProdutoArrayIterator()); die;
                }
                $objEntradaProdutoVO->setId($this->getParams()[0]);

                $retorno = $objEntradaProdutoBO->alterar($objEntradaProdutoVO);
//                var_dump($retorno); die;
                echo $this->returnDefaultSuccessJson($retorno['retornoMensagem'], 'entradaproduto/inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }

            $view->noRenderize();
        }

        $objEntradaProdutoVO->setId($this->getParams()[0]);
        $objEntradaProdutoVO = $objEntradaProdutoBO->selecionar($objEntradaProdutoVO)['retornoOperacao'];
        $view->setVariable('objEntradaProdutoVO', $objEntradaProdutoVO);

        $objItemEntradaProdutoVO = new ItemEntradaProdutoVO;
        $objItemEntradaProdutoBO = new ItemEntradaProdutoBO;
        $objItemEntradaProdutoVO->getIdEntradaProdutos()->setId($objEntradaProdutoVO->getId());
        $arrayItItemEntradaProdutoVO = $objItemEntradaProdutoBO->listarItemEntradaProduto($objItemEntradaProdutoVO)['retornoOperacao'];

        $initValues = EntradaProdutoHelper::generateInit($arrayItItemEntradaProdutoVO);
        $objProdutoBO = new ProdutoBO();
        $objProdutoVO = new ProdutoVO();
        $retornoProduto = $objProdutoBO->listar($objProdutoVO);

        $view->setVariable('objEntradaProdutoVO', $objEntradaProdutoVO);
        $view->setVariable('arrayProduto', $retornoProduto['retornoOperacao']);
        $view->setVariable('initGridValues', $initValues);
        $view->setVariable('$arrayEntradaProduto', array());

        $view->renderize();
    }

    public function excluir() {

        try {
            $objEntradaProdutoBO = new EntradaProdutoBO();
            $objEntradaProdutoVO = new EntradaProdutoVO();

            $parametros = $this->getParams();
            $session = $this->getSession();

            $objEntradaProdutoVO->setId($parametros[0]);

            $objEntradaProdutoVO->setUsuarioAlteracao($session['usuId']);
            $objEntradaProdutoVO->setDataAlteracao(date('d/m/Y H:i:s'));

            $retorno = $objEntradaProdutoBO->excluir($objEntradaProdutoVO);

            echo $this->returnDefaultSuccessJson($retorno, 'produto/inicio');
        } catch (\Exception $ex) {
            echo $this->returnDefaultFailJson($ex->getMessage(), 'produto/inicio');
        }
    }

    public function visualizar() {

        $view = new View('entradaProduto/visualizar', parent::pathToController());
        $view->pageTitle('Relatorio de Entrada');
        $view->setStyles(array('all/css/padrao.css'));

        //ENTRADA PRODUTO
        $objEntradaProdutoBO = new EntradaProdutoBO();
        $objEntradaProdutoVO = new EntradaProdutoVO();
        $objEntradaProdutoVO->setId($this->getParams()[0]);
        $objEntradaProdutoVO = $objEntradaProdutoBO->selecionar($objEntradaProdutoVO)['retornoOperacao'];

        //ITEM PRODUTO
        $objItemEntradaProdutoBO = new ItemEntradaProdutoBO();
        $objItemEntradaProdutoVO = new ItemEntradaProdutoVO();
        $objItemEntradaProdutoVO->getIdEntradaProdutos()->setId($objEntradaProdutoVO->getId());
        $ItemEntradaProdutoArrayIterator = $objItemEntradaProdutoBO->listarItemEntradaProduto($objItemEntradaProdutoVO)['retornoOperacao'];
        $objEntradaProdutoVO->setItemEntradaProdutoArrayIterator($ItemEntradaProdutoArrayIterator);
        $view->setVariable('objEntradaProdutoVO', $objEntradaProdutoVO);

        $view->disableNavbar();
        $view->disableTopbar();

        $view->renderize();
    }

    public function relatorioDeEntrada() {

        $view = new View('entradaProduto/relatorioDeEntrada', parent::pathToController());
        $view->breadcrumb('fa-calendar', array("Relatorio Entrada"));
        $view->setStyles(array('all/css/padrao.css'));
        //$view->setScripts(array('almoxarifado/filtro/js/inicio.js'));
        $view->pageTitle('Relatório de Entrada');

        try {

            if ($this->isPost()) {

                // Metodo POST:: LISTA RELATORIO 

                $post = $this->getAllRequestPost();
//                var_dump($post); die;

                $objEntradaProdutoBO = new EntradaProdutoBO();
                $objEntradaProdutoVO = new EntradaProdutoVO();

                $objEntradaProdutoVO->setDataInicial($post['dataInicial']);
                $objEntradaProdutoVO->setDataFinal($post['dataFinal']);

                //$objEntradaProdutoVO->setNomeEntregador ($post['NOME_ENTREGADOR']);
                $retornoEntradaProduto = $objEntradaProdutoBO->relatorioDeEntrada($objEntradaProdutoVO);
                $view->setVariable('arrayEntradaProduto', $retornoEntradaProduto['retornoOperacao']);

                $view->renderizeReport();
            } else {
                // Método GET :: FILTRO RELATÓRIO
                $view = new View('entradaProduto/filtroRelatorioEntrada', parent::pathToController());
                $view->breadcrumb('fa-calendar', array('Administrativo', 'Relatório Estoque', 'Consultar Relatório'));
                $view->setScripts(array('almoxarifado/entradaProduto/js/filtroRelatorioDeEntrada.js'));

                $view->pageTitle('Relatório de Entrada');
                $view->renderize();
            }
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }
    }

}

?>