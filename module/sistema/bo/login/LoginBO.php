<?php

namespace module\sistema\bo\login;

use core\helper\BoHelper;
use core\helper\LogHelper;
use module\sistema\dao\login\LoginDAO;
use module\sistema\vo\login\LoginVO;
use core\bo\AbstractBO;
use module\sistema\vo\login\UsuarioVO;

/**
 * Classe de negócio referente a Login
 * @author Judá Passos Viegas Santos
 */
class LoginBO extends AbstractBO {

    /**
     * Método que realiza a autenticação do usuário
     * @param \module\sistema\vo\LoginVO $objUsuarioVO
     * @return \layers\helper\BoHelper
     */
    public function autenticar(UsuarioVO $objUsuarioVO) {


        $objBoHelper = new BoHelper();

        ## Verificando ##

        if (strlen($objUsuarioVO->getEmail()) == 0) {
            $objBoHelper->addRetornoMensagem("Campo Email não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }


        if (strlen($objUsuarioVO->getSenha()) == 0) {
            $objBoHelper->addRetornoMensagem("Campo SENHA não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        ## Executando ##

        if (!$objBoHelper->getChkErro()) {

            $objLoginDAO = new LoginDAO();

            try {

                $objBoHelper->setRetorno($objLoginDAO->autenticar($objUsuarioVO));
                $objBoHelper->setRetornoMensagem("Autenticado com sucesso!");
           } catch (\Exception $ex) { echo $ex->getMessage(); die;
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        ## Retornando ##

        return $objBoHelper->getRetorno();
    }
    
    public function selecionarBySenha(UsuarioVO $objUsuarioVO) {


        $objBoHelper = new BoHelper();

        ## Verificando ##

//        if (strlen($objLoginVO->getUsuario()) == 0) {
//            $objBoHelper->addRetornoMensagem("Campo USUARIO não pode ser vazio");
//            $objBoHelper->setChkErro(TRUE);
//        }
//
//
//        if (strlen($objLoginVO->getSenha()) == 0) {
//            $objBoHelper->addRetornoMensagem("Campo SENHA não pode ser vazio");
//            $objBoHelper->setChkErro(TRUE);
//        }

        ## Executando ##

        if (!$objBoHelper->getChkErro()) {

            $objLoginDAO = new LoginDAO();

            try {

                $objBoHelper->setRetorno($objLoginDAO->selecionarBySenha($objUsuarioVO));
                $objBoHelper->setRetornoMensagem("Autenticado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage(); die;
                throw new \Exception($ex->getMessage());
            }
        } else {
//            LogManipulador::registrar(__CLASS__, __FUNCTION__, $objBoHelper->getMsgRetorno());          
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        ## Retornando ##

        return $objBoHelper->getRetorno();
    }

//    private function validaAcesso($cpf, $permissoes) {
//        $perfisLivres = array('administrador', 'equipecodeb', 'coordenadorcodeb', 'diretornte');
//        foreach ($permissoes as $key => $descricao) {
//            if (in_array($key, $perfisLivres)) {
//                return true;
//            }
//        }
//        $professorBO = new ProfessorBO();
//    }

}
