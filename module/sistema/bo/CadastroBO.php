<?php

namespace module\sistema\bo;

use core\helper\BoHelper;
use module\sistema\dao\login\LoginDAO;
use module\sistema\vo\login\UsuarioVO;
use module\sistema\dao\CadastroDAO;
use core\bo\AbstractBO;

/**
 * Classe de negócio referente a Login
 * @author Judá Passos Viegas Santos
 */
class CadastroBO extends AbstractBO {

    /**
     * Método que realiza a autenticação do usuário
     * @param \module\sistema\vo\UsuarioVO $objUsuarioVO
     * @return \layers\helper\BoHelper
     */
    public function inserirUsuario(UsuarioVO $objUsuarioVO) {


        $objBoHelper = new BoHelper();

        ## Verificando ##

        if (strlen($objUsuarioVO->getNome()) == 0) {
            $objBoHelper->addRetornoMensagem("Campo Nome Completo não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objUsuarioVO->getEmail()) == 0) {
            $objBoHelper->addRetornoMensagem("Campo E-mail não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objUsuarioVO->getCpf()) == 0) {
            $objBoHelper->addRetornoMensagem("Campo CPF não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objUsuarioVO->getSenha()) == 0) {
            $objBoHelper->addRetornoMensagem("Campo Senha não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objUsuarioVO->getConfirSenha()) == 0) {
            $objBoHelper->addRetornoMensagem("Campo Confirmar Senha não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }
        if (strlen($objUsuarioVO->getGenero()) == 0) {
            $objBoHelper->addRetornoMensagem("Campo Genero não pode ser vazio");
            $objBoHelper->setChkErro(TRUE);
        }

        ## Executando ##

        if (!$objBoHelper->getChkErro()) {

            $objCadastroDAO = new CadastroDAO();

            try {

                $objBoHelper->setRetorno($objCadastroDAO->inserirUsuario($objUsuarioVO));
                $objBoHelper->setRetornoMensagem("Adicionado com sucesso!");
           } catch (\Exception $ex) {
                throw new \Exception("Não foi Possível realizar a operação");
            }
        } else {
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        ## Retornando ##

        return $objBoHelper->getRetorno();
    }
    
    public function selecionarBySenha(LoginVO $objLoginVO) {


        $objBoHelper = new BoHelper();

        ## Verificando ##

//        if (strlen($objLoginVO->getUsuario()) == 0) {
//            $objBoHelper->addRetornoMensagem("Campo USUARIO não pode ser vazio");
//            $objBoHelper->setChkErro(TRUE);
//        }
//
//
//        if (strlen($objLoginVO->getSenha()) == 0) {
//            $objBoHelper->addRetornoMensagem("Campo SENHA não pode ser vazio");
//            $objBoHelper->setChkErro(TRUE);
//        }

        ## Executando ##

        if (!$objBoHelper->getChkErro()) {

            $objLoginDAO = new LoginDAO();

            try {

                $objBoHelper->setRetorno($objLoginDAO->selecionarBySenha($objLoginVO));
                $objBoHelper->setRetornoMensagem("Autenticado com sucesso!");
            } catch (\Exception $ex) {
                echo $ex->getMessage(); die;
                throw new \Exception($ex->getMessage());
            }
        } else {
//            LogManipulador::registrar(__CLASS__, __FUNCTION__, $objBoHelper->getMsgRetorno());          
            throw new \Exception($objBoHelper->getRetornoMensagemImplode());
        }

        ## Retornando ##

        return $objBoHelper->getRetorno();
    }

//    private function validaAcesso($cpf, $permissoes) {
//        $perfisLivres = array('administrador', 'equipecodeb', 'coordenadorcodeb', 'diretornte');
//        foreach ($permissoes as $key => $descricao) {
//            if (in_array($key, $perfisLivres)) {
//                return true;
//            }
//        }
//        $professorBO = new ProfessorBO();
//    }

}
