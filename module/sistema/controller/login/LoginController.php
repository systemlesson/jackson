<?php

namespace module\sistema\controller\login;

use core\controller\AbstractController;
use core\view\View;
use config\SystemConfig;
use core\helper\SessionHelper;
use core\helper\FormatHelper;
use module\sistema\vo\login\UsuarioVO;
use module\sistema\bo\login\LoginBO;
class LoginController extends AbstractController {

    public function inicio() {
        $view = new View('login\login', parent::pathToController());
        $view->pageTitle('Login');
        $view->setStyles(array('sistema/login/css/login.css'));
        $view->setScripts(array('sistema/login/js/login.js'));
//        var_dump("aaa"); die;
        
        
//        $session = $this->getSession();
//        echo '<pre>'; var_dump($session); die;
//        if($session['cadastro']){
//                    
//                }
        
        if ($this->isPost()) {

            try {
                SessionHelper::generateSessionName();
                SessionHelper::startSession();
                SessionHelper::setSessionValue('captcha', TRUE);
                
                $objUsuarioVO = new UsuarioVO();
                $objLoginBO = new LoginBO();
                $objUsuarioVO->setEmail($this->getRequestPost('email'));
                $objUsuarioVO->setSenha($this->getRequestPost('senha'));
                
                //Verificar se o usuário e senha esta correto
                if ($objLoginBO->autenticar($objUsuarioVO)['retornoOperacao']) {
                    //Seleciona o usuário do banco local
                    $retornoUser = $objLoginBO->selecionarBySenha($objUsuarioVO);
                    if (!$retornoUser['retornoOperacao']) {
                        echo $this->returnDefaultFailJson(SystemConfig::SYSTEM_MSG['MSG28']);
                        exit();
                    }
                    $user = new UsuarioVO();
                    $user = $retornoUser['retornoOperacao'];
//                    echo '<pre>';
//                    var_dump($user); die;

                    /* INFORMACAO DO USUARIO */
                    SessionHelper::setSessionValue('usuNome', $user->getNome());
                    SessionHelper::setSessionValue('usuLogin', $user->getEmail());
                    SessionHelper::setSessionValue('usuId', $user->getId());

//                    /* SEGURANÇA */
//                    SessionHelper::setSessionValue('segPerfil', $userRasea->getPerfilNome());
//                    SessionHelper::setSessionValue('segPerfilDescricao', $userRasea->getPerfilDescricao());
//                    SessionHelper::setSessionValue('segPermissoes', $permissoes);
//                    SessionHelper::setSessionValue('segChaveFixa', md5(SystemConfig::CHAVE_APLICACAO));
                    SessionHelper::setSessionValue('segPerfil', $user->getTipoUsuario());
//                    var_dump($user->getTipoUsuario()); die;
                    SessionHelper::setSessionValue('segPerfilDescricao', $user->getTipoUsuario());
//                    var_dump($user->getTipoUusario()); die;
                    SessionHelper::setSessionValue('segPermissoes', array());
                    SessionHelper::setSessionValue('segChaveFixa', md5(SystemConfig::CHAVE_APLICACAO));

                    /* INFORMAÇÕES DE ACESSO */
                    SessionHelper::setSessionValue('aceDataUltimoAcesso', date('Y-m-d'));
                    SessionHelper::setSessionValue('aceHoraUltimoAcesso', date('h:i:s'));
                    SessionHelper::setSessionValue('aceTotalAcesso', 1);
                    
                   
                    echo $this->returnDefaultSuccessJson(SystemConfig::SYSTEM_MSG['MSG28'], 'inicio');
                } else {
                    echo $this->returnDefaultFailJson(SystemConfig::SYSTEM_MSG['MSG30']);
                }
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }


//        $view->disableFooter();
        $view->disableTopbar();
        $view->disableMain();
        $view->disableSessionControl();
        $view->renderize();
    }

    public function esqueciMinhaSenha() {
        $this->redirect('RecuperarSenha');
    }

}

?>