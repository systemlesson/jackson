<?php

namespace module\sistema\controller\cadastro;

use core\controller\AbstractController;
use core\view\View;
use module\sistema\vo\login\UsuarioVO;
use module\sistema\bo\CadastroBO;

class CadastroController extends AbstractController {

    public function inicio() {

        $view = new View('cadastro\cadastro', parent::pathToController());
        $view->pageTitle('Cadastro');
        $view->setStyles(array('sistema/cadastro/css/cadastro.css'));
        $view->setScripts(array('sistema/cadastro/js/cadastro.js'));


        if ($this->isPost()) {
            try {

                $session = $this->getSession();
                
//                echo '<pre>';
//                var_dump($session); die;
//                $post = $this->getAllRequestPost();
                
                $objUsuarioVO = new UsuarioVO();
                $objCadastroBO = new CadastroBO();

                
                $objUsuarioVO->setNome($this->getRequestPost('nome'));
                $objUsuarioVO->setEmail($this->getRequestPost('email'));
                $objUsuarioVO->setCpf($this->getRequestPost('cpf'));
                $objUsuarioVO->setSenha($this->getRequestPost('senha'));
                $objUsuarioVO->setConfirSenha($this->getRequestPost('confirmar_senha'));
                $objUsuarioVO->setGenero($this->getRequestPost('genero'));
                $objUsuarioVO->setTipoUsuario('U');
                $objUsuarioVO->setUsuarioInclusao($session['usuNome']);
                $objUsuarioVO->setDataInclusao(date('d/m/Y H:i:s'));

                $retorno = $objCadastroBO->inserirUsuario($objUsuarioVO);
                
                
                echo $this->returnDefaultSuccessJson($retorno, 'inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }

            $view->noRenderize();
        }



//        $view->disableFooter();
//        $view->disableTopbar();
        $view->disableMain();
        $view->disableSessionControl();
        $view->renderize();
    }


}

?>