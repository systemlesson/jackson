<?php

namespace module\sistema\controller\RecuperarSenha;

use core\controller\AbstractController;
use core\view\View;
use core\bo\rasea\RaseaBO;
use core\vo\rasea\RaseaVO;
use core\helper\ControllerHelper;
use core\helper\SafeHelper;
use core\helper\EmailHelper;
use config\SystemConfig;
use core\helper\FormatHelper;

class RecuperarSenhaController extends AbstractController {

    public function inicio() {

        $view = new View('RecuperarSenha\recuperarSenha', parent::pathToController());

        $view->pageTitle('Recuperar senha');
        $view->setStyles(array('sistema/alterarSenha/css/alterarSenha.css'));
        $view->setScripts(array('sistema/alterarSenha/js/alterarSenha.js'));

        $objControllerHelper = new ControllerHelper();

        if ($this->isPost()) {

            try {
                $objRaseaBO = new RaseaBO();
                $objRaseaVO = new RaseaVO();

                $objRaseaVO->setUsuarioLogin(FormatHelper::removeMask($this->getRequestPost('cpf')));

                $objRaseaVO->setNovaSenha(SafeHelper::geraSenha(6));
                
                $objControllerHelper->setRetorno($objRaseaBO->recuperarSenha($objRaseaVO));
                
                $email = trim($objRaseaBO->obterEmail($objRaseaVO));
                
                if(empty($email)){
                    echo $this->returnDefaultFailJson('CPF não cadastrado!');
                }
                
                $textoProvisorioSenha = EmailHelper::aplicarTemplatePadrao("<strong> Acesso ao PAPUAB</strong> <br><br>"
                        . "<strong>Usuário:</strong> " . $this->getRequestPost('cpf') . "<br>"
                        . "<strong>Nova senha:</strong> " . $objRaseaVO->getNovaSenha() . "<br>");

                EmailHelper::enviar("Recuperação de senha", $textoProvisorioSenha, SystemConfig::EMAIL_REMETENTE, SystemConfig::NOME_REMETENTE, $email);
                
                if ($objControllerHelper->getRetornoOperacao()) {
                    echo $this->returnDefaultSuccessJson($objControllerHelper->getRetornoMensagem(), 'login');
                } else {
                    echo $this->returnDefaultFailJson($objControllerHelper->getRetornoMensagem());
                }
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
        }

        $view->disableNavbar();
        $view->disableTopbar();
        $view->disableMain();
        $view->disableSessionControl();
        $view->renderize();
    }

}

?>