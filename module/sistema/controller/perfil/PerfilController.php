<?php

namespace module\sistema\controller\perfil;

use core\controller\AbstractController;
use core\view\View;
use core\bo\rasea\RaseaBO;
use core\vo\rasea\RaseaVO;
use config\SystemConfig;
use core\helper\SessionHelper;
use core\helper\SafeHelper;

class PerfilController extends AbstractController {

    public function asdasd() {
        $view = new View('perfil\perfil', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Perfil'));
        $view->pageTitle('Perfil');

        $view->renderize();
    }

    public function alterarSenha() {
        $view = new View('perfil\alterarSenha', parent::pathToController());
        $view->breadcrumb('fa-calendar', array('Alterar Senha'));
        $view->setScripts(array('sistema/perfil/alterarSenha.js'));
        $view->pageTitle('Alterar Senha');

        if ($this->isPost()) {

            try {
                SessionHelper::generateSessionName();
                SessionHelper::startSession();

                $objRaseaBO = new RaseaBO();
                $objRaseaVO = new RaseaVO();

                $objRaseaVO->setAplicacaoNome(SystemConfig::AUTH_RASEA_APPLICATION_NAME);
                $objRaseaVO->setUsuarioSenha($this->getRequestPost('frmSenhaAtual'));
                $objRaseaVO->setUsuarioLogin(SessionHelper::getSessionValue('usuLogin'));
                
                $retorno = $objRaseaBO->alterarSenha($objRaseaVO, $this->getRequestPost('frmNovaSenha'),$this->getRequestPost('frmConfNovaSenha'));
                    
                if ($retorno['retornoOperacao']) {
                     echo $this->returnDefaultSuccessJson($retorno['retornoMensagem'], 'inicio');   
                } else {
                    echo $this->returnDefaultFailJson($retorno['retornoMensagem']);
                }
                
                 echo $this->returnDefaultSuccessJson('Senha alterada com sucesso!', 'inicio');
            } catch (\Exception $ex) {
                echo $this->returnDefaultFailJson($ex->getMessage());
            }
            $view->noRenderize();
        }
        
        $view->renderize();
    }

}

?>