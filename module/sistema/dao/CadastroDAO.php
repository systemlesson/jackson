<?php

namespace module\sistema\dao;

use module\sistema\factory\ConnectionFactory;
use core\helper\DaoHelper;
use core\helper\LogHelper;
use module\sistema\vo\login\UsuarioVO;

/**
 * Classe de persistencia Login
 * @package layers
 * @subpackage dao
 * @author Judá Passos Viegas Santos
 */
class CadastroDAO extends ConnectionFactory {  //implements LoginInterface {

    /**
     * @var BdHelper $bm
     * @access private
     */

    private $dm = NULL;

    function __construct() {

        $this->dm = new DaoHelper();
    }

    /**
     * @param UsuarioVO $objUsuarioVO
     * @return LoginVO
     * @access public
     */
    public function inserirUsuario(UsuarioVO $objUsuarioVO) {

        $objDaoHelper = new DaoHelper();

        try {


            // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('TRAB'));

            // Comando SQL

            $objDaoHelper->setSql("INSERT INTO USUARIOS
                                  (
                                   NOME,
                                   EMAIL,
                                   CPF,
                                   SENHA,
                                   CONFIRMAR_SENHA,
                                   TIPO_USUARIO,
                                   GENERO,
                                   DATA_INCLUSAO,
                                   USUARIO_INCLUSAO
                                    )                                    
                                 VALUES 
                                 ( 
                                   :NOME,
                                   :EMAIL,
                                   :CPF,
                                   :SENHA,
                                   :CONFIRMAR_SENHA,
                                   :TIPO_USUARIO,
                                   :GENERO,
                                   :DATA_INCLUSAO,
                                   :USUARIO_INCLUSAO
                                    )
                                     ");


            $objDaoHelper->setStmt($objDaoHelper->getConexao()->prepare($objDaoHelper->getSql()));

            // Atribuindo valores
            $objDaoHelper->getStmt()->bindValue(":NOME", $objUsuarioVO->getNome());
            $objDaoHelper->getStmt()->bindValue(":EMAIL", $objUsuarioVO->getEmail());
            $objDaoHelper->getStmt()->bindValue(":CPF", $objUsuarioVO->getCpf());
            $objDaoHelper->getStmt()->bindValue(":SENHA", $objUsuarioVO->getSenha());
            $objDaoHelper->getStmt()->bindValue(":CONFIRMAR_SENHA", $objUsuarioVO->getConfirSenha());
            $objDaoHelper->getStmt()->bindValue(":TIPO_USUARIO", $objUsuarioVO->getTipoUsuario());
            $objDaoHelper->getStmt()->bindValue(":GENERO", $objUsuarioVO->getGenero());
            $objDaoHelper->getStmt()->bindValue(":USUARIO_INCLUSAO", $objUsuarioVO->getUsuarioInclusao());
            $objDaoHelper->getStmt()->bindValue(":DATA_INCLUSAO", $objUsuarioVO->getDataInclusao());

//            echo $objDaoHelper->getSql(); die;
            // Executando comando
//            echo'<pre>';
//            var_dump($objUsuarioVO);die;
            $objDaoHelper->getStmt()->execute();

            $objDaoHelper->setRetornoOperacao(TRUE);

            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {

            //      LogManipulador::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

}
