<?php

namespace module\sistema\dao\login;

use module\sistema\factory\ConnectionFactory;
use core\helper\DaoHelper;
use core\helper\LogHelper;
use module\sistema\vo\login\UsuarioVO;

/**
 * Classe de persistencia Login
 * @package layers
 * @subpackage dao
 * @author Judá Passos Viegas Santos
 */
class LoginDAO extends ConnectionFactory {  //implements LoginInterface {

    /**
     * @var BdHelper $bm
     * @access private
     */

    private $dm = NULL;

    function __construct() {

        $this->dm = new DaoHelper();
    }

    /**
     * @param LoginVO $objLoginVO
     * @return LoginVO
     * @access public
     */
    public function autenticar(UsuarioVO $objUsuarioVO) {

        $objDaoHelper = new DaoHelper();

        try {


            // Obtendo conexao
            $objDaoHelper->setConexao(parent::getInstance('TRAB'));

            // Comando SQL

            $objDaoHelper->setSql(" SELECT COUNT(U.EMAIL) AS TOTAL
                                        FROM USUARIOS U
                                    WHERE 
                                        U.EMAIL = :EMAIL
                                        AND U.SENHA = :SENHA");


            $objDaoHelper->setStmt($objDaoHelper->getConexao()->prepare($objDaoHelper->getSql()));

            // Atribuindo valores
            $objDaoHelper->getStmt()->bindValue(":EMAIL", $objUsuarioVO->getEmail());
            $objDaoHelper->getStmt()->bindValue(":SENHA", $objUsuarioVO->getSenha());

//            echo $objDaoHelper->getSql(); die;
            // Executando comando
            $objDaoHelper->getStmt()->execute();

            // Obtendo resposta
//            print_r($objDaoHelper->getStmt()->fetchAll()); die;
            foreach ($objDaoHelper->getStmt()->fetchAll() as $autenticar) {
                if ($autenticar['total'] > 0) {
                    $objDaoHelper->setRetornoOperacao(TRUE);
                }
            }
            //Fechando conexão
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {

            //      LogManipulador::registrar(__CLASS__, __FUNCTION__, $ex->getMessage());
            throw new \Exception($ex->getMessage());
        }

        // Retornando resposta
        return $objDaoHelper->getRetorno();
    }

    public function selecionarBySenha(UsuarioVO $objUsuarioVO) {
        $objDaoHelper = new DaoHelper();

        try {
            # Obtendo conexão #
            $objDaoHelper->setConexao(parent::getInstance('TRAB'));

            # Comando SQL #
            $objDaoHelper->setSql("SELECT 
                                    U.ID, 
                                    U.NOME, 
                                    U.EMAIL, 
                                    U.SENHA,
                                    U.CPF,
                                    U.TIPO_USUARIO,
                                    U.GENERO
                                FROM USUARIOS U 
                                WHERE 
                                U.SENHA = :SENHA
                                AND U.EMAIL = :EMAIL");
                                   

            # Atribuindo valores #
            
            $objDaoHelper->setStmt($objDaoHelper->getConexao()->prepare($objDaoHelper->getSql()));
            
            $objDaoHelper->getStmt()->bindValue(":SENHA", $objUsuarioVO->getSenha());
            $objDaoHelper->getStmt()->bindValue(":EMAIL", $objUsuarioVO->getEmail());
            
//            echo $objDaoHelper->getSql(); exit;
            $objDaoHelper->getStmt()->execute();
            
            # Setando retorno em caso de sucesso. Por padrão setRetornoOperacao é FALSE. #
            foreach ($objDaoHelper->getStmt()->fetchAll() as $usuario) {
                $objUsuarioVO = new UsuarioVO();
                $objUsuarioVO->bind($usuario);
            }
            $objDaoHelper->setRetornoOperacao($objUsuarioVO);

            # Encerrando conexão #
            $objDaoHelper->setConexao(NULL);
        } catch (Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

        # Retorno da Resposta #
        return $objDaoHelper->getRetorno();
    }

}
