<?php
namespace module\sistema\vo\login;
use core\vo\AbstractVO;

/**
 * Description of LoginVO
 *
 * @author juda.santos
 */
class UsuarioVO extends AbstractVO {
   
    
    private $id;
    private $nome;
    private $email;
    private $cpf;
    private $senha;
    private $tipoUsuario;
    private $genero;
    private $confirSenha;
    
     public function __construct() {
        parent::__construct();
    }
    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getSenha() {
        return $this->senha;
    }

    function getTipoUsuario() {
        return $this->tipoUsuario;
    }

    function getGenero() {
        return $this->genero;
    }

    function getConfirSenha() {
        return $this->confirSenha;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setTipoUsuario($tipoUsuario) {
        $this->tipoUsuario = $tipoUsuario;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setConfirSenha($confirSenha) {
        $this->confirSenha = $confirSenha;
    }

            
    
    public function bind($array, $prefixo = "") {

 
          !empty($array["{$prefixo}id"]) ? $this->setId(trim($array["{$prefixo}id"])) : null;
         !empty($array["{$prefixo}nome"]) ? $this->setNome(trim($array["{$prefixo}nome"])) : null;
         !empty($array["{$prefixo}senha"]) ? $this->setSenha(trim($array["{$prefixo}senha"])) : null;
         !empty($array["{$prefixo}corfirmar_senha"]) ? $this->setConfirSenha(trim($array["{$prefixo}corfirmar_senha"])) : null;
         !empty($array["{$prefixo}email"]) ? $this->setEmail(trim($array["{$prefixo}email"])) : null;
         !empty($array["{$prefixo}tipo_usuario"]) ? $this->setTipoUsuario(trim($array["{$prefixo}tipo_usuario"])) : null;
         !empty($array["{$prefixo}genero"]) ? $this->setGenero(trim($array["{$prefixo}genero"])) : null;
         !empty($array["{$prefixo}cpf"]) ? $this->setCpf(trim($array["{$prefixo}cpf"])) : null;
         
         !empty($array["{$prefixo}usuario_inclusao"]) ? $this->setUsuarioInclusao(trim($array["{$prefixo}usuario_inclusao"])) : null;
         !empty($array["{$prefixo}usuario_alteracao"]) ? $this->setUsuarioAlteracao(trim($array["{$prefixo}usuario_alteracao"])) : null;
         !empty($array["{$prefixo}data_inclusao"]) ? $this->setDataInclusao(trim($array["{$prefixo}data_inclusao"])) : null;
         !empty($array["{$prefixo}data_alteracao"]) ? $this->setDataAlteracao(trim($array["{$prefixo}data_alteracao"])) : null;
    
        
         
      }
    
}
