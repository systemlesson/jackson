<?php

namespace module\sistema\factory;

use module\sistema\implementation\DbEstudoFrameworkDesenvolvimentoImplementation;
use module\sistema\implementation\DbEstudoFrameworkHomologacaoImplementation;
use module\sistema\implementation\DbEstudoFrameworkProducaoImplementation;
use core\factory\CoreConnectionFactory;
use config\SystemConfig;

class ConnectionFactory extends CoreConnectionFactory {

    private static $instanciaEstudoFrameworkDesenvolvimento;
    private static $instanciaEstudoFrameworkHomologacao;
    private static $instanciaEstudoFrameworkProducao;
  

    public static function getInstance($db) {

//        if(!is_null(parent::getDefaultInstance())){
//            return parent::getDefaultInstance();
//        }
        
        $db = strtoupper($db . "_" . SystemConfig::ENVIRONMENT);

        switch ($db) {
            case "TRAB_DESENVOLVIMENTO":
                if (!isset(self::$instanciaEstudoFrameworkDesenvolvimento)) {
                    self::$instanciaEstudoFrameworkDesenvolvimento = DbEstudoFrameworkDesenvolvimentoImplementation::connect();
                }
                return self::$instanciaEstudoFrameworkDesenvolvimento;
                break;

            case "TRAB_HOMOLOGACAO":
                if (!isset(self::$instanciaEstudoFrameworkHomologacao)) {
                    self::$instanciaEstudoFrameworkHomologacao = DbEstudoFrameworkHomologacaoImplementation::connect();
                }
                return self::$instanciaEstudoFrameworkHomologacao;
                break;

            case "TRAB_PRODUCAO":
                if (!isset(self::$instanciaEstudoFrameworkProducao)) {
                    self::$instanciaEstudoFrameworkProducao = DbEstudoFrameworkProducaoImplementation::connect();
                }
                return self::$instanciaEstudoFrameworkProducao;
                break;

            default :
                die('INSTANCIA NAO EXISTE');
                break;
        }

        return NULL;
    }

  

}

?>